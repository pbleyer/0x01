![0x01](doc/0x01.png)

A bunch of utilities for everything and nothing in particular.

# [util/cblf](util/cblf): Lock-free circular buffer

Provides a single-writer/single-reader lock free circular buffer using the unallocated slot mechanism.
Both C ([util_cblf.h](util/cblf/util_cblf.h) module)
and C++ ([Cblf.h](util/cblf/Cblf.h) class) implementations are given.

# [util/dlst](util/dlst): Dual linked list

Dual linked list implementation in C ([util_dlst.h](util/dslt/util_dlst.h) module).

# [util/graph](util/graph): Graph

A C++ directed graph implementation using node adjacency lists.

# [util/guid](util/guid): UUID library

A C++ UUID class wrapper that maps UUIDs to C++ arrays.

# [util/jamc](util/jamc): A compact serialization library

Implements a serialization protocol as defined in [jam.xlxs](util/jam/jam.xlsx)

