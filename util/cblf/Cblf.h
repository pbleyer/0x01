/** @file
	Single-producer, single-consumer, lock-free circular buffer support using
	unallocated slot.

	Only methods that modify atomically the head and tail indexes to push or pull
	items from the buffer are guaranteed to be lock-free. hence, head and tail
	assignments marked as (*atomic*) must be executed in a single uninterrupted
	step.

	If CBLF_USE_EXCEPTIONS is defined, methods will throw exceptions with
	incorrect usage.

	@author pbleyer
*/

#ifndef CBLF_H_
#define CBLF_H_

#include <atomic>
#include <cstddef>
#include <algorithm>

#ifdef CBLF_USE_EXCEPTIONS
#include <stdexcept>
#endif

template <
	typename ValueType, ///< Buffer type
	std::size_t SizeValue, ///< Buffer total size (including unallocated slot)
	typename AtomicType = std::atomic_size_t, ///< Atomic type for head and tail indexes
	typename IndexType = std::size_t ///< Type for index calculations
>
class Cblf
{
public:
	using value_type = ValueType; ///< Buffer value type
	using atomic_type = AtomicType; ///< Atomic access type
	using index_type = IndexType; ///< Index calculation

private:
	volatile atomic_type head; ///< Current head
	volatile atomic_type tail; ///< Current tail
	value_type data[SizeValue]; ///< Buffer array

private:
	/** Calculate next index. */
	static inline index_type next(index_type n)
	{
		++n;
		return (n < SizeValue) ? n : 0 /* Wrap around */;
	}

#ifdef CBLF_USE_EXCEPTIONS
	/* Exception messages */
	static inline const char *errInvalidSize() { return "Invalid buffer size"; }
	static inline const char *errEmpty() { return "Buffer is empty"; }
	static inline const char *errFull() { return "Buffer is full"; }
#endif

public:

	/** Circular buffer constructor.
		@note Due to unallocated slot, size of 1 does not allow any storage.
	*/
	Cblf()
		: head(0), tail(0)
	{
#ifdef CBLF_USE_EXCEPTIONS
		if (SizeValue < 1)
			throw std::invalid_argument(errInvalidSize());
#endif
	}

	/** Copy constructor. */
	Cblf(const Cblf &c)
		: head(c.head), tail(c.tail)
	{
		std::copy(c.data, c.data + SizeValue, data);
	}

	/** Assignment operator. */
	Cblf &operator=(const Cblf &c)
	{
		head = c.head, tail = c.tail;
		std::copy(c.data, c.data + SizeValue, data);
		return *this;
	}

	/** Destructor. */
	~Cblf()
	{ }

	/** Get the circular buffer size. */
	inline std::size_t size() const
	{
		return SizeValue;
	}

	/** Get the circular buffer current number of items.
		@note Number of items may have changed after this call (not a lock-free operation).
	*/
	inline index_type items() const
	{
		volatile index_type h = head, t = tail; // Cache head, tail
		return (t < h) ? SizeValue - (h - t) : t - h;
	}

	/** Get the circular buffer current available space.
		@note Available space may have changed after this call (not lock-free operation).
	*/
	inline index_type available() const
	{
		volatile index_type h = head, t = tail; // Cache head, tail
		return ((t < h) ? h - t : SizeValue - (t - h)) - 1;
	}

	/** Clear a circular buffer. */
	inline void clear()
	{
		head = tail; // Method is lock-free if this operation is atomic
	}

	/** Check if circular buffer is empty (head == tail). */
	inline bool empty() const
	{
		return (head == tail);
	}

	/** Check if circular buffer is full ('next' tail is head).
		@note Buffer condition may have changed after this call if head was updated.
	*/
	inline bool full() const
	{
		return (next(tail) == head);
	}

	/** Add single element to the buffer by value.
		@param e Value to be put
		@return True on success
	*/
	inline bool put(const value_type &e)
	{
		index_type n = next(tail);
		if (n == head) // Unavailable
		{
#ifdef CBLF_USE_EXCEPTIONS
			throw std::overflow_error(errFull());
#else
			return false;
#endif
		}
		data[tail] = e;
		tail = n; // Update tail (*atomic*)
		return true; // Success;
	}

	/** Add elements to the buffer.
		@param ii Begin iterator
		@param oi End iterator
		@return Number of elements successfully added to the buffer
	*/
	template <typename I, typename O>
	inline std::size_t put(const I ii, const O oi)
	{
		std::size_t i = 0;
		index_type n;

		while (ii != oi)
		{
			n = next(tail);
			if (n == head)
				break; // Unavailable
			data[tail] = *ii++;
			tail = n; // Update tail (*atomic*)
			++i;
		}
		return i; // Number of elements added
	}

	/** Add (and overwrite) a single element into the buffer by value.
		@return True if value was overwritten
		@note This function needs locking or mutual exclusion in a multi-threaded environment.
	*/
	inline bool write(const value_type &e)
	{
		bool r = false; // True when a value was overwritten
		index_type n = next(tail);

		if (n == head) // Head reached, move it (will need lock)
		{
			head = next(head); // Update head
			r = true; // Value was overwritten
		}
		data[tail] = e;
		tail = n; // Update tail (*atomic*)
		return r;
	}

	/** Add (and overwrite) elements to the buffer.
		@param ii Begin iterator
		@param oi End iterator
		@param ow Set to true if values were overwritten
		@return Number of elements successfully added to the buffer
		@note This function needs locking or mutual exclusion in a multi-threaded environment.
	*/
	template <typename I, typename O>
	inline std::size_t write(const I ii, const O oi, bool &ow)
	{
		std::size_t i = 0;
		index_type n;
		ow = false; // True when values were overwritten

		while (ii != oi)
		{
			n = next(tail);
			if (n == head) // Head reached, move it (will need lock)
			{
				head = next(head);
				ow = true; // Value was overwritten
			}
			data[tail] = *ii++;
			tail = n; // Update tail (*atomic*)
			++i;
		}
		return i; // Number of elements added
	}

	/** Add (and overwrite) elements to the buffer.
		@param ii Begin iterator
		@param oi End iterator
		@return Number of elements successfully added to the buffer
	*/
	template <typename I, typename O>
	inline index_type write(const I ii, const O oi)
	{
		bool ow; // Dummy
		return write(ii, oi, ow);
	}

	/** Peek element from the buffer (value).
		@return Peeked value (temporary if data not available)
	*/
	inline value_type peek() const
	{
		if (empty())
		{
#ifdef CBLF_USE_EXCEPTIONS
			throw std::underflow_error(errEmpty());
#else
			return value_type();
#endif
		}
		else
			return data[head];
	}

	/** Peek element from the buffer into value pointer.
		@param e Value pointer
		@return True if data was available
	*/
	inline bool peek(value_type *e) const
	{
		if (empty())
			return false;
		else
		{
			*e = data[head];
			return true;
		}
	}

	/** Peek elements from the buffer
		@param ii Begin iterator
		@param oi End iterator
		@return Number of available items
	*/
	template <typename I, typename O>
	inline std::size_t peek(I ii, O oi)
	{
		std::size_t i = 0;
		index_type h = head;

		while (ii != oi)
		{
			if (h == tail)
				break; // No more elements
			*ii++ = data[h];
			h = next(h);
			++i;
		}
		return i;
	}

	/** Get element from the buffer.
		@return Value removed from buffer (temporary if no data was available)
	*/
	inline value_type get()
	{
		if (empty())
		{
#ifdef CBLF_USE_EXCEPTIONS
			throw std::underflow_error(errEmpty());
#else
			return value_type();
#endif
		}
		value_type e = data[head];
		head = next(head); // Move head (*atomic*)
		return e;
	}

	/** Get element from the buffer into value pointer (if provided).
		@param e Value pointer; if nullptr value is dropped from buffer
		@return True if data was available
	*/
	inline bool get(value_type *e)
	{
		if (empty())
		{
#ifdef CBLF_USE_EXCEPTIONS
			throw std::underflow_error(errEmpty());
#else
			return false;
#endif
		}
		if (e)
			*e = data[head];
		head = next(head); // Move head (*atomic*)
		return true;
	}

	/** Get elements from the buffer.
		@param ii Begin iterator
		@param oi End iterator
		@return Number of elements removed
	*/
	template <typename I, typename O>
	inline std::size_t get(I ii, O oi)
	{
		std::size_t i = 0;

		while (ii != oi)
		{
			if (empty())
				break; // Empty
			*ii++ = data[head];
			++i;
			head = next(head); // Move head (*atomic*)
		}
		return i; // Number of elements removed
	}
};

#endif // CBLF_H_
