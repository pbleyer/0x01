/** @file
	Simple test for Cblf implementation
*/

#include <thread>
#include <iostream>

#define CBLF_USE_EXCEPTIONS
#include "Cblf.h"

#define BUFFER_SIZE 10
#define PRODUCER_TIME_BASE 10
#define CONSUMER_TIME_BASE 20
#define ITERATIONS 100

Cblf<int, BUFFER_SIZE> cblf; ///< Shared buffer

void
producer()
{
	int i = 0;
	while (i < ITERATIONS)
	{
		try
		{
			if (!cblf.full())
			{
				int n = rand() & 0xff;
				std::cout << "Producer: " << n << std::endl;
				cblf.put(n);
				++i;
			}
			else
				std::cout << "Producer: Buffer is full" << std::endl;

			std::this_thread::sleep_for(std::chrono::milliseconds(int(double(rand())/RAND_MAX * PRODUCER_TIME_BASE)));
		}
		catch (std::exception &e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
}

void
consumer()
{
	int i = 0;
	while (i < ITERATIONS)
	{
		try
		{
			if (!cblf.empty())
			{
				int n = cblf.items();
				std::vector<int> l(n);
				std::vector<int>::iterator li = l.begin();

				n = cblf.get(li, li + n);

				std::cout << "Consumer: " << n << " item" << ((n == 1) ? ":" : "s:");
				for (li = l.begin(); li != l.end(); ++li)
					std::cout << " " << (*li);
				std::cout << std::endl;
				i += n;
			}
			else
				std::cout << "Consumer: Buffer is empty" << std::endl;

			std::this_thread::sleep_for(std::chrono::milliseconds(int(double(rand())/RAND_MAX * CONSUMER_TIME_BASE)));
		}
		catch (std::exception &e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
}

int
main(int argc, char **argv)
{
	std::thread pt(producer);
	std::thread ct(consumer);
	pt.join();
	ct.join();
	return 0;
}
