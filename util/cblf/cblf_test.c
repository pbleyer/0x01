/** @file
	Test lock-free circular buffer implementation
*/

#include <stdint.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFU16_SIZE 10
#define BUFU32_SIZE 10
#define PRODUCER_TIME_BASE 200
#define CONSUMER_TIME_BASE 100

#define ITERATIONS 10

// CblfU16 type and functions
#define cblf_ATOM uint8_t
#define cblf_NAME U16
#define cblf_TYPE uint16_t
#include "util_cblf.h"

// CblfU32 type and functions
#define cblf_ATOM uint8_t
#define cblf_NAME U32
#define cblf_TYPE uint32_t
#define cblf_SIZE BUFU32_SIZE // All CblfU32 objects have a fixed size buffer
#include "util_cblf.h"

// Build the cblfU16 variable
// cblf_TYPEVAR(cblfU16, uint8_t, uint8_t, uint16_t, BUFU16_SIZE, /*empty*/);
// static CblfU16 cblfU16;
static cblf_BUILD(uint8_t /*size type*/, uint8_t /*atomic type*/, uint16_t /*type*/, BUFU16_SIZE /*size*/, /*attributes*/) cblfU16;

static CblfU32 cblfU32;

static void *
producer(void *p)
{
	struct timespec ts;
	int i = 0;
	while (i < ITERATIONS)
	{
		uint16_t n = rand() & 0xffff;
		if (!cblfU16Put(&cblfU16, n))
		{
			printf("Producer: %d\n", n);
			++i;
		}
		else
			printf("Producer unable to put!\n");

		ts.tv_sec = 0;
		ts.tv_nsec = (int)((double)(rand())/RAND_MAX * PRODUCER_TIME_BASE);
		pthread_delay_np(&ts);
	}
	return NULL;
}

static void *
consumer(void *p)
{
	struct timespec ts;
	int i = 0;
	while (i < ITERATIONS)
	{
		uint16_t n;
		if (!cblfU16Get(&cblfU16, &n))
		{
			printf("Consumer: %d\n", n);
			++i;
		}
		else
			printf("Consumer unable to get!\n");

		ts.tv_sec = 0;
		ts.tv_nsec = (int)((double)(rand())/RAND_MAX * CONSUMER_TIME_BASE);
		pthread_delay_np(&ts);
	}
	return NULL;
}

int
main(int argc, char **argv)
{
	pthread_t pt, ct;

	cblfU16Setup(&cblfU16, BUFU16_SIZE);
	cblfU32Setup(&cblfU32);

	pthread_create(&pt, NULL, producer, NULL);
	pthread_create(&ct, NULL, consumer, NULL);

	pthread_join(pt, NULL);
	pthread_join(ct, NULL);

	return 0;
}
