/** @file
	Single-producer, single-consumer lock-free circular buffer support using
	unallocated slot.

	This file works like a template for Cblf objects and functions. To use it,
	declare cblf_TYPE and cblf_NAME to the buffer type of object and name
	stem, respectively, and include this file once.

	@author pbleyer
*/

#ifndef UTIL_CBLF_H_
#define UTIL_CBLF_H_

/** @defgroup cblfdef Cblf definitions */
///@{

#define cblf_CAT2(x,y) x##y
#define cblf_CAT3(x,y,z) x##y##z
#define cblf_TPLT(x,y) cblf_CAT2(x,y)
#define cblf_FUNC(x,y,z) cblf_CAT3(x,y,z)

/** Declare a stubbed-base circular buffer object type.
	@param o Object name
	@param n Size type
	@param a Atomic index type
	@param t Buffer type name
	@param l Buffer data array length
	@param p Optional struct attributes (eg for packing or aligning members)
*/
#define cblf_TYPEVAR(o,n,a,t,l,p) \
	typedef struct o o p; \
	struct o \
	{ \
		n size; \
		volatile a head; \
		volatile a tail; \
		t data[(l)]; \
	} p

/** Variable circular buffer static initialization. */
#define cblf_INITVAR(l, v) { (l), 0, 0, v }

/** Declare a fixed circular buffer object type.
	@param o Object name
	@param a Atomic index type
	@param t Buffer type name
	@param l Buffer data array length
	@param p Optional struct attributes (eg for packing or aligning members)
*/
#define cblf_TYPEFIX(o,a,t,l,p) \
	typedef struct o o p; \
	struct o \
	{ \
		volatile a head; \
		volatile a tail; \
		t data[(l)]; \
	} p

/** Fixed circular buffer static initialization. */
#define cblf_INITFIX(v) { 0, 0, v }

/** Build a stubbed-base circular buffer object in place (use as a type before variable name).
	@param n Size type
	@param a Atomic index type
	@param t Buffer type name
	@param l Buffer data array length
	@param p Optional struct attributes (eg for packing or aligning members)
*/
#define cblf_BUILD(n,a,t,l,p) \
	struct \
	{ \
		n size; \
		volatile a head; \
		volatile a tail; \
		t data[(l)]; \
	} p

///@}

#endif // UTIL_CBLF_H_


/** @defgroup cblfimp Cblf implementation */
///@{

#ifndef cblf_OPFX
#define cblf_OPFX Cblf ///< Default prefix for object names
#endif

#ifndef cblf_FPFX
#define cblf_FPFX cblf ///< Default prefix for module function names
#endif

#ifndef cblf_ATOM
// #warning "cblf_ATOM not defined, using default 'unsigned int'"
#define cblf_ATOM unsigned int
#endif

#ifndef cblf_INDX
#define cblf_INDX cblf_ATOM ///< Use atomic type as default for sizes & indexes
#endif

#ifndef cblf_KIND
// #warning "cblf_KIND not defined, using struct by default"
#define cblf_KIND struct
#endif

#ifndef cblf_TYPE
#error "cblf_TYPE not defined"
#endif

#ifndef cblf_NAME
#error "cblf_NAME not defined"
#endif

#ifndef cblf_SIZE
#define cblf_SIZE 1 ///< Data is stub by default
#elif (cblf_SIZE < 0)
#error "cblf_SIZE must be greater than 0, use 1 for stub data"
#endif

#ifndef cblf_CODE
#define cblf_CODE signed char ///< Type used for result codes
#endif

#ifndef cblf_USIZ
#define cblf_USIZ 1 ///< Size field included by default
#elif !cblf_USIZ && (cblf_SIZE == 1)
#error "Size field needed for stub data"
#endif

#define cblf_OBJ cblf_TPLT(cblf_OPFX, cblf_NAME)
#define cblf_FNC(x) cblf_FUNC(cblf_FPFX, cblf_NAME, x)
#if cblf_SIZE == 1 // Stubbed object, has runtime size
#define cblf_LEN(x) ((x)->size) ///< Use dynamic size
#define cblf_PTR void * ///< Object pointer
#else
#define cblf_LEN(x) cblf_SIZE ///< Use fixed size
#define cblf_PTR cblf_OBJ * ///< Object pointer
#endif

typedef cblf_KIND cblf_OBJ cblf_OBJ;
cblf_KIND cblf_OBJ
{
#if cblf_USIZ
	cblf_INDX size; ///< The circular buffer allocation size (for stubs)
#endif
	volatile cblf_ATOM head; ///< Buffer head, points to current item
	volatile cblf_ATOM tail; ///< Buffer tail, points to next empty slot
	cblf_TYPE data[cblf_SIZE]; ///< Data array of size 'size' including the slack slot
};

/** Step index. */
static __inline cblf_INDX
cblf_FNC(Next)(cblf_PTR p, cblf_INDX n)
{
	++n;
	return (n < cblf_LEN((cblf_OBJ *)p)) ? n : 0 /* Wrap around */;
}

/** Initialize a circular buffer object.
	@param c Pointer to circular buffer object
	@param l Circular buffer data allocation size, must be 1 or more
	@return 0 on success, negative on error
	@note Using size of 1 does not allow any storage due to the unallocated slot.
*/
static __inline cblf_CODE
#if cblf_SIZE == 1
cblf_FNC(Setup)(cblf_PTR p, cblf_INDX l)
#else
cblf_FNC(Setup)(cblf_PTR p)
#endif
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
#if cblf_SIZE == 1
	if (l < 1)
		return -1;
	c->size = l;
#elif cblf_USIZ
	c->size = cblf_SIZE;
#endif
	c->head = 0;
	c->tail = 0;
	return 0;
}

/** Get the circular buffer size */
static __inline cblf_INDX
cblf_FNC(Size)(cblf_PTR p)
{
	return cblf_LEN((cblf_OBJ *)p);
}

/** Get the circular buffer current number of items.
	@note Not lock-free, number of items may have changed after this call.
*/
static __inline cblf_INDX
cblf_FNC(Items)(cblf_PTR p)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	volatile cblf_INDX h = c->head, t = c->tail; // cache head, tail

	return (t < h) ? cblf_LEN(c) - (h - t) : t - h;
}

/** Get the circular buffer current available space.
	@note Not lock-free, available space may have changed after this call.
*/
static __inline cblf_INDX
cblf_FNC(Available)(cblf_PTR p)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	volatile cblf_INDX h = c->head, t = c->tail; // Cache head, tail

	return ((t < h) ? h - t : cblf_LEN(c) - (t - h)) - 1;
}

/** Clear a circular buffer. */
static __inline void
cblf_FNC(Clear)(cblf_PTR p)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	c->tail = c->head; // Lock-free if atomic
}

/** Check if circular buffer is empty (head == tail). */
static __inline cblf_CODE
cblf_FNC(Empty)(cblf_PTR p)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	return (c->head == c->tail);
}

/** Check if circular buffer is full ('next' tail is head). */
static __inline cblf_CODE
cblf_FNC(Full)(cblf_PTR p)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	cblf_INDX n = cblf_FNC(Next)(c, c->tail);
	return (n == c->head);
}

#ifndef cblf_NOBYVALUE
/** Add single element to the buffer by value. */
static __inline cblf_CODE
cblf_FNC(Put)(cblf_PTR p, const cblf_TYPE e)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	cblf_INDX n = cblf_FNC(Next)(c, c->tail);
	if (n == c->head) // Buffer is currently full
		return -1;
	c->data[c->tail] = e;
	c->tail = n; // Move tail (*atomic*)
	return 0;
}

/** Add (and overwrite) a single element into the buffer by value.
	@note This function needs locking or mutual exclusion in a multi-threaded environment.
	@return Non-zero if value was overwritten, zero otherwise
*/
static __inline cblf_CODE
cblf_FNC(Write)(cblf_PTR p, const cblf_TYPE e)
{
	cblf_CODE r = 0;
	cblf_OBJ *const c = (cblf_OBJ *)p;
	cblf_INDX n = cblf_FNC(Next)(c, c->tail);
	if (n == c->head) // Head reached, move it
	{
		c->head = cblf_FNC(Next)(c, c->head); // This violates lock-free condition
		r = 1; // Flag value was overwritten
	}
	c->data[c->tail] = e;
	c->tail = n;
	return r;
}
#endif // !cblf_NOBYVALUE

/** Add elements to the buffer.
	@return Number of elements successfully added to the buffer
*/
static __inline cblf_INDX
cblf_FNC(PutArray)(cblf_PTR p, const cblf_TYPE *e, cblf_INDX l)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	cblf_INDX i = 0;

	while (i < l)
	{
		cblf_INDX n = cblf_FNC(Next)(c, c->tail);
		if (n == c->head)
			break; // Unavailable
		c->data[c->tail] = e[i++];
		c->tail = n; // Move tail (*atomic*)
	}

	return i; // Number of elements added successfully
}

/** Add (and overwrite) elements to the buffer.
	@return Number of elements successfully added to the buffer
	@note Not lock-free since head is modified when overwriting values.
*/
static __inline cblf_INDX
cblf_FNC(WriteArray)(cblf_PTR p, const cblf_TYPE *e, cblf_INDX l)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	cblf_INDX i = 0;

	while (i < l)
	{
		cblf_INDX n = cblf_FNC(Next)(c, c->tail);
		if (n == c->head) // Head reached, move it
			c->head = cblf_FNC(Next)(c, c->head); // This violates lock-free condition
		c->data[c->tail] = e[i++];
		c->tail = n;
	}

	return i; // Number of elements added successfully
}

/** Peek first element from the buffer.
	@return 0 if data was available
*/
static __inline cblf_CODE
cblf_FNC(Peek)(cblf_PTR p, cblf_TYPE *e)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	if (c->head == c->tail) // Empty here
		return -1;
	else
	{
		*e = c->data[c->head];
		return 0; // Done
	}
}

/** Peek elements from the buffer.
	@return Number of available items
*/
static __inline cblf_INDX
cblf_FNC(PeekArray)(cblf_PTR p, cblf_TYPE *e, cblf_INDX l)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	cblf_INDX i = 0, h = c->head; // Cache current head

	while (i < l)
	{
		if (h == c->tail) // No more elements here
			break;
		e[i++] = c->data[h];
		h = cblf_FNC(Next)(c, h);
	}

	return i;
}

/** Get first element from the buffer. */
static __inline cblf_CODE
cblf_FNC(Get)(cblf_PTR p, cblf_TYPE *e)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;

	if (c->head == c->tail)
		return -1; // Empty
	if (e)
		*e = c->data[c->head];
	c->head = cblf_FNC(Next)(c, c->head); // Move head (*atomic*)
	return 0; // Done
}

/** Get elements from the buffer.
	@return Number of removed items
*/
static __inline cblf_INDX
cblf_FNC(GetArray)(cblf_PTR p, cblf_TYPE *e, cblf_INDX l)
{
	cblf_OBJ *const c = (cblf_OBJ *)p;
	cblf_INDX i = 0;

	while (i < l)
	{
		if (c->head == c->tail)
			break; // empty
		if (e)
			e[i] = c->data[c->head];
		c->head = cblf_FNC(Next)(c, c->head); // Move head (*atomic*)
		++i;
	}

	return i; // Number of elements removed
}

#undef cblf_OBJ
#undef cblf_FNC
#undef cblf_LEN
#undef cblf_PTR

#undef cblf_OPFX
#undef cblf_FPFX
#undef cblf_ATOM
#undef cblf_INDX
#undef cblf_KIND
#undef cblf_TYPE
#undef cblf_NAME
#undef cblf_SIZE
#undef cblf_USIZ
#undef cblf_CODE
#undef cblf_NOBYVALUE

///@}
