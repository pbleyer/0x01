/** @file
	Double-linked list implementation

	Nodes are structures that must conform to the DlstNode prototype
	by having the first member equal to the header.

	Note that no resource locking is done by these functions.

	By default dlst_ITEMS is 1 and the list will keep track of its number of items.

	@author pbleyer
*/

#ifndef UTIL_DLST_H_
#define UTIL_DLST_H_

/** @defgroup dlstdef Dlst definitions */
///@{

#ifndef dlst_ITEMS
#define dlst_ITEMS 1 ///< Use items field by default
#endif

#ifndef dlst_ITYPE
#define dlst_ITYPE unsigned int ///< Default items field type
#endif

/** Double-linked list header
	- _plst: Pointer to the previous node
	- _nlst: Pointer to the next node
*/
#define dlst_HEADER void *_plst; void *_nlst

/** Get previous node pointer */
#define dlst_PREV(ptr) ((DlstNode *)(ptr))->_plst

/** Get next node pointer */
#define dlst_NEXT(ptr) ((DlstNode *)(ptr))->_nlst

/** Default node initialization constant */
#define dlst_NODEINIT { NULL, NULL }

/** Default list initialization constant */
#if dlst_ITEMS
#define dlst_LISTINIT { NULL, NULL, 0 }
#else
#define dlst_LISTINIT { NULL, NULL }
#endif

#define dlst_REV ((char)0) ///< Reverse
#define dlst_FWD ((char)1) ///< Forward

#ifndef NULL
#define NULL ((void *)0)
#endif

///@}

/** @defgroup dlstimp Dlst implementation */
///@{

/** Generic node prototype */
typedef struct DlstNode DlstNode;
struct DlstNode
{
	DlstNode *_plst; ///< Pointer to the previous node
	DlstNode *_nlst; ///< Pointer to the next node
};

/** Double-linked list object */
typedef struct DlstList DlstList;
struct DlstList
{
	DlstNode *head; ///< Pointer to the head node
	DlstNode *tail; ///< Pointer to the tail node
#if dlst_ITEMS
	dlst_ITYPE items; ///< Number of items
#endif
};

/** Functor prototype
	@param node Pointer to the node
	@param arg User defined argument
	@return Value depends on usage context
*/
typedef int (*DlstFunctor)(void *node, void *arg);

/** Get previous node
	@param p Node pointer
	@return Previous node pointer
*/
static __inline void *
dlstPrev(void *p)
{
	return (p) ? (void *)dlst_PREV(p) : NULL;
}

/** Get next node
	@param p Node pointer
	@return Next node pointer
*/
static __inline void *
dlstNext(void *p)
{
	return (p) ? (void *)dlst_NEXT(p) : NULL;
}

/** Initialize a list procedurally
	@param l Pointer to list
	@return 0 on success
*/
static __inline int
dlstSetup(DlstList *l)
{
#if dlst_ITEMS
	l->items = 0;
#endif
	l->head = l->tail = NULL;
	return 0;
}

/** Clear a double-linked list
	@param l Pointer to list
	@return 0 on success
*/
static __inline int
dlstClear(DlstList *l)
{
	return dlstSetup(l);
}

/** Initialize a node procedurally
	@param p Pointer to node object
	@return 0 on success
*/
static __inline int
dlstNodeSetup(void *p)
{
	dlst_PREV(p) = NULL;
	dlst_NEXT(p) = NULL;
	return 0;
}

/** Count all items starting from the specified node
	@param p Pointer to start node
	@param d Search direction (forward if non-zero)
	@return Item count
*/
static __inline int
dlstNodeItems(void *p, char d)
{
	int r = 0;
	while (p)
	{
		++r;
		p = (d) ? dlst_NEXT(p) : dlst_PREV(p);
	}
	return r;
}

/** Get the list total number of items
	@param l Pointer to list
	@return Item count
*/
static __inline int
dlstItems(DlstList *l)
{
#if dlst_ITEMS
	return l->items;
#else
	return dlstNodeItems(l->head, dlst_FWD);
#endif
}

/** Check if double-linked list is empty
	@param l Pointer to list
	@return Non-zero if list is empty, 0 otherwise
*/
static __inline char
dlstEmpty(DlstList *l)
{
	return (l->head == NULL);
}

/** Add a node to the list before the specified node
	@param l Pointer to list
	@param p Pointer to reference node or NULL if pushing front
	@param n Pointer to new node
	@return Pointer to inserted node
*/
static __inline void *
dlstInsertBefore(DlstList *l, void *p, void *n)
{
	if (p == NULL) // Insert front
		p = l->head;

	if (p == NULL) // Empty list
		l->head = l->tail = n;
	else
	{
		DlstNode *t = dlst_PREV(p);

		dlst_PREV(n) = t;
		dlst_NEXT(n) = p;
		if (t == NULL) // Reference node was head
			l->head = n;
		else
			dlst_NEXT(t) = n;
		dlst_PREV(p) = n;
	}

#if dlst_ITEMS
	++l->items;
#endif

	return n;
}

/** Add a node to the front of the list (helper)
	@param l Pointer to list
	@param n Pointer to new node
	@return Pointer to inserted node
*/
static __inline void *
dlstInsertFront(DlstList *l, void *n)
{
	return dlstInsertBefore(l, NULL, n);
}

/** Add a node to the list after the specified node
	@param l Pointer to list
	@param p Pointer to reference node or NULL if pushing back
	@param n Pointer to new node
	@return Pointer to inserted node
*/
static __inline void *
dlstInsertAfter(DlstList *l, void *p, void *n)
{
	if (p == NULL) // Insert back
		p = l->tail;

	if (p == NULL) // Empty list
		l->head = l->tail = n;
	else
	{
		DlstNode *t = dlst_NEXT(p);

		dlst_PREV(n) = p;
		dlst_NEXT(n) = t;
		if (t == NULL) // Reference node was tail
			l->tail = n;
		else
			dlst_PREV(t) = n;
		dlst_NEXT(p) = n;
	}

#if dlst_ITEMS
	++l->items;
#endif

	return n;
}

/** Add a node to the back of the list (helper)
	@param l Pointer to list
	@param n Pointer to new node
	@return Pointer to inserted node
*/
static __inline void *
dlstInsertBack(DlstList *l, void *n)
{
	return dlstInsertAfter(l, NULL, n);
}

/** Remove node from list
	@param l Pointer to list
	@param r Pointer to node to remove
	@return Pointer to removed node
*/
static __inline void *
dlstRemove(DlstList *l, void *n)
{
	if (n)
	{
		DlstNode *p = dlst_PREV(n), *q = dlst_NEXT(n);

		if (p == NULL)
			l->head = q;
		else
			dlst_NEXT(p) = q;

		if (q == NULL)
			l->tail = p;
		else
			dlst_PREV(q) = p;

	#if dlst_ITEMS
		--l->items;
	#endif
	}

	return n;
}

/** Search for node starting from the specified node
	@param p Pointer to the start node
	@param f Search function pointer (returns non-zero on success)
	@param a Search function user argument
	@param d Search direction (forward if non-zero)
	@return Node pointer on success, NULL otherwise
*/
static __inline void *
dlstSearch(void *p, DlstFunctor f, void *a, char d)
{
	while (p)
	{
		if (f(p, a)) // Functor true, node found
			break;
		p = (d) ? dlst_NEXT(p) : dlst_PREV(p);
	}

	return p;
}


/** Locate a node by position from current node
	@param p Pointer to the start node
	@param i Location index
	@param n Pointer reference to the searched node, if found
	@param d Search direction (forward if non-zero)
	@return Item index of location, or last node index if list is shorter (n will be NULL)
*/
static __inline int
dlstLocate(void *p, int i, void **n, char d)
{
	int j = 0;

	while (p && (j < i))
	{
		p = (d) ? dlst_NEXT(p) : dlst_PREV(p);
		++j;
	}

	*n = p;

	return j;
}

/** Apply mapping function to nodes, starting from the specified node
	@param p Pointer to the start node
	@param f Functor pointer
	@param a Functor user argument
	@param n If not NULL, pointer reference to the first node where functor returns non-zero
	@param d Search direction (forward if non-zero)
	@return Last functor result
	@note If node pointer reference not provided, functor is executed for all nodes regardless of functor return value
*/
static __inline int
dlstApply(void *p, DlstFunctor f, void *a, void **n, char d)
{
	int r = 0;

	while (p)
	{
		DlstNode *t = (d) ? dlst_NEXT(p) : dlst_PREV(p); // In case functor alters node
		r = f(p, a);
		if (r && n) // Result non-zero and pointer reference provided
		{
			*n = p;
			break;
		}
		p = t;
	}

	return r;
}

///@}

#endif // UTIL_DLST_H_
