/** @file
	Directed graph utility using node adjacency lists.
*/

#ifndef GRAPH_H_
#define GRAPH_H_

/** Default namespace */
#ifndef graph_NAMESPACE
#define graph_NAMESPACE graph
#endif

#include <string>
#include <iostream>
#include <list>
#include <unordered_map>
#include <algorithm>
#include <array>
#include <cstdint>

namespace graph_NAMESPACE {

/** The graph type
	@param IdType Type of the node ids, used to identify and search nodes
	@param GraphDataType Graph data field type, used to store graph information
	@param NodeDataType Node data field type, used to store node information
	@param EdgeDataType Edge data field type, used to store edge information
*/
template <
	typename IdType = unsigned,
	typename GraphDataType = void *,
	typename NodeDataType = void *,
	typename EdgeDataType = void *>
class graph_t
{
public:
	/** Internal enumerations */
	enum enum_t: uint8_t
	{
		Source = (1<<0), ///< Specify source node
		Target = (1<<1), ///< Specify target node
		Both = Source|Target, ///< Specify source or target node
		SrcPtr = (1<<2), ///< Specify source node pointer
		TgtPtr = (1<<3), ///< Specify target node pointer
		BothPtr = SrcPtr|TgtPtr, ///< Specify both node pointers
	};

	using id_t = IdType;
	using gdata_t = GraphDataType;
	using ndata_t = NodeDataType;
	using edata_t = EdgeDataType;

	/** Edge type
		Stores the Target node id that this edge points to, and the data associated with the edge.
		The source node id is not stored since the adjacency lists are organized by the source node.
	*/
	struct edge_t
	{
		id_t target; ///< Target node id
		edata_t data; ///< Per-edge data
		// node_t *node; ///< Target node pointer?
	};

	using edges_t = std::list<edge_t>; ///< Edges container (adjacency list)

	/** Node type
		Stores the data associated with the node, and the adjacency list with the
		target nodes and edge data.
	*/
	struct node_t
	{
		ndata_t data; ///< Per-node data
		edges_t edges; ///< Node adjacency list (targets)

		/** Shortcut operator to assign node data
			@param d New node data
			@return Node reference
		*/
		inline node_t &operator=(const ndata_t &d)
		{
			data = d;
			return *this;
		}

		/** Find edge in adjacency list (const)
			@param t Target node id to search
			@return Edge iterator (const)
		*/
		inline typename edges_t::const_iterator find(const id_t &t) const
		{
			return std::find_if(edges.begin(), edges.end(),
				[&](const edge_t &e) { return e.target == t; }
			);
		}

		/** Find edge in adjacency list
			@param t Target node id to search
			@return Edge iterator
		*/
		inline typename edges_t::iterator find(const id_t &t)
		{
			return std::find_if(edges.begin(), edges.end(),
				[&](const edge_t &e) { return e.target == t; }
			);
		}

		/** Map target id to edge pointer (const)
			@param t Target node id
			@return Edge pointer if found, nullptr otherwise
		*/
		inline const edge_t *edge(const id_t &t) const
		{
			auto ei = find(t);
			return (ei == edges.end()) ? nullptr : &(*ei);
		}

		/** Map target id to edge pointer
			@param t Target node id
			@return Edge pointer if found, nullptr otherwise
		*/
		inline edge_t *edge(const id_t &t)
		{
			return const_cast<edge_t *>(static_cast<const node_t *>(this)->edge(t));
		}

		/** Edge data access operator
			Will add an edge with target node id to the adjacency list if missing.
			@param t Target node id
			@return Data reference
		*/
		inline edata_t &operator[](const id_t &t)
		{
			auto ei = find(t);
			if (ei != edges.end()) // Found
				return ei->data;

			edges.emplace_back(edge_t{t, edata_t()}); // Not found, add it
			return edges.back().data;
		}

		/** Add edge or replace edge data to adjacency list
			@param t Target node id
			@param d Edge data
			@return True if new edge was created, false if only edge data was replaced
		*/
		inline bool link(const id_t &t, const edata_t &d = edata_t())
		{
			auto ei = find(t);
			if (ei == edges.end()) // Not found
			{
				edges.emplace_back(edge_t{t, d}); // Add new edge
				return true; // New edge created
			}
			else
			{
				ei->data = d; // Replace data
				return false; // Node data replaced
			}
		}

		/** Link using edge object instead
			@param e Edge information
			@return True if new edge was created, false if only edge data was replaced
		*/
		inline bool link(const edge_t &e) { return link(e.target, e.data); }

		/** Link operator
			@param e Edge information
			@return Node reference, in case operations are concatenated
		*/
		inline node_t& operator+=(const edge_t &e)
		{
			link(e);
			return *this;
		}

		/** Link operator with id (empty edge data)
			@param t Target node id
			@return Node reference, in case operations are concatenated
		*/
		inline node_t& operator+=(const id_t &t)
		{
			link(t);
			return *this;
		}

		/** Remove edge
			@param t Target node id
			@return True if node was found and removed, false otherwise
		*/
		inline bool unlink(const id_t &t)
		{
			auto ei = find(t);
			if (ei == edges.end()) // Not found
				return false;

			edges.erase(ei); // Found, remove
			return true;
		}

		/** Unlink operator
			@param t Target node id
			@return Node reference, in case operations are concatenated
		*/
		inline node_t& operator-=(const id_t &t)
		{
			unlink(t);
			return *this;
		}
	};

	using nodes_t = std::unordered_map<id_t, node_t>; ///< The node container, maps node ids to respective nodes

protected:
	gdata_t _data; ///< Graph data
	nodes_t _nodes; ///< Graph nodes

public:
	/** Default empty constructor */
	graph_t() = default;

	/** Default empty constructor */
	graph_t(const graph_t &g) = default;

	/** Move constructor */
	graph_t(graph_t &&g) = default;

	/** Create a graph from data and a nodes map
		@param d Data value
		@param n Nodes map
	*/
	graph_t(const gdata_t &d, const nodes_t &n = nodes_t()): _data(d), _nodes(n) { }
	// graph_t(nodes_t n): _nodes(std::move(n)) { }

	/** Create a graph from a nodes map and empty data
		@param n Nodes map
	*/
	graph_t(const nodes_t &n): _nodes(n) { }

	/** Destructor */
	virtual ~graph_t() = default;

	/** Copy idiom
		@param g Graph to copy
	*/
	inline void copy(const graph_t &g)
	{
		if (this != &g)
			_nodes = g._nodes;
	}

	/** Clone idiom
		@return New cloned graph pointer
	*/
	inline graph_t *clone() const { return new graph_t(*this); }

	/** Copy operator
		@param g Graph to copy
		@return Graph reference
	*/
	inline graph_t &operator=(const graph_t &g)
	{
		copy(g);
		return *this;
	}

	/** Move operator
		@param g Graph to copy
		@return Graph reference
	*/
	inline graph_t &operator=(graph_t &&g) = default;

	/** Access data */
	inline gdata_t &data() { return _data; }

	/** Access data (const) */
	inline const gdata_t &data() const { return _data; }

	/** Set data */
	inline void setData(const gdata_t &d) { _data = d; }

	/** Access nodes */
	inline nodes_t &nodes() { return _nodes; }

	/** Access nodes (const) */
	inline const nodes_t &nodes() const { return _nodes; }

	/** Set nodes */
	inline void setNodes(const nodes_t &n) { _nodes = n; }

	/** Add node using node object
		@param s Source node id
		@param n Node object
	*/
	inline void add(const id_t &s, const node_t &n) { _nodes[s] = n; }

	/** Add node
		@param s Source node id
		@param d Node data
		@param e Adjacency list (default empty)
	*/
	inline void add(const id_t &s, const ndata_t &d = ndata_t(), const edges_t &e = edges_t())
	{
		add(s, node_t{d, e});
	}

	/** Shortcut operator to add empty node
		@param s Source node id
		@return Graph reference, in case operations are concatenated
	*/
	inline graph_t& operator+=(const id_t &s)
	{
		add(s);
		return *this;
	}

	/** Clear graph */
	inline void clear() { _nodes.clear(); }

	/** Add node and edge, or replace node and edge data
		@pre Assumes target node exists
		@param s Source node id
		@param t Target node id
		@param n Node data
		@param e Edge data
		@return True if new node or edge were added, false if only data was replaced
	*/
	inline bool link(const id_t &s, const id_t &t, const ndata_t &n, const edata_t &e)
	{
		auto ni = _nodes.find(s);
		if (ni == _nodes.end()) // Node not found, add it
		{
			_nodes[s] = node_t{n, edges_t{edge_t{t, e}}};
			return true;
		}
		else // Found
		{
			ni->second.data = n; // Replace data
			return ni->second.link(t, e);
		}
	}

	/** Add node and edge, or replace edge data
		Node data will be default if node missing, or untouched otherwise.
		@pre Assumes target node exists
		@param s Source node id
		@param t Target node id
		@param e Edge data
		@return True if new node or edge were added, false if only data was replaced
	*/
	inline bool link(const id_t &s, const id_t &t, const edata_t &e = edata_t())
	{
		auto ni = _nodes.find(s);
		if (ni == _nodes.end()) // Node not found, add it
		{
			_nodes[s] = node_t{ndata_t(), edges_t{edge_t{t, e}}};
			return true;
		}
		else // Found, link with target using provided edge data
			return ni->second.link(t, e);
	}

	/** Link source using edge information
		@param s Source node id
		@param e Edge data
		@return True if new node or edge were added, false if only data was replaced
	*/
	inline bool link(const id_t &s, const edge_t &e)
	{
		return link(s, e.target, e.data);
	}

	/** Map id to node pointer (const)
		Can be used to check if node exists.
		@param n Node id
		@return Node pointer if found, else nullptr
	*/
	inline const node_t *node(const id_t &n) const
	{
		auto ni = _nodes.find(n);
		return (ni == _nodes.end()) ? nullptr : &(ni->second);
	}

	/** Map id to node pointer
		@param n Node id
		@return Node pointer if found, else nullptr
	*/
	inline node_t *node(const id_t &n)
	{
		return const_cast<node_t *>(static_cast<const graph_t *>(this)->node(n));
	}

	/** Map node pointer to id */
	inline id_t id(const node_t *n, bool *ok = nullptr) const
	{
		auto ni = std::find_if(_nodes.begin(), _nodes.end,
			[&](const typename nodes_t::value_type &p) { return &p.second == n; }
		);

		bool f = ni != _nodes.end();
		if (ok)
			*ok = f;
		return f ? ni.first : id_t();
	}

	/** Node reference operator
		Will create a new node with specified id and default data if it is missing
		@param n Node id
		@return Node reference
	*/
	inline node_t &operator[](const id_t &n) { return _nodes[n]; }

	/** Map ids to edge pointer (const)
		@param s Source node id
		@param t Target node id
		@return Edge pointer if it exists, nullptr otherwise
	*/
	inline const edge_t *edge(const id_t &s, const id_t &t) const
	{
		auto ni = _nodes.find(s);
		if (ni == _nodes.end())
			return nullptr;

		return ni->second.edge(t);
	}

	/** Map ids to edge pointer
		@param s Source node id
		@param t Target node id
		@return Edge pointer if it exists, nullptr otherwise
	*/
	inline edge_t *edge(const id_t &s, const id_t &t)
	{
		return const_cast<edge_t *>(static_cast<const graph_t *>(this)->edge(s, t));
	}

	/** Remove node by id
		@param n Node id
		@param a Remove only first reference to node in adjacency lists if false (default), otherwise remove all
	*/
	inline void remove(const id_t &n, bool a = false)
	{
		_nodes.erase(n); // Remove from container

		// Remove node id in adjacency lists
		for (auto &i: _nodes)
		{
			auto &el = i.second.edges; // Node adjacency list

			if (!a) // Assume only one edge has node id as target
			{
				auto ei = std::find_if(el.begin(), el.end(),
				  [&](const edge_t &e) { return e.target == n; }
				);
				if (ei != el.end()) // Found, remove
					el.erase(ei);
			}
			else // Remove all
				el.remove_if(
					[&](const edge_t &e) { return e.target == n; }
				);
		}
	}

	/** Shortcut operator to remove node
		@param n Node id
		@return Graph reference
	*/
	inline graph_t& operator-=(const id_t &n)
	{
		remove(n);
		return *this;
	}

	/** Unlink edge
		@param s Source node id
		@param t Target node id
		@return True if edge found and removed, false otherwise
	*/
	inline bool unlink(const id_t &s, const id_t &t)
	{
		auto ni = _nodes.find(s);
		if (ni == _nodes.end()) // Not found
			return false;
		return ni->second.unlink(t); // Found, unlink
	}

	/** Remove edges that have node as source and/or target
		@param n Node id
		@param f Specify source, target or both (default) kind of nodes
	*/
	inline void unlink(const id_t &n, enum_t f = Both)
	{
		for (auto &i: _nodes)
		{
			if ((f & Source) && (i.first == n))
				i.second.edges.clear(); // Remove all edges from source
			else if (f & Target) // Remove edges when target
				i.second.edges.remove_if(
					[&](const edge_t &e) { return e.target == n; }
				);
		}
	}

	/** Link type
		Used in function results to identify the source and target nodes by id,
		and point to the edge that links them.
	*/
	struct link_t
	{
		id_t source; ///< Source node
		edge_t *edge; ///< Pointer to edge with target node id and edge data
		node_t *srcptr; ///< Source pointer if requested, nullptr otherwise
		node_t *tgtptr; ///< Target pointer if requested, nullptr otherwise
	};

	using links_t = std::list<link_t>; ///< Links container

	/** Find edges that have node as source and/or target
		@param n Node id
		@param f Specify source, target or both (default) kind of nodes
		@return List with found links
	*/
	inline links_t links(const id_t &n, uint8_t f = Both) const
	{
		links_t r;

		for (auto &i: _nodes)
		{
			node_t *sp = (f & SrcPtr) ? const_cast<node_t *>(&i.second) : nullptr, *tp;
			edge_t *ep;

			if ((f & Source) && (i.first == n))
				for (auto &e: i.second.edges)
				{
					ep = const_cast<edge_t *>(&e);
					tp = (f & TgtPtr) ? ((e.target == n) ? sp : const_cast<node_t *>(node(e.target))): nullptr;
					r.emplace_back(link_t{n, ep, sp, tp});
				}
			else if (f & Target)
				for (auto &e: i.second.edges)
				{
					bool ts = false; // Flag target pointer set
					if (e.target == n)
					{
						if (!ts)
						{
							tp = (f & TgtPtr) ? ((i.first == n) ? sp : const_cast<node_t *>(node(e.target))): nullptr;
							ts = true;
						}
						ep = const_cast<edge_t *>(&e);
						r.emplace_back(link_t{i.first, ep, sp, tp});
					}
				}
		}

		return r;
	}
};

} // namespace graph_NAMESPACE


#endif // GRAPH_H_



//	struct edge_t
//	{
//		id_t source; ///< Source node id
//		id_t target; ///< Target node id
//		edata_t data; ///< Per-edge data

//		edge_t(): source(), target() { }
//		edge_t(const id_t &s, const id_t &t, const edata_t &d = edata_t()): source(s), target(t), data(d) { }
//		// edge_t(const edge_t &e): source(e.source), target(e.target), data(e.data) { }
//		// edge_t(edge_t &&e): source(std::move(e.source)), target(std::move(e.target)), data(std::move(e.data)) { }

//		/** Basic edges are the same if they connect same source and target too */
//		bool operator==(const edge_t &e) { return source == e.source && target == e.target; }
//		edge_t &operator=(const edata_t &d)
//		{
//			data = d;
//			return *this;
//		}
//	};
