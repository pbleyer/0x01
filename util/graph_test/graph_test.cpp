#include "graph.h"

#include <string>

using namespace graph;

using Graph = graph_t<unsigned, std::string, std::string, std::string>;
using Node = Graph::node_t;
using Edge = Graph::edge_t;
using Nodes = Graph::nodes_t;
using Edges = Graph::edges_t;
using Link = Graph::link_t;
using Links = Graph::links_t;

void
printNode(const Nodes::value_type &p)
{
	std::cout << "Node: " << p.first << " [" << p.second.data << "]" << std::endl;
	for (auto const &e: p.second.edges)
	{
		std::cout << "  " << p.first << "->";
		std::cout << e.target << " [" << e.data << "]" << std::endl;
	}
}

void
printGraph(const Graph &g)
{
	std::cout << "Graph = " << g.data() << std::endl;
	for (auto const &n: g.nodes())
		printNode(n);
	std::cout << std::endl;
}

void
printLink(const Link &l)
{
	std::cout << "Link: " << l.source << ":" << std::hex << uintptr_t(l.srcptr);
	std::cout << "->" << l.edge->target << ":" << std::hex << uintptr_t(l.tgtptr);
	std::cout << " [" << l.edge->data << "]" << std::endl;
}

void
printMatrix(const Graph &g)
{

}


int
main(int argc, char **argv)
{
	Edges e[] =
	{
		/* 0-> */ { {1, "0->1"}, {2, "0->2"} },
		/* 1-> */ { {0, "1->0"}, {2, "1->2"} },
		/* 2-> */ { {2, "2->2"} }
	};
	Nodes n
	{
		{ 0, {"nil", e[0]} },
		{ 1, {"one", e[1]} },
		{ 2, {"two", e[2]} },
		{ 9, {} }
	};

	Graph gg;
	gg = Graph("first graph", {{ 10, {"0xa"}}});
	printGraph(gg);

	Graph g(n);

	printGraph(g);

	g.add(3, "three");
	g.add(4, {"four", { {1, "4->1"} } } );
	g += 8;

	g.link(3, 2, "3->2");
	g.link(3, 4, "3->4");

	printGraph(g);

	g[0].link(3, "0->3");
	g[0].link({4, "0->4"});
	g[1] += {4, "1->4"};
	g[1] += 5;

	g[5] = "five";
	g[0] = "zero";
	g[6] = Node{"six"};

	printGraph(g);

	auto l = g.links(2, Graph::Target|Graph::BothPtr);
	std::cout << "Targets node 2:" << std::endl;
	for (auto &i: l)
		printLink(i);
	std::cout << std::endl;

	std::cout << "Unlinking 0->3, 3->2, 1 as target:" << std::endl;
	g[0] -= 3;
	g.unlink(3, 2);
	g.unlink(1, Graph::Target);
	printGraph(g);

	std::cout << "Removing nodes 0, 2" << std::endl;
	g.remove(0);
	g.remove(2);

	printGraph(g);

	std::cout << "Clearing graph" << std::endl;
	g.clear();

	g.link(0, 1, "0->1");
	g.link(1, 2, "uno", "1->2"); // Note node 2 does not currently exist
	g[0] = "zero";
	g[0][2] = "0->2";

	std::cout << (g.node(2) ? "Node 2 exists" : "Node 2 missing") << std::endl;

	g[1] = "uno";
	g[2] = "dos";
	g[1][2] = "1->2";

	std::cout << (g.node(2) ? "Node 2 exists" : "Node 2 missing") << std::endl;

	printGraph(g);
}

