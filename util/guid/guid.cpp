#include "guid.h"

#include <iostream>

#define USE_ALT 0
#define USE_QT 1
#define USE_WIN 2

#ifndef IMPL
#if defined(QT_CORE_LIB) // We have Qt
#define IMPL USE_QT
#elif defined(_WIN32) || defined(WIN32)
#define IMPL USE_WIN
#else
#define IMPL USE_ALT
#endif
#endif

#if IMPL == USE_QT
#include <QUuid>
#elif IMPL == USE_WIN
#include <rpc.h>
#elif IMPL == USE_ALT
#include <stdlib.h> /** @todo Add more implementations */
#else
#error "No UUID implemetation found."
#endif

#define _byte(x,p) uint8_t(((x)>>(8*(p))) & 0xff)

namespace guid_NAMESPACE {

#if IMPL == USE_QT
static void
_copyGuid(guid_t::value_t &g, const QUuid &q)
{
	g[0] = _byte(q.data1, 0);
	g[1] =	_byte(q.data1, 1);
	g[2] =	_byte(q.data1, 2);
	g[3] =	_byte(q.data1, 3);
	g[4] =	_byte(q.data2, 0);
	g[5] =	_byte(q.data2, 1);
	g[6] =	_byte(q.data3, 0);
	g[7] =	_byte(q.data3, 1);
	g[8] =	q.data4[0];
	g[9] =	q.data4[1];
	g[10] =	q.data4[2];
	g[11] =	q.data4[3];
	g[12] =	q.data4[4];
	g[13] =	q.data4[5];
	g[14] =	q.data4[6];
	g[15] =	q.data4[7];
}
#elif IMPL == USE_WIN
static void
_copyGuid(guid_t::value_t &g, const UUID &u)
{
	g[0] =  _byte(u.Data1, 0);
	g[1] =	_byte(u.Data1, 1);
	g[2] =	_byte(u.Data1, 2);
	g[3] =	_byte(u.Data1, 3);
	g[4] =	_byte(u.Data2, 0);
	g[5] =	_byte(u.Data2, 1);
	g[6] =	_byte(u.Data3, 0);
	g[7] =	_byte(u.Data3, 1);
	g[8] =	u.Data4[0];
	g[9] =	u.Data4[1];
	g[10] =	u.Data4[2];
	g[11] =	u.Data4[3];
	g[12] =	u.Data4[4];
	g[13] =	u.Data4[5];
	g[14] =	u.Data4[6];
	g[15] =	u.Data4[7];
}
#endif

static void
_createGuid(guid_t::value_t &g)
{
#if IMPL == USE_QT
	_copyGuid(g, QUuid::createUuid());

#elif IMPL == USE_WIN
	UUID u;
	UuidCreate(&u);
	_copyGuid(g, u);

#elif IMPL == USE_ALT
	/** @todo Improve */
	for (auto &i: g)
		i = (uint8_t)rand();
#endif
}

guid_t::guid_t(bool r)
{
	if (r)
		_createGuid(_value);
	else
		_value.fill(0);
}

guid_t::guid_t(const char *s)
{
#if IMPL == USE_QT
	_copyGuid(_value, QUuid(s));

#elif IMPL == USE_WIN
	UUID u = {0};
	UuidFromStringA((RPC_CSTR)s, &u);
	_copyGuid(_value, u);

#elif IMPL == USE_ALT
	/** @todo */
#endif
}

std::string guid_t::str() const
{
#if IMPL == USE_QT
	QUuid q(
		(_value[3]<<24) | (_value[2]<<16) | (_value[1]<<8) | _value[0],
		(_value[5]<<8) | _value[4],
		(_value[7]<<8) | _value[6],
		_value[8], _value[9],	 _value[10], _value[11], _value[12], _value[13], _value[14], _value[15]
	);

	return q.toString().toUtf8().constData();

#elif IMPL == USE_WIN
	UUID u = {
		uint32_t((_value[3]<<24) | (_value[2]<<16) | (_value[1]<<8) | _value[0]), // Data1
		uint16_t((_value[5]<<8) | _value[4]), // Data2
		uint16_t((_value[7]<<8) | _value[6]), // Data3
		{
			_value[8], _value[9],	 _value[10], _value[11], _value[12], _value[13], _value[14], _value[15]
		} // Data 4
	};
	RPC_CSTR s;
	UuidToStringA(&u, &s);
	std::string r((const char *)s);
	RpcStringFree(&s);
	return r;

#elif IMPL == USE_ALT
	char buf[37] = {0};
	sprintf(buf, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",
    _value[0], _value[1], _value[2], _value[3], _value[4], _value[5], _value[6], _value[7],
    _value[8], _value[9], _value[10], _value[11], _value[12], _value[13], _value[14], _value[15]
	);
	return std::string(buf);

#else
	return "";
#endif
}

} // namespace guid_NAMESPACE
