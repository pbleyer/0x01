/** @file
	A UUID wrapper.
*/

#ifndef GUID_H_
#define GUID_H_

#ifndef guid_NAMESPACE
#define guid_NAMESPACE guid
#endif

#include <array>
#include <cstdint>
#include <string>
#include <utility>

namespace guid_NAMESPACE {

class guid_t
{
public:
	using value_t = std::array<uint8_t, 16>;

public:
	/** Default constructor, null guid */
	guid_t(): _value{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} { }

	/** Create guid from a raw value. */
	guid_t(value_t a): _value(std::move(a)) { }

	/** Create a null(false) or random(true) guid. */
	explicit guid_t(bool r);

	/** Initialize guid from c string. */
	explicit guid_t(const char *s);

	/** Initialize guid from string. */
	explicit guid_t(const std::string &s) { *this = guid_t(s.c_str()); }

	/** Set guid to null. */
	inline void clear() { _value.fill(0); }

	/** Regenerate guid. */
	inline void reset() { *this = guid_t(true); }

	inline const value_t& value() const { return _value; }
	inline value_t& value() { return _value; }

	inline const uint8_t &operator[](std::size_t p) const { return _value[p]; }
	inline uint8_t &operator[](std::size_t p) { return _value[p]; }

	/** Get standard guid representation. */
	std::string str() const;

	inline bool operator==(const guid_t &g) const { return _value == g._value; }
	inline bool operator!=(const guid_t &g) const { return !(*this == g); }

	inline operator bool() const
	{
		for (auto &i: _value)
			if (i)
				return true; // Not zero
		return false; // All zeroes
	}

protected:
	value_t _value;
};

} // namespace guid_NAMESPACE

#endif // GUID_H_
