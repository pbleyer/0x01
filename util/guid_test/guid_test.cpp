
#include <guid.h>

#include <iostream>

using guid_t = guid::guid_t;

int
main(int argc, char **argv)
{
	guid_t a;
	std::cout << "a = " << a.str() << (a ? " (ok)" : " (!ok)") << std::endl;

	guid_t b("12345678-abcd-0000-ffff-123456789abc");

	std::cout << "b = " << b.str() << (b ? " (ok)" : " (!ok)") << std::endl;

	a = b;
	std::cout << "a = " << a.str() << (a ? " (ok)" : " (!ok)") << std::endl;

	std::cout << ((a == b) ? "a == b" : "a != b") << std::endl;

	b.clear();
	std::cout << "b = " << b.str() << (b ? " (ok)" : " (!ok)") << std::endl;

	std::cout << ((a == b) ? "a == b" : "a != b") << std::endl;

	a.reset();
	std::cout << "a = " << a.str() << (a ? " (ok)" : " (!ok)") << std::endl;

	return 0;
}