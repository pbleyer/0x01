#include "jam.h"


// Format a single implicit element
static int
formatImp(unsigned bl, uint8_t *b, const Jam &k, const void *o)
{
	if (!o) // Use jam internal value if external pointer not provided
		o = k.value;

	uint8_t sz = extent(k.id);

	if (b && o)
	{
		if (bl < sz)
			return -1; // No space

		memcpy(b, o, sz);
	}

/*
	if (b && o)
	{
		switch (k.id & 0xf)
		{
		case U8: *(uint8_t *)b = *(const uint8_t *)o; break;
		case U16: *(uint16_t *)b = *(const uint16_t *)o; break;
		case U32: *(uint32_t *)b = *(const uint32_t *)o; break;
		case U64: *(uint64_t *)b = *(const uint64_t *)o; break;

		case I8: *(int8_t *)b = *(const int8_t *)o; break;
		case I16: *(int16_t *)b = *(const int16_t *)o; break;
		case I32: *(int32_t *)b = *(const int32_t *)o; break;
		case I64: *(int64_t *)b = *(const int64_t *)o; break;

		case C8: *(char *)b = *(const char *)o; break;
		case C16: *(uint16_t *)b = *(const uint16_t *)o; break;

		case F32: *(float *)b = *(const float *)o; break;
		case F64: *(double *)b = *(const double *)o; break;
		}
	}
*/
	return sz;
}

// Format explicit element header
static int
formatExpHdr(unsigned bl, uint8_t *b, const Jam &k)
{
	int r;
	Jam::id_info f;
	Jam::info(k.id, f);

	uint8_t tp = 1<<f.type, sz = 1<<f.size;

	r = sz + tp;

	if (b)
	{
		if (bl < r)
			return -1; // No space

		// Format type
		switch (tp)
		{
		case 1: *(uint8_t *)b = (uint8_t)k.type; break;
		case 2: *(uint16_t *)b = (uint16_t)k.type; break;
		case 4: *(uint32_t *)b = (uint32_t)k.type; break;
		default: break;
		}

		b += tp;

		// Format size
		switch (sz)
		{
		case 1: *(uint8_t *)b = (uint8_t)k.size; break;
		case 2: *(uint16_t *)b = (uint16_t)k.size; break;
		case 4: *(uint32_t *)b = (uint32_t)k.size; break;
		default: break;
		}
	}

	return r;
}

// Format a single explicit element
static int
formatExp(unsigned bl, uint8_t *b, const Jam &k, const void *o)
{
	// Format contents
	if (!o)
		o = k.value;

	if (b && o)
	{
		if (bl < k.size)
			return -1; // No space

		memcpy(b, o, k.size);
	}

	return k.size;
}

// Format an array header (append array number of items)
static int
formatArrayHdr(unsigned bl, uint8_t *b, const Jam &k)
{
	uint8_t ln = moxMshLength(k.id);
	ln = (ln == 0B) ? 0 : 1<<(ln-1);

	if (b)
	{
		if (bl < ln)
			return -1; // No space

		// Format length
		switch (ln)
		{
		case 1: *(uint8_t *)b = (uint8_t)k.length; break;
		case 2: *(uint16_t *)b = (uint16_t)k.length; break;
		case 4: *(uint32_t *)b = (uint32_t)k.length; break;
		default: break;
		}
	}

	return ln;
}

// Format an implicit array content
static int
formatArrayImplicit(unsigned bl, uint8_t *b, const Jam &k)
{
	int r;
	uint8_t sz = moxMshSize(k.id);
	sz = (sz == 8B) ? 8 : 1<<(sz-1);
	r = k.length * sz;

	if (b && k.value)
	{
		if (bl < r)
			return -1; // No space

		memcpy(b, k.value, r);

/*
		void *o = k.value;
		uint32_t m = k.length;
		int q = 0;
		while (m--)
		{
			q = formatImp(b, k, o);
			b += q;
			o = (uint8_t *)o + q;
		}
*/
	}

	return r;
}

// Format an explicit array content
static int
formatArrayExplicit(unsigned bl, uint8_t *b, const Jam &k)
{
	int r = k.size * k.length;

	if (b && k.value)
	{
		if (bl < r)
			return -1; // No space

		memcpy(b, k.value, r);

/*
		void *o = k.value;
		uint32_t m = k.length;
		int q = 0;
		while (m--)
		{
			q = formatExp(b, k, o);
			b += q;
			o = (uint8_t *)o + q;
		}
*/
	}

	return r;
}

// Format a generic array
static int
formatArray(unsigned bl, uint8_t *b, const Jam &k)
{
	int r = 0;
	int q = formatArrayHdr(bl, b, k);
	if (q < 0)
		return q; // Error
	r += q;

	if (moxMshKind(k.id) == Imp)
		q = formatArrayImplicit(bl - r, (b) ? b + r : b, k);
	else
	{
		q = formatExpHdr(bl - r, (b) ? b + r : b, k);
		if (q < 0)
			return q;
		r += q;
		q = formatArrayExplicit(bl - r, (b) ? b + r : b, k);
	}

	if (q < 0)
		return q; // Error
	r += q;

	return r;
}

int
Jam::format(unsigned bl, uint8_t *b, bool f) const
{
	int r = 0; // Track number of bytes written

	id_t d = id(); // Calculate object descriptor byte

	if (f && b)
	{
		if (bl < 1)
			return -1; // No space

		*b = d; // Add descriptor as prefix
		++r;
	}

	int q;

	switch (kind)
	{

	}


	if (moxMshLength(id) == 0B) // Single object
	{
		if (moxMshKind(id) == Imp) // Implicit
			q = formatImp(bl - r, (b) ? b + r : b, *this, NULL);
		else // Explicit
		{
			q = formatExpHdr(bl - r, (b) ? b + r : b, *this);
			if (q < 0)
				return q; // Error
			r += q;
			q = formatExp(bl - r, (b) ? b + r : b, *this, NULL);
		}
	}
	else // Array
		q = formatArray(bl - r, (b) ? b + r : b, *this);

	if (q < 0)
		return q; // Error
	r += q;

	return r;
}

// Parsing

int
parseImplicit(unsigned bl, const uint8_t *b, Jam &k, void *o)
{
	uint8_t sz = moxMshSize(k.id);
	sz = (sz == 8B) ? 8 : 1<<(sz-1);

	if (!o) // Use marshal value if external pointer not provided
		o = k.value;

	if (b && o)
	{
		if (bl < sz)
			return -1; // No space

		memcpy(o, b, sz);
	}


/*
	if (b && o)
	{
		switch ((Imp<<6)|(moxMshType(k.id)<<2)|(moxMshSize(k.id)))
		{
		case U8: *(uint8_t *)o = *(const uint8_t *)b; break;
		case U16: *(uint16_t *)o = *(const uint16_t *)b; break;
		case U32: *(uint32_t *)o = *(const uint32_t *)b; break;
		case U64: *(uint64_t *)o = *(const uint64_t *)b; break;

		case I8: *(int8_t *)o = *(const int8_t *)b; break;
		case I16: *(int16_t *)o = *(const int16_t *)b; break;
		case I32: *(int32_t *)o = *(const int32_t *)b; break;
		case I64: *(int64_t *)o = *(const int64_t *)b; break;

		case C8: *(char *)o = *(const char *)b; break;
		case C16: *(uint16_t *)o = *(const uint16_t *)b; break;

		case F32: *(float *)o = *(const float *)b; break;
		case F64: *(double *)o = *(const double *)b; break;
		}
	}
*/
	return sz;
}

int
parseExplicitHdr(unsigned bl, const uint8_t *b, Jam &k)
{
	int r;
	uint8_t tp = moxMshType(k.id), sz = moxMshSize(k.id);
	sz = (sz == 0B) ? 0 : 1<<(sz-1);
	tp = (tp == 0B) ? 0 : 1<<(tp-1);

	r = sz + tp;

	if (b)
	{
		if (bl < r)
			return -1; // No space

		switch (tp)
		{
		case 1: k.type = *(uint8_t *)b; break;
		case 2: k.type = *(uint16_t *)b; break;
		case 4: k.type = *(uint32_t *)b; break;
		default: break;
		}

		b += tp;

		// Format size
		switch (sz)
		{
		case 1: k.size = *(uint8_t *)b; break;
		case 2: k.size = *(uint16_t *)b; break;
		case 4: k.size = *(uint32_t *)b; break;
		default: break;
		}
	}

	return r;
}

int
parseExplicit(unsigned bl, const uint8_t *b, Jam &k, void *o)
{
	// Parse contents
	if (!o)
		o = k.value;

	if (b && o)
	{
		if (bl < k.size)
			return -1; // No space

		memcpy(o, b, k.size);

	}
	return k.size;
}

// Format an array header (append array number of items)
static int
parseArrayHdr(unsigned bl, const uint8_t *b, Jam &k)
{
	uint8_t ln = moxMshLength(k.id);
	ln = (ln == 0B) ? 0 : 1<<(ln-1);

	if (b)
	{
		if (bl < ln)
			return -1; // No space

		// Parse length
		switch (ln)
		{
		case 1: k.length = *(uint8_t *)b; break;
		case 2: k.length = *(uint16_t *)b; break;
		case 4: k.length = *(uint32_t *)b; break;
		default: break;
		}
	}

	return ln; // Bytes parsed
}

// Format an implicit array content
static int
parseArrayImplicit(unsigned bl, const uint8_t *b, Jam &k)
{
	int r;
	uint8_t sz = moxMshSize(k.id);
	sz = (sz == 8B) ? 8 : 1<<(sz-1);
	r = k.length * sz;

	if (b && k.value)
	{

		if (bl < r)
			return -1; // No space

		memcpy(k.value, b, r);

/*
		void *o = k.value;
		uint32_t m = k.length;
		int q = 0;
		while (m--)
		{
			q = parseImplicit(b, k, o);
			b += q;
			o = (uint8_t *)o + q;
		}
*/
	}

	return r;
}

// Format an explicit array content
static int
parseArrayExplicit(unsigned bl, const uint8_t *b, Jam &k)
{
	int r = k.size * k.length;
	uint32_t m = k.length;

	if (b && k.value)
	{
		if (bl < r)
			return -1; // No space

		memcpy(k.value, b, r);

/*
		void *o = k.value;
		uint32_t m = k.length;
		int q = 0;
		while (m--)
		{
			q = parseExplicit(b, k, o);
			b += q;
			o = (uint8_t *)o + q;
		}
*/
	}

	return r;
}

// Format a generic array
static int
parseArray(unsigned bl, const uint8_t *b, Jam &k)
{
	int r = 0;
	int q = parseArrayHdr(bl, b, k);
	if (q < 0)
		return q; // Error
	r += q;

	if (moxMshKind(k.id) == Imp)
		q = parseArrayImplicit(bl - r, (b) ? b + r : b, k);
	else
	{
		q = parseExplicitHdr(bl - r, (b) ? b + r : b, k);
		if (q < 0)
			return q; // Error
		r += q;
		q = parseArrayExplicit(bl - r, (b) ? b + r : b, k);
	}

	if (q < 0)
		return q; // Error
	r += q;

	return r;
}

int
Jam::parse(unsigned bl, const uint8_t *b, bool f)
{
	int r = 0;
	uint8_t d;

	if (f) // Get descriptor from stream
	{
		if (!b || bl < 1)
			return -1; // Byte stream must be provided
		d = *b++;
		id = d;
		++r;
	}
	else
		d = id;

	if (moxMshLength(d) == 0B) // Single object
	{
		if (moxMshKind(d) == Imp) // Implicit
			q = parseImplicit(bl - r, (b) ? b + r : b, *this, NULL);
		else // Explicit
		{
			q = parseExplicitHdr(bl - r, (b) ? b + r : b, *this);
			if (q < 0)
				return q; // Error
			r += q;
			q = parseExplicit(bl - r, (b) ? b + r : b, *this, NULL);
		}
	}
	else // Array
		q = parseArray(bl - r, (b) ? b + r : b, *this);

	if (q < 0)
		return q; // Error
	r += q;

	return r;
}

} // namespace mox

// #define _MOX_TEST_
#ifdef _MOX_TEST_

struct TestStruct
{
	uint8_t byte;
	char str[16];
	float flt;
};

static void
_printValue(const Jam &k)
{
	uint8_t sz = moxMshSize(k.id);
	void *o = k.value;
	unsigned i = 0;
	uint32_t ln = moxMshLength(k.id);
	sz = (sz == 8B) ? 8 : 1<<(sz-1);

	if (o)
	{
		while (1)
		{
			switch ((Imp<<6)|(moxMshType(k.id)<<2)|(moxMshSize(k.id)))
			{
			case U8: printf("%u", *(const uint8_t *)o); break;
			case U16: printf("%u", *(const uint16_t *)o); break;
			case U32: printf("%u", *(const uint32_t *)o); break;
			case U64: printf("%ull", *(const uint64_t *)o); break;

			case I8: printf("%d", *(const int8_t *)o); break;
			case I16: printf("%d", *(const int16_t *)o); break;
			case I32: printf("%d", *(const int32_t *)o); break;
			case I64: printf("%dll", *(const int64_t *)o); break;

			case C8: printf("%c", *(const char *)o); break;
			// case C16: printf("%s", *(const wchar_t*)o); break;

			case F32: printf("%g", *(const float *)o); break;
			case F64: printf("%g", *(const double *)o); break;
			default: printf("???");
			}

			o = (uint8_t *)o + sz;
			++i;
			if (ln == 0 || i >= k.length)
				break;
			else if (((moxMshType(k.id)<<2)|(moxMshSize(k.id))) != C8)
				printf(" ");
		}
	}
	else
		printf("null");
}

static void
_printMarshal(const Jam &k)
{
	char *s;
	uint8_t tp = moxMshType(k.id), sz = moxMshSize(k.id), ln = moxMshLength(k.id);
	uint8_t kn = moxMshKind(k.id);

	sz = (sz == 0B) ? 0 : 1<<(sz-1);
	tp = (tp == 0B) ? 0 : 1<<(tp-1);
	ln = (ln == Sng) ? 0 : 1<<(ln-1);

	printf("id: 0x%x (%s", k.id, (kn == Exp) ? "Exp" : "Imp");
	if (kn == Imp)
	{
		if (sz == 0)
			sz = 8;

		switch (tp)
		{
		case Uns: s = "Uns"; break;
		case Sig: s = "Sig"; break;
		case Flt: s = (sz < 4) ? "Chr" : "Flt"; break;
		default: s = "???";
		}
		printf(",%s,%dB)", s, sz);
		if (ln)
			printf(", length: %d(%dB)", k.length, ln);
		printf(", value: ");
		_printValue(k);
	}
	else
	{
		printf(",%dB,%dB)", tp, sz);
		printf(", type: 0x%x, size: %d", k.type, k.size);
		if (ln)
			printf(", length: %d(%dB)", k.length, ln);
	}
	printf("\n");
}


int
main(int argc, char **argv)
{
	uint8_t u8 = 0xca;
	uint16_t u16 = 0xcafe;
	uint32_t u32 = 0xdeadbeef;

	char msg[] = "This is the message";
	uint16_t arr[] = { 0x1234, 0x5678, 0xaa00, 0xf00d };

	struct TestStruct ts;
	uint8_t buf[256], mem[256];
	int r = 0,  l, m;
	Jam k;

	printf("Formatting\n");

	k.id = U8;
	k.value = &u8;
	_printMarshal(k);
	r += k.format(256-r, buf+r, Pfx);

	k.id = U16;
	k.value = &u16;
	_printMarshal(k);
	r += k.format(256-r, buf+r, Pfx);

	k.id = U32;
	k.value = &u32;
	_printMarshal(k);
	r += k.format(256-r, buf+r, Pfx);

	k = Jam(moxMshImp(C8,1B), 0, 0, strlen(msg), msg);
	_printMarshal(k);
	r += k.format(256-r, buf+r, Pfx);

	k = Jam(moxMshImp(U16,1B), 0, 0, 4, arr);
	_printMarshal(k);
	r += k.format(256-r, buf+r, Pfx);

	ts.byte = 0x55;
	strncpy(ts.str, "The string", 16);
	ts.flt = 3.14159f;

	k = Jam(moxMshExp(2B,1B,Sng), 0xbabe, sizeof(ts), 0, &ts);
	_printMarshal(k);
	r += k.format(256-r, buf+r, Pfx);

	printf("Parsing\n");
	m = 0, l = 0;
	k.value = mem;

	while (m < r)
	{
		l = k.parse(256-m, buf+m, Pfx);
		if (l)
		{
			_printMarshal(k);
			m += l;
		}
	}

	return m;
}

#endif // _MOX_TEST_
