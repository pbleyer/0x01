/** @file
	Jam data serialization.
*/

#ifndef JAM_H_
#define JAM_H_

/** Default namespace. */
#ifndef jam_NAMESPACE
#define jam_NAMESPACE jam
#endif

/** Object type used to store type/size/items field values. */
#ifndef jam_FIELDTYPE
#define jam_FIELDTYPE uintptr_t
#endif

/** Object type used to store size field values. */
#ifndef jam_SIZETYPE
#define jam_SIZETYPE uintptr_t
#endif

/** Memory allocation function (same prototype as malloc). */
#ifndef jam_ALLOC
#define jam_ALLOC malloc
#endif

/** Allocated memory free function (same prototype as free). */
#ifndef jam_FREE
#define jam_FREE free
#endif

/** Memory byte copy function (same prototype as memcpy). */
#ifndef jam_MEMCPY
#define jam_MEMCPY memcpy
#endif

#include <cstdint>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector>
#include <list>
#include <iostream>
#include <string>

namespace jam_NAMESPACE {

/** Object kind enumeration. */
enum kind_e: uint8_t
{
	// Object kind
	SngI = 0x0, ///< Singleton implicit.
	SngE, ///< Singleton explicit.
	Lst, ///< List.
	Map, ///< Map.
	Spc, ///< Special.
	Usr = 0x6, ///< User reserved.
	ArrI = 0x8, ///< Array implicit.
	ArrE, ///< Array explicit.
	Und = 0xff, ///< Undefined.

	// Aliases
	SingleImplicit = SngI,
	SingleExplicit = SngE,
	List = Lst,
	Special = Spc,
	User = Usr,
	ArrayImplicit = ArrI,
	ArrayExplicit = ArrE,
	Undefined = Und,
};

/** Object type enumeration. */
enum type_e: uint8_t
{
	// Implicit object size / Explicit object items/type field / Cardinality items (2 bits)
	b8 = 0x0, ///< 1-byte object, array with 1-byte size.
	b16 = 0x1, ///< 2-byte object, array with 2-byte size.
	b32 = 0x2, ///< 4-byte object, array with 4-byte size.
	b64 = 0x3, ///< 8-byte object, array with 8-byte size or embedded size.

	// Aliases
	Bits8 = b8,
	Bits16 = b16,
	Bits32 = b32,
	Bits64 = b64,

	// Implicit object type (2 bits)
	Uns = 0x0, ///< Unsigned integer.
	Sig = 0x1, ///< Signed integer.
	Flt = 0x2, ///< Floating point.
	Chr = Flt, ///< 8-bit or 16-bit char (string if array).
	// Bit = 0x3, ///< Bitfield object.
	Emb = 0x3, ///< Embedded value.

	// Aliases
	Unsigned = Uns,
	Signed = Sig,
	Float = Flt,
	Char = Chr,
	Embedded = Emb,

	// Special object ids
	SpcType = 0x7, ///< Type footprint or definition.

	// Type objects
	SpcTypeF = 0x0, ///< Type form footprint (serialization id array, no names).
	SpcTypeN, ///< Type definition (name-type map).
	SpcTypeNV, ///< Full type definition plus default values (name-type-value map).

	// Aliases
	SpecialTypeForm = SpcTypeF,
	SpecialTypeName = SpcTypeN,
	SpecialTypeNameValue = SpcTypeNV,

	// Implicit type field aliases
	U8 = (Uns<<2)|(b8), ///< uint8_t.
	U16 = (Uns<<2)|(b16), ///< uint16_t.
	U32 = (Uns<<2)|(b32), ///< uint32_t.
	U64 = (Uns<<2)|(b64), ///< uint64_t.

	I8 = (Sig<<2)|(b8), ///< int8_t.
	I16 = (Sig<<2)|(b16), ///< int16_t.
	I32 = (Sig<<2)|(b32), ///< int32_t.
	I64 = (Sig<<2)|(b64), ///< int64_t.

	C8 = (Chr<<2)|(b8), ///< ASCII (UTF-8).
	C16 = (Chr<<2)|(b16), ///< Wide char (UTF-16).

	F32 = (Flt<<2)|(b32), ///< Float.
	F64 = (Flt<<2)|(b64), ///< Double.

	T = (Emb<<2)|0x0, ///< True.
	F = (Emb<<2)|0x1, ///< False.
	X = (Emb<<2)|0x2, ///< Unknown.
	N = (Emb<<2)|0x3, ///< Null value.

	// Aliases
	Uint8 = U8,
	Uint16 = U16,
	Uint32 = U32,
	Uint64 = U64,
	Int8 = I8,
	Int16 = I16,
	Int32 = I32,
	Int64 = I64,

	True = T,
	False = F,
	Unknown = X,
	Null = N,

	// B1 = (Bit<<2)|0x0, ///< 1-bit (boolean).
	// B2 = (Bit<<2)|0x1, ///< 2-bit.
	// B4 = (Bit<<2)|0x2, ///< 4-bit (nibble).
	// B7 = (Bit<<2)|0x3, ///< 7-bit.
	// B8 = T, ///< 8-bit boolean.
};

/** Flags. */
enum flag_e: uint8_t
{
	// Object flags
	Bnd = 1<<0, ///< Bound object (object is owner of dynamical resources).
	Cst = 1<<1, ///< Object is constant (read-only).
	Fix = 1<<2, ///< Fixed size object (memory provided externally, not dynamic).

	// Aliases
	Bound = Bnd,
	Constant = Cst,
	Fixed = Fix,

	// Formatting flags
	Dsc = 1<<0, ///< Descriptor id byte.
	Inf = 1<<1, ///< Include object information (type, size, items).
	Val = 1<<2, ///< Include value.
	Nrc = 1<<3, ///< Non-recurse list/map traversal (default is recursive).
	Upd = 1<<4, ///< Inform list/map size is up to date.
	Tag = 1<<5, ///< Include name tag (in type).
	Skp = 1<<6, ///< Skip checks (eg for recursive calls).
	Hdr = Dsc|Inf, ///< Object header (descriptor byte + information fields) alias.

	// Aliases
	Descriptor = Dsc,
	Information = Inf,
	Value = Val,
	Nonrecurse = Nrc,
	Updated = Upd,
	Skip = Skp,
	Header = Hdr
};

/** Structure to store serialization id information. */
struct info_t
{
	uint8_t kind; ///< Object kind.
	uint8_t type; ///< Implicit type, explicit type span, special id, user id.
	uint8_t size; ///< Explicit item size span, list/map byte size span.
	uint8_t items; ///< Array number of items span.
};

/** Serialized element information. */
class jam_t
{
public:
	using id_t = uint8_t; ///< Serialization id byte.
	using field_t = jam_FIELDTYPE; ///< Size to store type/size/items field values.
	using size_t = jam_SIZETYPE; ///< Size field.

public:
	uint8_t kind; ///< Object kind.
	field_t type; ///< Implicit type / Explicit item type field value.
	mutable field_t size; ///< Item size field value.
	field_t items; ///< Array number of items field value.

#ifdef jam_DEBUG
	static int objects; ///< Track number of objects.
	static int allocs; ///< Track allocations.
#endif

protected:
	uint8_t flags; ///< Object control flags.

public:
	void *value; ///< Item value pointer (externally provided or dynamically allocated).
	size_t space; ///< Allocated space for value in bytes.

protected:
	/** Build object value.
		For a value that is not bounded, value pointer is passed through. Otherwise, new
		value is created.
		@param k Object kind.
		@param t Object type.
		@param s Object item size.
		@param n Object number of items.
		@param v Object external value pointer.
		@param f Object flags.
		@return Value pointer.
	*/
	static inline void *build(
		uint8_t k = SngI, field_t t = 0, field_t s = 0, field_t n = 0,
		const void *v = nullptr, uint8_t f = 0
	)
	{
		if (!(f & Bnd)) // External value object or type definition
			return const_cast<void *>(v);

		if (isImplicit(k)) // Implicit object
			s = width(uint8_t(t));

		if (isArray(k)) // Object is array
			s *= n; // Total bytes

		void *p = nullptr; // Return value

		if (k == SngI && s <= sizeof(void *)) // Enclosed object value in a void*
		{
			if (v)
			{
				p = &p; // Point to self

				switch (t)
				{
				case U8: *(uint8_t *)p = *(uint8_t *)v; break;
				case U16: *(uint16_t *)p = *(uint16_t *)v; break;
				case U32: *(uint32_t *)p = *(uint32_t *)v; break;
				case U64: *(uint64_t*)p = *(uint64_t *)v; break;
				case I8: *(int8_t *)p = *(int8_t *)v; break;
				case I16: *(int16_t *)p = *(int16_t *)v; break;
				case I32: *(int32_t *)p = *(int32_t *)v; break;
				case I64: *(int64_t *)p = *(int64_t *)v; break;
				case C8: *(uint8_t *)p = *(uint8_t *)v; break;
				case C16: *(uint16_t *)p = *(uint16_t *)v; break;
				case F32: *(float *)p = *(float *)v; break;
				case F64: *(double *)p = *(double *)v; break;
				case T: *(uint8_t *)p = 1; break;
				case F:
				case X:
				case N: *(uint8_t *)p = 0; break;
				default:
					p = nullptr;
					break;
				}
			}
		}
		else if (k == Lst || k == Map)
		{
			if (k == Map)
				n *= 2; // Double items

			jam_t *j = new jam_t[n];

#ifdef jam_DEBUG
			if (j)
				++allocs;
#endif

			if (v && j)
			{
				const jam_t *q = static_cast<const jam_t *>(v);
				for (size_t i = 0; i < n; ++i)
					j[i] = q[i];
			}

			p = static_cast<void *>(j);
		}
		else
		{
			p = jam_ALLOC(s);

			if (p)
			{
#ifdef jam_DEBUG
				++allocs;
#endif

				if (v)
					jam_MEMCPY(p, v, s);
				else
					std::fill_n((uint8_t *)p, s, 0);
			}
		}

		return p;
	}

	/** Build shortcut for singleton. */
	static inline void *build(uint8_t k, field_t t, const void *v, uint8_t f = 0)
	{
		return build(k, t, 0, 0, v, f);
	}

	/** Build shortcut for array. */
	static inline void *build(uint8_t k, field_t t, field_t n, const void *v, uint8_t f = 0)
	{
		return build(k, t, 0, n, v, f);
	}

	/** Build a jam list value. Assumes T is an iterable type. */
	template <typename T>
	static inline void *buildList(const T &l)
	{
		size_t n = l.size();
		jam_t *j = new jam_t[n];

		if (j)
		{
#ifdef jam_DEBUG
			++allocs;
#endif

			jam_t *p = j;

			for (auto &i: l)
				*p++ = i;
		}

		return static_cast<void *>(j);
	}

	/** Build a map value. Assumes T is a map type. */
	template <typename T>
	static inline void *buildMap(const T &m)
	{
		size_t n = m.size() * 2;
		jam_t *j = new jam_t[n];

		if (j)
		{
#ifdef jam_DEBUG
			++allocs;
#endif

			jam_t *p = j;

			for (auto &i: m)
			{
				*p++ = i.first;
				*p++ = i.second;
			}
		}

		return static_cast<void *>(j);
	}


public:
	/** Calculate span id field for value (b8-b64). */
	static inline id_t span(field_t v)
	{
		return (v <= UINT8_MAX) ? b8
		: (v <= UINT16_MAX) ? b16
		: (v <= UINT32_MAX) ? b32
		: b64;
	}

	/** Get number of bytes span. */
	static inline uint8_t spanBytes(field_t v)
	{
		return uint8_t(1<<span(v));
	}

	/** Get implicit type size in bytes. */
	static inline uint8_t width(uint8_t t)
	{
		if (t < (Emb<<2))
			return 1<<(t & 0x3);
		else
			return 0; // Embedded (value part of id)
	}

	/** Check if object type is embedded. */
	static inline bool isEmbedded(uint8_t t)
	{
		return (t >= (Emb<<2));
	}

	/** Check if object kind is valid. */
	static inline bool isKind(uint8_t k)
	{
		return (k <= ArrE && k != 0x5 && k != 0x7);
	}

	/** Check if object kind is implicit. */
	static inline bool isImplicit(uint8_t k)
	{
		return (k == SngI) || (k == ArrI);
	}

	/** Check if object kind is explicit. */
	static inline bool isExplicit(uint8_t k)
	{
		return (k == SngE) || (k == ArrE);
	}

	/** Check if object kind is singleton. */
	static inline bool isSingle(uint8_t k)
	{
		return k <= SngE; // SngI or SngE
	}

	/** Check if object kind is array. */
	static inline bool isArray(uint8_t k)
	{
		return (k & 0x8) ? true : false; // Check bit 7 is set
	}

	/** Check if implicit type can be enclosed inside void pointer. */
	static inline bool isEnclosed(uint8_t t)
	{
		uint8_t w = width(t);
		return (w > 0) && (w <= sizeof(void *));
	}

	/** Create a serialization id.
		@note Arguments have different meanings depending on object kind.
	*/
	static inline id_t id(uint8_t k, uint8_t a, uint8_t b = 0, uint8_t c = 0)
	{
		switch (k)
		{
		case SngI: // a:implicit type
		case SngE: // a:type span, b:size span
		case Lst: // a:items span, b:bytes span
		case Map: // a:items span, b:bytes span
			return (k<<4) | (b<<2) | (a<<0);
		case Spc: // a:special id, b:bytes span
		case Usr: // a:user id, b:bytes span (opt)
			return (k<<4) | (b<<3) | (a<<0);
		case ArrI: // a:items span, b:implicit type
		case ArrE: // a:items span, b:type span, c:size span
			return (k<<4) | (a<<5) | (c<<2) | (b<<0);
		default:
			return (SngI<<4)|X; // Unknown
		}
	}

	/** Get kind name. */
	static inline const char* kindName(uint8_t k)
	{
		switch (k)
		{
		case SngI: return "SngI";
		case SngE: return "SngE";
		case Lst: return "Lst";
		case Map: return "Map";
		case Spc: return "Spc";
		case Usr: return "Usr";
		case ArrI: return "ArrI";
		case ArrE: return "ArrE";
		case Und: return "Und";
		default: return "<?>";
		}
	}

	/** Get implicit type name. */
	static inline const char* typeName(uint8_t t)
	{
		switch (t)
		{
		case U8: return "U8";
		case U16 : return "U16";
		case U32 : return "U32";
		case U64 : return "U64";
		case I8 : return "I8";
		case I16 : return "I16";
		case I32 : return "I32";
		case I64 : return "I64";
		case C8 : return "C8";
		case C16 : return "C16";
		case F32 : return "F32";
		case F64 : return "F64";
		case True: return "True";
		case False: return "False";
		case Unknown: return "Unknown";
		case Null: return "Null";
		default: return "<?>";
		}
	}

	/** Explode object info from id. */
	static inline info_t info(id_t i)
	{
		info_t f;

		f.kind = i >> 4;

		if (f.kind > Map) // Not SngX, Lst or Map
		{
			if (isArray(f.kind)) // ArrX
				f.kind &= ArrE;
			else
				f.kind &= 0xe; // Spc or Usr
		}

		f.type = f.items = 0;

		switch (f.kind)
		{
		case ArrI:
			f.items = 1<<((i>>5) & 0x3); // Number of items span
		case SngI:
			f.type = (i>>0) & 0xf; // Implicit type
			f.size = width(f.type); // Implicit size in bytes
			break;

		case ArrE:
			f.items = 1<<((i>>5) & 0x3); // Number of items span
		case SngE:
			f.type = 1<<((i>>0) & 0x3); // Type span
			f.size = 1<<((i>>2) & 0x3); // Item size span
			break;

		case Lst:
		case Map:
			f.items = 1<<((i>>0) & 0x3); // Number of items span
			f.size = 1<<((i>>2) & 0x3); // Number of bytes span
			break;

		case Spc:
			f.type = (i>>0) & 0x7; // Special id
			f.size = 1<<((i>>3) & 0x3); // Number of bytes span
			break;

		case Usr:
			f.type = (i>>0) & 0x1f; // User-defined id
			f.size = 1<<((i>>3) & 0x3); // Number of bytes span
			break;
		}

		return f;
	}

	/** Read from buffer.
		@param b Buffer pointer. If null, will return number of bytes that would be read.
		@param o Offset.
		@param l Number of bytes to read.
		@param v Data pointer.
		@return Number of bytes actually read.
	*/
	static inline size_t read(const uint8_t *b, size_t o, size_t l, void *v)
	{
		if (b && v) // Only read if buffer is not null
			jam_MEMCPY(v, b + o, l);
		return l;
	}

	/** Write to buffer.
		@param b Buffer pointer. If null, will return number of bytes that would be written.
		@param o Offset.
		@param l Number of bytes to write.
		@param v Data pointer.
		@return Number of written bytes.
	*/
	static inline size_t write(uint8_t *b, size_t o, size_t l, const void *v)
	{
		if (b && v) // Write only if buffer is provided
			jam_MEMCPY(b + o, v, l);
		return l;
	}

public:
	/** Default constructor. */
	jam_t(): kind(Und), type(0), size(0), items(0), flags(0), value(nullptr)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

//	/** Full argument constructor. */
//	jam_t(kind_e k, field_t t, field_t s, void *v, field_t n, uint8_t f)
//		: kind(k), type(t), size(s), items(n), flags(f),
//		value(build(k, t, s, n, v, f))
//	{ }

	/** Constructor for bound implicit singleton, eg jam_t(U8). */
	jam_t(type_e t)
		: kind(SngI), type(t), size(0), items(0), flags(Bnd),
		value(build(SngI, t, nullptr, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for a generic singleton, eg jam_t(0xcafe, 8) or jam_t(0xcafe, 8, &extval).
		If value pointer provided is null, it will construct a bound object. Otherwise, the external value is used.
	*/
	jam_t(field_t t, field_t s, void *v = nullptr)
		: kind(SngE), type(t), size(s), items(0), flags((v == nullptr) ? Bnd : 0),
		value(build(SngE, t, s, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for an implicit bound array, eg jam_t(U16, 5). */
	jam_t(type_e t, field_t n, uint8_t f = 0)
	: kind(ArrI), type(t), size(0), items(n), flags(f | Bnd),
		value(build(ArrI, t, n, nullptr, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for an implicit array, eg jam_t(U16, &extval, 5). */
	jam_t(type_e t, void *v, field_t n, uint8_t f = 0)
	: kind(ArrI), type(t), size(0), items(n), flags(f | ((v == nullptr) ? Bnd : 0)),
		value(build(ArrI, t, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for an implicit array (const). */
	jam_t(type_e t, const void *v, field_t n, uint8_t f = 0)
	: kind(ArrI), type(t), size(0), items(n), flags(f | Cst | ((v == nullptr) ? Bnd : 0)),
		value(build(ArrI, t, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for an explicit bound array, eg jam_t(0xcafe, 8, 32). */
	jam_t(field_t t, field_t s, field_t n, uint8_t f = 0)
	: kind(ArrE), type(t), size(s), items(n), flags(f | Bnd),
		value(build(ArrE, t, s, n, nullptr, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for an explicit array, eg jam_t(0xcafe, 8, &extval, 32). */
	jam_t(field_t t, field_t s, void *v, field_t n, uint8_t f = 0)
	: kind(ArrE), type(t), size(s), items(n), flags(f | ((v == nullptr) ? Bnd : 0)),
		value(build(ArrE, t, s, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for an explicit array (const). */
	jam_t(field_t t, field_t s, const void *v, field_t n, uint8_t f = 0)
	: kind(ArrE), type(t), size(s), items(n), flags(f | Cst | ((v == nullptr) ? Bnd : 0)),
		value(build(ArrE, t, s, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for a list or map bound object, eg jam_t(Lst, 16). */
	jam_t(kind_e k, field_t n, uint8_t f = 0)
		: kind(k), type(0), size(0), items(n), flags(f | Bnd),
		value(build(k, 0, 0, n, nullptr, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for a list or map object, eg jam_t(Lst, &extval, 16). */
	jam_t(kind_e k, void *v, field_t n, uint8_t f = 0xff)
	: kind(k), type(0), size(0), items(n), flags((f == 0xff) ? f : ((v == nullptr) ? Bnd : 0)),
		value(build(k, 0, 0, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for a list or map object (const). */
	jam_t(kind_e k, const void *v, field_t n, uint8_t f = 0xff)
	: kind(k), type(0), size(0), items(n), flags((f == 0xff) ? f : Cst | ((v == nullptr) ? Bnd : 0)),
		value(build(k, 0, 0, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

//	/** Constructor for a compound (array, list or map) implicit bound object, eg jam_t(Lst, F32, 16). */
//	jam_t(kind_e k, type_e t, field_t n, uint8_t f = 0)
//		: kind(k), type(t), size(0), items(n), flags(f | Bnd),
//		value(build(k, t, 0, n, nullptr, flags))
//	{ }

//	/** Constructor for a compound (array, list or map) implicit object, eg jam_t(, F32, 16).
//		If value pointer provided is null, it will construct a bound object. Otherwise, the external value is used.
//	*/
//	jam_t(kind_e k, type_e t, void *v, field_t n, uint8_t f = 0)
//		: kind(k), type(t), size(0), items(n), flags(f | ((v == nullptr) ? Bnd : 0)),
//		value(build(k, t, 0, n, v, flags))
//	{ }

//	/** Constructor for a compound (array, list or map) implicit object (const).
//		If value pointer provided is null, it will construct a bound object. Otherwise, the external value is used.
//	*/
//	jam_t(kind_e k, type_e t, const void *v, field_t n, uint8_t f = 0)
//		: kind(k), type(t), size(0), items(n), flags(f | Cst | ((v == nullptr) ? Bnd : 0)),
//		value(build(k, t, 0, n, v, flags))
//	{ }

//	/** Constructor for a compound (array, list or map) explicit bound object. */
//	jam_t(kind_e k, field_t t, field_t s, field_t n, uint8_t f = 0xff)
//		: kind(k), type(t), size(s), items(n), flags((f != 0xff) ? f : Bnd),
//		value(build(k, t, s, n, nullptr, flags))
//	{ }

	/** Generic object constructor.
		With default flags, if value pointer provided is null, it will construct a bound object. Otherwise, the external value is used.
	*/
	jam_t(kind_e k, field_t t, field_t s, void *v, field_t n, uint8_t f = 0xff)
		: kind(k), type(t), size(s), items(n), flags((f != 0xff) ? f : ((v == nullptr) ? Bnd : 0)),
		value(build(k, t, s, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Generic object constructor (const).
		If value pointer provided is null, it will construct a bound object. Otherwise, the external value is used.
	*/
	jam_t(kind_e k, field_t t, field_t s, const void *v, field_t n, uint8_t f = 0xff)
		: kind(k), type(t), size(s), items(n), flags((f != 0xff) ? f : Cst | ((v == nullptr) ? Bnd : 0)),
		value(build(k, t, s, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Constructor for a pure type definition. */
	jam_t(kind_e k, field_t t, field_t s, field_t n)
	: kind(k), type(t), size(s), items(n), flags(0 /* !Bnd */),
		value(build(k, t, s, n, nullptr, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** jam_t type definition factory.
		@note A jam pure type definition has an unbound (external) null pointer value.
	*/
	static inline jam_t def(kind_e k, field_t t, field_t s = 0, field_t n = 0)
	{
		return jam_t(k, t, s, (void *)nullptr, n, 0 /* !Bnd */);
	}

	jam_t(const jam_t &j)
		: kind(j.kind), type(j.type), size(j.size), items(j.items), flags(j.flags),
			value(build(j.kind, j.type, j.size, j.items, j.ptr(), j.flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

#if __cplusplus >= 201103L
	/** Move constructor. */
	jam_t(jam_t &&j)
		: kind(j.kind), type(j.type), size(j.size), items(j.items), flags(j.flags), value(j.value)
	{
		j.value = nullptr;
	}
#endif

	/** Free dynamic resources and clear value. */
	inline void dealloc()
	{
		if (isDynamic())
		{
			if (kind == Lst || kind == Map)
				delete[] static_cast<jam_t *>(value);
			else
				jam_FREE(value);

#ifdef jam_DEBUG
			--allocs;
#endif
		}
		value = nullptr;
	}

	~jam_t()
	{
		dealloc();

#ifdef jam_DEBUG
		--objects;
#endif
	}

	inline jam_t& operator=(const jam_t &j)
	{
		if (this != &j)
		{
			dealloc(); /** @todo Optimize to reuse allocated memory. */
			kind = j.kind, type = j.type, size = j.size, items = j.items, flags = j.flags;
			value = build(kind, type, size, items, j.ptr(), flags);
		}
		return *this;
	}

#if __cplusplus >= 201103L
	inline jam_t& operator=(jam_t &&j)
	{
		dealloc(); /** @todo Optimize to reuse allocated memory. */
		kind = j.kind, type = j.type, size = j.size, value = j.value, items = j.items, flags = j.flags;
		j.value = nullptr;
		return *this;
	}
#endif

	/** Check if object is valid. */
	inline bool isOk() const
	{
		return isKind(kind) && ((kind == SngI) ? type <= 0xf : true);
	}

	inline operator bool() const { return isOk(); }

	/** Check if object is dynamic. */
	inline bool isDynamic() const
	{
		return value && (flags & Bnd) && ((kind != SngI) || ((kind == SngI) && !isEnclosed(uint8_t(type))));
	}

	/** Check if object is type definition (null value pointer and not bound). */
	inline bool isType() const
	{
		return (value == nullptr) && !(flags & Bnd);
	}

	/** Check if object is constant. */
	inline bool isConstant() const
	{
		return (flags & Cst) != 0;
	}

	/** Build id for object. */
	inline id_t id() const
	{
		switch (kind)
		{
		case SngI: return id(kind, uint8_t(type));
		case SngE: return id(kind, span(type), span(size));
		case Lst:
		case Map: return id(kind, span(items), span(size));
		case Spc:
		case Usr: return id(kind, span(size));
		case ArrI: return id(kind, span(items), uint8_t(type));
		case ArrE: return id(kind, span(items), span(type), span(size));
		}
		return 0;
	}

	/** Calculate serialized bytes. */
	inline size_t bytes(uint8_t f = Hdr|Val) const
	{
		size_t r = (f & Dsc) ? 1 : 0;

		switch (kind)
		{
		case SngI:
			if (isEmbedded(type))
				r = 1; // Force embedded value
			else if (f & Val)
				r += width(uint8_t(type));
			break;

		case SngE:
			if (f & Inf)
				r += spanBytes(size) + spanBytes(type);
			if (f & Val)
				r += size;
			break;

		case Map:
		case Lst:
			if (!(f & Upd)) // Calculate size if not updated
				update(f);
			if (f & Inf)
				r += spanBytes(size) + spanBytes(items);
			if (f & Val)
				r += size;
			break;

		case Spc:
		case Usr:
			if (f & Inf)
				r += spanBytes(size);
			if (f & Val)
				r += size;
			break;

		case ArrI:
			if (f & Inf)
				r += spanBytes(items);
			if (f & Val)
				r += items*width(uint8_t(type));
			break;

		case ArrE:
			if (f & Inf)
				r += spanBytes(items) + spanBytes(size) + spanBytes(type);
			if (f & Val)
				r += items*size;
			break;

		default: // Gets encoded as embedded unknown value (X)
			r = 1;
			break;
		}
		return r;
	}

	/** Synchronize fields. */
	inline void update(uint8_t f = Hdr|Val) const
	{
		size_t l = items;

		switch (kind)
		{
		case SngI:
		case ArrI:
			size = width(uint8_t(type));
			break;

		// case SngE:

		case Map:
			l *= 2;
		case Lst:
			size = 0;
			for (size_t i = 0; i < l; ++i)
				size += (f & Nrc) ? ((jam_t *)value)[i].size : ((jam_t *)value)[i].bytes(f);
			break;

		// case Spc:
		// case Usr:
		// case ArrE:

		case Und:
			size = 0;
			break;
		}
	}

	/** Get pointer to a list/map item. */
	const jam_t *item(size_t n) const
	{
		size_t l = items;
		switch (kind)
		{
		case Map:
			l *= 2;
		case Lst:
			if (n < l)
				return static_cast<const jam_t *>(value) + n;
		}
		return nullptr;
	}

	jam_t *item(size_t n)
	{
		return const_cast<jam_t *>(static_cast<const jam_t &>(*this).item(n));
	}

	/** Get pointer to a map key (even item). */
	const jam_t *key(size_t n) const
	{
		size_t l = items;
		switch (kind)
		{
		case Lst:
			l /= 2; // Treat list as map
		case Map:
			if (n < l)
				return static_cast<const jam_t *>(value) + 2*n;
		}
		return nullptr;
	}

	jam_t *key(size_t n)
	{
		return const_cast<jam_t *>(static_cast<const jam_t &>(*this).key(n));
	}

	/** Get pointer to a map entry (odd item). */
	const jam_t *entry(size_t n) const
	{
		size_t l = items;
		switch (kind)
		{
		case Lst:
			l /= 2; // Treat list as map
		case Map:
			if (n < l)
				return static_cast<const jam_t *>(value) + 2*n + 1;
		}
		return nullptr;
	}

	jam_t *entry(size_t n)
	{
		return const_cast<jam_t *>(static_cast<const jam_t &>(*this).entry(n));
	}

	/** Get pointer to value. */
	inline void *ptr()
	{
		if (!isOk() || isConstant())
			return nullptr; // Disallow
		else
			return (kind == SngI && (flags & Bnd) && isEnclosed(uint8_t(type))) ? (void *)&value : value;
	}

	/** Get pointer to const value. */
	inline const void *ptr() const
	{
		if (!isOk())
			return nullptr;
		else
			return (kind == SngI && (flags & Bnd) && isEnclosed(uint8_t(type))) ? (const void *)&value : value;
	}

	/** Format (serialize) an object into byte array.
		@param b Pointer to serialization buffer.
		@param bl Buffer length.
		@param f Serialization control flags.

		@return Error code if negative, number of serialized bytes otherwise.
	*/
	inline int format(uint8_t *b, size_t bl, uint8_t f = Hdr|Val) const
	{
		size_t r = 0; // Track number of bytes written

		if (!(f & Skp))
		{
			if (bl < bytes(f))
				return -1; // No space
		}

		id_t d = id(); // Calculate object descriptor byte
		info_t g = info(d);

		if (f & Dsc) // Add descriptor as prefix
			r += write(b, r, sizeof(id_t), (void *)&d);

		size_t l = items;

		switch (kind)
		{
		case SngI:
			if (isEmbedded(type))
			{
				if (!(f & Dsc))
				{
					uint8_t v = (type == True) ? 1 : 0;
					r += write(b, r, 1, &v);
				}
			}
			else if (f & Val)
				r += write(b, r, g.size, ptr());
			break;

		case SngE:
			if (f & Inf)
			{
				r += write(b, r, g.size, (void *)&size);
				r += write(b, r, g.type, (void *)&type);
			}
			if (f & Val)
				r += write(b, r, size, (void *)&value);
			break;

		case Map:
			l *= 2;
		case Lst:
			if (f & Inf)
			{
				r += write(b, r, g.size, (void *)&size);
				r += write(b, r, g.items, (void *)&items);
			}
			if (f & Val)
				for (size_t i = 0; i < l; ++i)
					r += ((jam_t *)value)[i].format(b + r, bl - r, f|Skp);
			break;

		case Spc:
		case Usr:
			if (f & Inf)
				r += write(b, r, g.size, (void *)&size);
			if (f & Val)
				r += write(b, r, size, value);
			break;

		case ArrI:
			if (f & Inf)
				r += write(b, r, g.items, (void *)&items);
			if (f & Val)
				r += write(b, r, l*g.size, value);
			break;

		case ArrE:
			if (f & Inf)
			{
				r += write(b, r, g.items, (void *)&items);
				r += write(b, r, g.size, (void *)&size);
				r += write(b, r, g.type, (void *)&type);
			}
			if (f & Val)
				r += write(b, r, l*size, value);
			break;
		}

		return (int)r;
	}

	/** Format (serialize) a compound object.
		@param bl Buffer length.
		@param b Pointer to serialization buffer.
		@param l Items count.
		@param f Serialization control flags.
		@param a Pointer to jam objects array.

		@return Error code if negative, number of serialized bytes otherwise.
	*/
	static int format(
		size_t bl, uint8_t *b, size_t l,
		jam_t *a, uint8_t f = Dsc|Val
	)
	{
		size_t r = 0;

		// Check buffer length
		if (!(f & Skp))
		{
			for (size_t i = 0; i < l; ++i)
				r += a[i].bytes(f);

			if (bl < r)
				return -1; // No space

			r = 0;
		}

		for (size_t i = 0; i < l; ++i)
			r += a[i].format(b + r, bl - r, f|Skp);

		return (int)r;
	}

	/** Parse (unserialize) an object from byte array
		@param bl Buffer items
		@param b Pointer to serialization buffer
		@param f Parse flags
		@return Error code if negative, number of deserialized bytes otherwise
	*/
	inline int parse(const uint8_t *b, size_t bl, uint8_t f = Hdr|Val)
	{
		size_t r = 0; // Track number of bytes read
		id_t d;

		if (f & Dsc) // Read descriptor from buffer
		{
			if (bl < 1)
				return -1;
			r += read(b, r, sizeof(id_t), (void *)&d);
		}
		else // Use object existing id
			d = id();

		info_t g = info(d);
		size_t h = (f & Inf) ? g.type + g.size + g.items : 0; // Header bytes

		if (bl <= r + h)
			return -1; // Not enough bytes

		field_t ts, tt, ti;
		void *tv = nullptr;

		switch (g.kind)
		{
		case SngI:
			if (f & Val)
			{
				tv = isEnclosed(g.type) ? &tv : jam_ALLOC(g.size);
				if (tv)
					r += read(b, r, g.size, tv);
			}
			break;

		case SngE:
			if (f & Inf)
			{
				r += read(b, r, g.size, (void *)&ts);
				r += read(b, r, g.type, (void *)&tt);
			}
			if (f & Val)
			{
				tv = jam_ALLOC(ts);
				if (tv)
					r += read(b, r, ts, tv);
			}
			break;

		case Map:
		case Lst:
			if (f & Inf)
			{
				r += read(b, r, g.size, (void *)&ts);
				r += read(b, r, g.type, (void *)&ti);
			}
			if (f & Val)
			{
				size_t l = (g.kind == Map) ? ti * 2 : ti;
				jam_t *ja = new jam_t[l];
				if (ja)
					for (size_t i = 0; i < l; ++i)
						r += ja[i].parse(b + r, bl - r, f);
				tv = static_cast<void *>(ja);
			}
			break;

		case Spc:
		case Usr:
			if (f & Inf)
				r += read(b, r, g.size, (void *)&ts);
			if (f & Val)
				r += read(b, r, ts, tv);
			break;

		case ArrI:
			if (f & Inf)
				r += read(b, r, g.items, (void *)&ti);
			if (f & Val)
				r += read(b, r, ti*g.size, tv);
			break;

		case ArrE:
			if (f & Inf)
			{
				r += read(b, r, g.items, (void *)&ti);
				r += read(b, r, g.size, (void *)&ts);
				r += read(b, r, g.type, (void *)&tt);
			}
			if (f & Val)
				r += read(b, r, ti*ts, value);
			break;
		}

		return (int)r;
	}

	/** Parse (unserialize) an object from byte array using type template.
	@param b Pointer to serialization buffer.
	@param bl Buffer items.
	@param[in,out] kl Type array items.
	@param k Type array.
	@param f Parse flags.
	@return Error code if negative, otherwise number of deserialized bytes.
	*/
	int parse(uint8_t *b, size_t bl, size_t &kl, const jam_t *k, uint8_t f = 0);

	/** Format (serialize) items */
	static int format(uint8_t *b, size_t bl, size_t &kl, const jam_t *k);

	/** Parse (deserialize) items */
	static int parse(uint8_t *b, size_t bl, jam_t *k, void *v = nullptr);

	// Helper constructors
	// SngX
	jam_t(uint8_t v)
	: kind(SngI), type(U8), size(0), items(0), flags(Bnd),
		value(build(SngI, U8, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint8_t *v)
	: kind(SngI), type(U8), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const uint8_t *v)
	: kind(SngI), type(U8), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint16_t v)
		: kind(SngI), type(U16), size(0), items(0), flags(Bnd),
		value(build(SngI, U16, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint16_t *v)
	: kind(SngI), type(U16), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const uint16_t *v)
	: kind(SngI), type(U16), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint32_t v)
	: kind(SngI), type(U32), size(0), items(0), flags(Bnd),
		value(build(SngI, U32, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint32_t *v)
	: kind(SngI), type(U32), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const uint32_t *v)
	: kind(SngI), type(U32), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint64_t v)
	: kind(SngI), type(U64), size(0), items(0), flags(Bnd),
		value(build(SngI, U64, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint64_t *v)
	: kind(SngI), type(U64), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const uint64_t *v)
	: kind(SngI), type(U64), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int8_t v)
	: kind(SngI), type(I8), size(0), items(0), flags(Bnd),
		value(build(SngI, I8, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int8_t *v)
	: kind(SngI), type(I8), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const int8_t *v)
	: kind(SngI), type(I8), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int16_t v)
	: kind(SngI), type(I16), size(0), items(0), flags(Bnd),
		value(build(SngI, I16, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int16_t *v)
	: kind(SngI), type(I16), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const int16_t *v)
	: kind(SngI), type(I16), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int32_t v)
	: kind(SngI), type(I32), size(0), items(0), flags(Bnd),
		value(build(SngI, I32, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int32_t *v)
	: kind(SngI), type(I32), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const int32_t *v)
	: kind(SngI), type(I32), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int64_t v)
	: kind(SngI), type(I64), size(0), items(0), flags(Bnd),
		value(build(SngI, I64, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int64_t *v)
	: kind(SngI), type(I64), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const int64_t *v)
	: kind(SngI), type(I64), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(char v)
	: kind(SngI), type(C8), size(0), items(0), flags(Bnd),
		value(build(SngI, C8, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** For convenience using C strings, this will create a char array instead of a pointer to a char.
		To create a pointer to a single char, use the full argument constructor instead.
	*/
	jam_t(char *v)
	: kind(ArrI), type(C8), size(0), items(v ? strlen(v) : 0), flags((v == nullptr) ? Bnd : 0),
		value(build(ArrI, C8, items, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const char *v)
	: kind(ArrI), type(C8), size(0), items(v ? strlen(v) : 0), flags(Cst | ((v == nullptr) ? Bnd : 0)),
		value(build(ArrI, C8, items, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** std::string wrapper. */
	jam_t(const std::string &s)
	: kind(ArrI), type(C8), size(0), items(s.size()), flags(Cst),
		value(build(ArrI, C8, items, s.c_str(), flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

/*
	jam_t(char *v)
	: kind(SngI), type(C8), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const char *v)
	: kind(SngI), type(C8), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}
*/

#ifdef WCHAR_MAX
	jam_t(wchar_t v)
	: kind(SngI), type(C16), size(0), items(0), flags(Bnd),
		value(build(SngI, C16, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** For convenience using C wstrings, this will create a wchar array instead of a pointer to a wchar.
		To create a pointer to a single wchar, use the full argument constructor instead.
	*/
	jam_t(wchar_t *v)
	: kind(ArrI), type(C16), size(0), items(v ? wcslen(v) : 0), flags((v == nullptr) ? Bnd : 0),
		value(build(ArrI, C16, items, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const wchar_t *v)
	: kind(ArrI), type(C16), size(0), items(v ? wcslen(v) : 0), flags(Cst | ((v == nullptr) ? Bnd : 0)),
		value(build(ArrI, C16, items, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

/*
	jam_t(wchar_t *v)
	: kind(SngI), type(C16), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const wchar_t *v)
	: kind(SngI), type(C16), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}
*/
#endif

	jam_t(float v)
	: kind(SngI), type(F32), size(0), items(0), flags(Bnd),
		value(build(SngI, F32, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(float *v)
	: kind(SngI), type(F32), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const float *v)
	: kind(SngI), type(F32), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(double v)
	: kind(SngI), type(F64), size(0), items(0), flags(Bnd),
		value(build(SngI, F64, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(double *v)
	: kind(SngI), type(F64), size(0), items(0), flags(0), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const double *v)
	: kind(SngI), type(F64), size(0), items(0), flags(Cst), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(bool v)
	: kind(SngI), type(v ? True : False), size(0), items(0), flags(Bnd),
		value(build(SngI, v ? True : False, &v, Bnd))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(bool *v)
	: kind(SngI), type(v ? (*v ? True : False) : False), size(0), items(0), flags(v ? 0 : Bnd), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const bool *v)
	: kind(SngI), type(v ? (*v ? True : False) : False), size(0), items(0), flags(Cst | (v ? 0 : Bnd)), value((void *)v)
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}


	// ArrX
	jam_t(uint8_t *v, field_t n)
	: kind(ArrI), type(U8), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, U8, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const uint8_t *v, field_t n)
	: kind(ArrI), type(U8), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, U8, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint16_t *v, field_t n)
	: kind(ArrI), type(U16), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, U16, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const uint16_t *v, field_t n)
	: kind(ArrI), type(U16), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, U16, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint32_t *v, field_t n)
	: kind(ArrI), type(U32), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, U32, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const uint32_t *v, field_t n)
	: kind(ArrI), type(U32), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, U32, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(uint64_t *v, field_t n)
	: kind(ArrI), type(U64), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, U64, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const uint64_t *v, field_t n)
	: kind(ArrI), type(U64), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, U64, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int8_t *v, field_t n)
	: kind(ArrI), type(I8), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, I8, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const int8_t *v, field_t n)
	: kind(ArrI), type(I8), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, I8, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int16_t *v, field_t n)
	: kind(ArrI), type(I16), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, I16, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const int16_t *v, field_t n)
	: kind(ArrI), type(I16), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, I16, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int32_t *v, field_t n)
	: kind(ArrI), type(I32), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, I32, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const int32_t *v, field_t n)
	: kind(ArrI), type(I32), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, I32, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(int64_t *v, field_t n)
	: kind(ArrI), type(I64), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, I64, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const int64_t *v, field_t n)
	: kind(ArrI), type(I64), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, I64, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(char *v, field_t n)
	: kind(ArrI), type(C8), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, C8, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const char *v, field_t n)
	: kind(ArrI), type(C8), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, C8, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Jam string factory. */
	static inline jam_t str(char *s)
	{
		return jam_t(s, strlen(s));
	}

	/** Jam string factory (const). */
	static inline jam_t str(const char *s)
	{
		return jam_t(s, strlen(s));
	}

#ifdef WCHAR_MAX
	jam_t(wchar_t *v, field_t n)
	: kind(ArrI), type(C16), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, C16, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const wchar_t *v, field_t n)
	: kind(ArrI), type(C16), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, C16, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	/** Jam wide string factory. */
	static inline jam_t wstr(wchar_t *s)
	{
		return jam_t(s, wcslen(s));
	}

	/** Jam wide string factory (const). */
	static inline jam_t wstr(const wchar_t *s)
	{
		return jam_t(s, wcslen(s));
	}
#endif

	jam_t(float *v, field_t n)
	: kind(ArrI), type(F32), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, F32, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const float *v, field_t n)
	: kind(ArrI), type(F32), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, F32, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(double *v, field_t n)
	: kind(ArrI), type(F64), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, F64, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const double *v, field_t n)
	: kind(ArrI), type(F64), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, F64, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(bool *v, field_t n)
	: kind(ArrI), type(True), size(0), items(n), flags(v ? 0 : Bnd),
		value(build(ArrI, True, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	jam_t(const bool *v, field_t n)
	: kind(ArrI), type(True), size(0), items(n), flags(Cst | (v ? 0 : Bnd)),
		value(build(ArrI, True, n, v, flags))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	template <typename T>
	jam_t(const T &t)
	: kind(Lst), type(0), size(0), items(t.size()), flags(Bnd),
		value(buildList(t))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	template <typename K, typename T, typename H, typename P, typename A>
	jam_t(const std::unordered_map<K, T, H, P, A> &m)
	: kind(Map), type(0), size(0), items(m.size()), flags(Bnd),
		value(buildMap(m))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

	template <typename K, typename T, typename C, typename A>
	jam_t(const std::map<K, T, C, A> &m)
	: kind(Map), type(0), size(0), items(m.size()), flags(Bnd),
		value(buildMap(m))
	{
#ifdef jam_DEBUG
		++objects;
#endif
	}

};

using djam_t = std::map<std::string, jam_t>; ///< Type object form definition.

/*
std::ostream &operator<<(std::ostream &o, const jam_t &j)
{
	return o;
}
*/

/** Jam output. */
class ojam_t
{
public:
	std::ostream *stream;
	uint8_t flags; ///< Formatting flags.

public:
	ojam_t(): stream(nullptr), flags(0) { }
	ojam_t(std::ostream &s, uint8_t f = Hdr|Val): stream(&s), flags(f) { }
	~ojam_t() { }

	ojam_t &operator<<(const jam_t &j)
	{
		if (stream)
		{
			jam_t::size_t bl = j.bytes(flags);
			uint8_t *b = (uint8_t *)jam_ALLOC(bl);
			if (b)
			{
				j.format(b, bl, Skp|flags);
				stream->write((const char *)b, bl);
				jam_FREE(b);
			}
		}

		return *this;
	}

	explicit operator bool() { return stream ? bool(*stream) : false; }
};

/** Jam input. */
class ijam_t
{
public:
	using types_t = std::unordered_map<jam_FIELDTYPE, jam_t>; ///< Container for type definitions.

public:
	std::istream *stream;
	types_t types; ///< Type storage.

public:
	ijam_t(): stream(nullptr) { }
	ijam_t(std::istream &s): stream(&s) { }
	~ijam_t() { }

	explicit operator bool() { return stream ? bool(*stream) : false; }
};

} // namespace jam_NAMESPACE

#endif // JAM_H_
