#-------------------------------------------------
#
# Project created by QtCreator 2015-10-19T14:56:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jam_gui
TEMPLATE = app

win32-g++|win64-g++ {
	QMAKE_CXXFLAGS = -std=c++11
}

INCLUDEPATH = ../jam

SOURCES += main.cpp\
        mainwindow.cpp \
    jamagent.cpp

HEADERS  += mainwindow.h \
    jamagent.h \
    ../jam/jam.h

FORMS    += mainwindow.ui
