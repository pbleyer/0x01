#ifndef JAMAGENT_H
#define JAMAGENT_H

#include <QObject>

#include <jam.h>

class JamAgent : public QObject
{
	Q_OBJECT

public:
	explicit JamAgent(QObject *parent = 0);
	~JamAgent() { }

signals:
	void transmit(const QByteArray &a);
	void updateType(uintptr_t, const jam::jam_t &j);
	void updateVariable(const QString &s, const jam::jam_t &j);

public slots:
	void receive(const QByteArray &a);

private:


private:
	jam::ijam_t::types_t _jtypes; ///< Jam types.
	std::map<std::string, jam::jam_t> _jvars; ///< Jam variables.
};

#endif // JAMAGENT_H
