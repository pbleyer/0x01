#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <sstream>
#include <iomanip>

#include <jam.h>

using namespace jam;

static std::string
_hexString(int l, uint8_t *b)
{
	std::stringstream ss;
	for (int i = 0; i < l; ++i)
		ss << std::hex << std::setw(2) << std::setfill('0') << ((unsigned)b[i] & 0xff);
	return ss.str();
}

static std::string
_valueString(const jam_t &k)
{
	std::stringstream ss;
	size_t sz = (jam_t::isImplicit(k.kind)) ? jam_t::width(k.type)
		: k.size;

	const void *o = k.ptr();
	unsigned i = 0, ln = (jam_t::isSingle(k.kind)) ? 1 : k.items;

	while (true)
	{
		switch (k.type)
		{
		case U8: ss << unsigned(*(const uint8_t *)o & 0xff); break;
		case U16: ss << *(const uint16_t *)o; break;
		case U32: ss << *(const uint32_t *)o; break;
		case U64: ss << *(const uint64_t *)o; break;
		case I8: ss << (int)*(const int8_t *)o; break;
		case I16: ss << *(const int16_t *)o; break;
		case I32: ss << *(const int32_t *)o; break;
		case I64: ss << *(const int64_t *)o; break;

		case C8: ss << *(const char *)o; break;
		// case C16: ss << *(const wchar_t*)o; break;

		case F32: ss << *(const float *)o; break;
		case F64: ss << *(const double *)o; break;

		case True: ss << "<T>"; break;
		case False: ss << "<F>"; break;
		case Nil: ss << "<N>"; break;

		default: ss << "<?>";
		}

		++i;
		if (i == ln)
			break;

		if (k.type != C8)
			ss << " ";

		o = (uint8_t *)o + sz;
	}
	return ss.str();
}

static std::string
_jamString(const jam_t &k)
{
	std::stringstream ss;
	jam_t::id_t i = k.id();
	ss << "id: " << std::hex << std::setw(2) << std::setfill('0') << (unsigned(k.id()) & 0xff);
	ss << " (" << jam_t::kindName(k.kind);

	info_t f = jam_t::info(i);

	switch (k.kind)
	{
	case SngI:
		ss << ", " << jam_t::typeName(k.type) << ", " << (unsigned)f.size << "B)";
		ss << ", value: " << _valueString(k);
		break;

	case SngE:
		ss << ", " << (unsigned)f.type << "B, " << (unsigned)f.size << "B)";
		ss << ", type: " << std::hex << std::setw(f.type) << std::setfill('0') << (unsigned)k.type;
		ss << ", size: " << std::dec << (unsigned)k.size;
		ss << ", value: " << _hexString(k.size, (uint8_t *)k.ptr());
		break;

	case Lst:
	case Map:
	case Spc:
	case Usr:

	case ArrI:
	case ArrE:
		ss << ", " << jam_t::typeName(k.type) << ", items:" << (unsigned)k.items << "[" << (unsigned)f.items << "B])";
		ss << ", value: " << _valueString(k);
		break;
	}
	return ss.str();
}

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	initTree(ui->treeWidgetHost);
	initTree(ui->treeWidgetTarget);
	initObjects();
	syncObjects();
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_pushButtonRun_clicked(bool checked)
{
	if (checked)
	{
		// Launch target
		JamAgent *tgt = new JamAgent();
		tgt->moveToThread(&_targetThread);

		connect(&_targetThread, &QThread::finished, tgt, &QObject::deleteLater);
		connect(this, &MainWindow::targetTransmit, tgt, &JamAgent::receive);
		connect(tgt, &JamAgent::transmit, this, &MainWindow::targetReceive);
		connect(tgt, &JamAgent::updateType, this, &MainWindow::targetUpdateType);
		connect(tgt, &JamAgent::updateVariable, this, &MainWindow::targetUpdateVariable);

		_targetThread.start();
	}
	else
	{
		_targetThread.quit();
		_targetThread.wait();
	}
}

void MainWindow::targetReceive(const QByteArray &a)
{

}

void MainWindow::targetUpdateType(uintptr_t, const jam_t &j)
{

}

void MainWindow::targetUpdateVariable(const QString &s, const jam_t &j)
{

}

void MainWindow::syncObjects()
{
	ui->listWidgetObjects->clear();
	for (auto &i: _jobjs)
	{
		ui->listWidgetObjects->addItem(QString(_jamString(i).c_str()));
	}

}

void
MainWindow::initTree(QTreeWidget *t)
{
	QStringList li{"Types", "Variables"};
	t->addTopLevelItem(new QTreeWidgetItem(li));
}

void
MainWindow::initObjects()
{
	_jobjs.clear();

	_jobjs.push_back(jam_t(uint8_t(123)));
	_jobjs.push_back(jam_t(uint32_t(11110000)));
	_jobjs.push_back(jam_t(float(3.14159)));
	_jobjs.push_back(jam_t::str("This is the message"));
}
