#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QTreeWidget>

#include <vector>

#include "jamagent.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_pushButtonRun_clicked(bool checked);

	void targetReceive(const QByteArray &a);
	void targetUpdateType(uintptr_t, const jam::jam_t &j);
	void targetUpdateVariable(const QString &s, const jam::jam_t &j);

	void syncObjects();

signals:
	/** Send data to target */
	void targetTransmit(const QByteArray &a);

private:
	void initTree(QTreeWidget *t);
	void initObjects();

private:
	Ui::MainWindow *ui;
	JamAgent _hostAgent;
	QThread _targetThread; ///< Target thread
	std::vector<jam::jam_t> _jobjs;
};

#endif // MAINWINDOW_H
