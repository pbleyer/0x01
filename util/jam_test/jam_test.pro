TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

win32-g++|win64-g++ {
	QMAKE_CXXFLAGS = -std=c++11
}

INCLUDEPATH += ../jam

SOURCES = main.cpp

HEADERS += \
    ../jam/jam.h
