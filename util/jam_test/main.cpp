#include <stdio.h>
#include <string.h>
#include <vector>
#include <iostream>
#include <cstdint>
#include <sstream>

// #define __STDC_FORMAT_MACROS
// #include <cinttypes>

#define jam_DEBUG
#include "jam.h"

using namespace jam;

int jam_t::objects = 0;
int jam_t::allocs = 0;

static void
_printStats()
{
	printf("Objects = %d\n", jam_t::objects);
	printf("Allocs = %d\n", jam_t::allocs);
}

static void
_printHex(const uint8_t *b, int l)
{
	for (int i = 0; i < l; ++i)
	{
		printf("%02x", b[i]);
		if ((i+1) % 16 == 0)
			printf("\n");
		else
			printf(" ");
	}
	printf("\n");
}

static void
_printHex(const std::string &s)
{
	_printHex((const uint8_t *)s.c_str(), s.size());
}

static void
_printValue(const jam_t &k)
{
	size_t sz = (jam_t::isImplicit(k.kind)) ? jam_t::width(uint8_t(k.type))
		: k.size;

	const void *o = k.ptr();

	if (!o)
	{
		printf("<nul>");
		return;
	}

	unsigned i = 0;
	size_t ln = (jam_t::isSingle(k.kind)) ? 1 : k.items;

	while (true)
	{
		switch (k.type)
		{
		case U8: printf("%u", *(const uint8_t *)o); break;
		case U16: printf("%u", *(const uint16_t *)o); break;
		case U32: printf("%u", *(const uint32_t *)o); break;
		// case jam_t::U64: printf("%" PRIu64, *(const uint64_t *)o); break;

		case I8: printf("%d", *(const int8_t *)o); break;
		case I16: printf("%d", *(const int16_t *)o); break;
		case I32: printf("%d", *(const int32_t *)o); break;
		// case I64: printf("%lld", *(const int64_t *)o); break;

		case C8: printf("%c", *(const char *)o); break;
		// case C16: printf("%s", *(const wchar_t*)o); break;

		case F32: printf("%g", *(const float *)o); break;
		case F64: printf("%g", *(const double *)o); break;

		case True: printf("True"); break;
		case False: printf("False"); break;
		case Unknown: printf("Unknown"); break;
		case Null: printf("Null"); break;

		default: printf("<?>");
		}

		++i;
		if (i == ln)
			break;

		if (k.type != C8)
			printf(" ");

		o = (uint8_t *)o + sz;
	}
}

static void
_printJam(const jam_t &k, bool lf = true)
{
	jam_t::id_t i = k.id();
	printf("id: 0x%x (%s", k.id(), jam_t::kindName(k.kind));

	info_t f = jam_t::info(i);
	size_t n = k.items;

	switch(k.kind)
	{
	case SngI:
		printf(",%s,%dB), value: ", jam_t::typeName(uint8_t(k.type)), f.size);
		_printValue(k);
		break;

	case SngE:
		printf(",%dB,%dB)", f.type, f.size);
		printf(", type: 0x%x, size: %d", unsigned(k.type), unsigned(k.size));
		break;

	case Map:
		n *= 2;
	case Lst:
		printf(",items:%d(%dB)), value:\n", n, f.items);
		for (size_t i = 0; i < n; ++i)
		{
			printf("  ");
			_printJam(*k.item(i));
		}
		lf = false;
		break;

	case Spc:
	case Usr:

	case ArrI:
	case ArrE:
		printf(",%s,items:%d(%dB)), value: ", jam_t::typeName(k.type), n, f.items); // Fix for explicit
		_printValue(k);
		break;

	default:
		printf(")");
		break;
	}

	if (lf)
		printf("\n");
}

static void
test_bound()
{
	jam_t u8(U8);
	_printJam(u8);

	jam_t *a16 = new jam_t(U16, 5);
	// jam_t a16(U16, 5);
	_printJam(*a16);
	delete a16;

	jam_t jl(Lst, 6);
	_printJam(jl);

	uint8_t buf[256];
	int r = 0;

	r = jl.format(buf+r, 256-r);
	_printHex(buf, r);
}

static void
test_embedded()
{
	jam_t j, t(T), f(F), x(X), n(N);
	_printJam(j);
	_printJam(t);
	_printJam(f);
	_printJam(x);
	_printJam(n);

	jam_t a = std::list<jam_t>{uint8_t(0xab), T, F, X, N};
	_printJam(a);

	uint8_t buf[256];
	int r = 0;

	r = a.format(buf+r, 256-r);
	_printHex(buf, r);

	r = 0;
	r = a.format(buf+r, 256-r, Val);
	_printHex(buf, r);
}

static void
test_string()
{
	jam_t j = "This is string 1";
	_printJam(j);

	std::string s = "This is string 2";
	j = s;
	_printJam(j);

	const char msg[] = "This is string 3";
	j = msg;
	_printJam(j);
}

static void
test_basic()
{
	uint8_t u8 = 12;
	uint16_t u16 = 3456;
	uint32_t u32 = 0xdeadb00b;

	char msg[] = "This is the message";
	uint16_t arr[] = { 0x1234, 0x5678, 0x1001, 0x1616 };

	std::cout << "Formatting" << std::endl;

	uint8_t buf[256];
	int r = 0;
	jam_t k;

	k = &u8;
	_printJam(k);
	r += k.format(buf+r, 256-r);
	_printHex(buf, r);

	k = &u16;
	_printJam(k);
	r += k.format(buf+r, 256-r);
	_printHex(buf, r);

	k = &u32;
	_printJam(k);
	r += k.format(buf+r, 256-r);
	_printHex(buf, r);

	k = jam_t::str(msg);
	_printJam(k);
	r += k.format(buf+r, 256-r);
	_printHex(buf, r);

	k = jam_t(arr, 4);
	_printJam(k);
	r += k.format(buf+r, 256-r);
	_printHex(buf, r);

	k = {arr, 4};
	_printJam(k);
	r += k.format(buf+r, 256-r);
	_printHex(buf, r);

}

// __pragma(pack(push, 1))
struct TestStruct
{
	uint8_t byt;
	char str[16];
	float flt;
};
// __pragma(pack(pop))

static void
test_struct()
{
	uint8_t buf[256];
	int r = 0;

	struct TestStruct ts = {0x55, "The string", 3.14159f};
	// ts.byt = 0x55;
	// strncpy(ts.str, "The string", sizeof(TestStruct::str));
	// ts.flt = 3.14159f;

	jam_t k = jam_t(666 /*type*/, sizeof(ts), &ts);
	_printJam(k);
	r += k.format(buf+r, 256-r);
	_printHex(buf, r);

	// Type definition for TestStruct
	djam_t vm =
	{
		{"byt", jam_t::def(SngI, U8)}, // uint8_t
		{"str", jam_t::def(ArrI, C8, sizeof(TestStruct::str))}, // char[16]
		{"flt", jam_t::def(SngI, F32)}, // float
	};
}

static void
test_compound()
{
	uint8_t buf[256];
	int r = 0;

	jam_t j(Lst, 5);
	_printJam(j);

	r = j.format(buf, 256);
	_printHex(buf, r);
}

static void
test_ojam(uint8_t f = Hdr|Val)
{
	// char buf[256];
	std::stringstream ss;
	ojam_t oj(ss, f);

	uint8_t u8 = 12;
	uint16_t u16 = 3456;
	uint32_t u32 = 0xdeadb00b;

	oj << u8;
	oj << u16;
	oj << u32;
	oj << "A message";
	oj << 12345;

	std::list<uint8_t> l = { 0, 1, 2, 3, 4, 5 };
	oj << l;

	_printHex(ss.str());
}



int
main(int argc, char **argv)
{
	_printStats();

	test_basic();
	test_bound();
	test_string();
	test_struct();
	test_embedded();
	test_compound();
	test_ojam();
	test_ojam(Val);

	_printStats();


/*
	jam_t::def_info vi[] =
	{
		{ "byt", &vt[0] },
		{ "str", &vt[1] },
		{ "flt", &vt[2] },
	};
*/
/*
	printf("Parsing\n");
	m = 0, l = 0;
	k.value = mem;

	while (m < r)
	{
		l = k.parse(256-m, buf+m, Pfx);
		if (l)
		{
			_printJam(k);
			m += l;
		}
	}
*/

	return 0;
}
