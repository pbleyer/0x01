#!python

# Recursively perform a byte value histogram of files under a folder

# Update the results output file after this number of files processed
UPDATE = 250

import os
import sys
from collections import Counter

# File counter
fc = 0

def histfile(c, fn, fo):
	"File byte value histogram"
	try:
		with open(fn, "rb") as fd:
			d = fd.read()
		c.update(Counter(d))
		histwrite(c, fo)
	except:
		pass

def histdir(c, dn, fo):
	"Recursive byte value histogram for files under given folder"
	for r, s, f in os.walk(dn, followlinks = False):
		for i in f:
			t = os.path.join(r, i)
			try:
				print(t)
				sys.stdout.flush()
			except:
				pass # In case file has unprintable characters
			histfile(c, t, fo)
		for i in s:
			histdir(c, i, fo)

def histwrite(c, fn):
	"Write histogram results to file"
	global fc
	fc += 1
	if (fc < UPDATE):
		return
	fc = 0
	# Sort by count
	r = sorted(c.items(), key = lambda x:x[1])
	s = ''
	for i in r:
		s += '0x{0:02x}: {1}\n'.format(*i)
	if fn:
		with open(fn, "w") as fd:
			fd.write(s)
	else:
		sys.stdout.write(s)
		sys.stdout.flush()

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print("Usage: " + sys.argv[0] + " start_folder [results_file]")
		sys.exit(-1)

	c = Counter()
	t = None if len(sys.argv) < 3 else sys.argv[2]
	histdir(c, sys.argv[1], t)
	fc = UPDATE # Force update
	histwrite(c, t)
