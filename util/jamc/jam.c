#include "jam.h"
#include "jammem.h"
#include "jamprv.h"

#ifdef jam_DEBUG
int _jamObjects = 0; ///< Track number of objects
int _jamAllocs = 0; ///< Track memory allocations
#endif

jam_t *
jamAlloc(jamsize_t n)
{
	jam_t *j = jam_ALLOC(sizeof(jam_t) * n);

	if (n)
		jam_ASSERT(j);

	if (j) // Initialize objects
	{
		for (jamsize_t i = 0; i < n; ++i)
		{
			j[i].kind = Und; // Object initially undefined
			// j[i].value = NULL;
		}

#ifdef jam_DEBUG
		++_jamObjects;
#endif
	}

	return j;
}

void
jamDealloc(jam_t *j, jamsize_t n)
{
	if (j)
	{
		while (n--) // Free objects
			jamFree(j + n);

		jam_FREE(j); // Release memory

#ifdef jam_DEBUG
		--_jamObjects;
#endif
	}
}

void *
jamValue(
	jamkind_e k, jamfield_t t, jamfield_t s, const void *v, jamfield_t n, uint8_t f
)
{
	if ((f & Xfr) || !(f & Bnd)) // External value object
		return (void *)v;

	if (jamIsImplicit(k)) // Implicit object
		s = jamWidth((uint8_t)t);

	bool nts = false;

	if (jamIsArray(k)) // Object is array
	{
		s = s ? s*n : n; // Total bytes (fix for embedded type)
		nts = (t == C8 || t == C16) && (f & Nts); // Add null and treat v as null-terminated
	}

	void *p = NULL; // Return value

	if (k == SngI && s <= sizeof(void *)) // Enclosed object value in a void*
	{
		if (v)
		{
			p = &p; // Point to self

			switch (t)
			{
			case U8: *(uint8_t *)p = *(uint8_t *)v; break;
			case U16: *(uint16_t *)p = *(uint16_t *)v; break;
			case U32: *(uint32_t *)p = *(uint32_t *)v; break;
			case U64: *(uint64_t*)p = *(uint64_t *)v; break;
			case I8: *(int8_t *)p = *(int8_t *)v; break;
			case I16: *(int16_t *)p = *(int16_t *)v; break;
			case I32: *(int32_t *)p = *(int32_t *)v; break;
			case I64: *(int64_t *)p = *(int64_t *)v; break;
			case C8: *(uint8_t *)p = *(uint8_t *)v; break;
			case C16: *(uint16_t *)p = *(uint16_t *)v; break;
			case F32: *(float *)p = *(float *)v; break;
			case F64: *(double *)p = *(double *)v; break;
			case T: *(uint8_t *)p = 1; break;
			case F:
			case X:
			case N: *(uint8_t *)p = 0; break;
			default:
				p = NULL;
				break;
			}
		}
	}
	else if (k == Lst || k == Map)
	{
		if (k == Map)
			n *= 2; // Double items

		jam_t *j = jamAlloc(n);

		if (v && j)
		{
			const jam_t *q = (const jam_t *)v;
			for (jamsize_t i = 0; i < n; ++i)
				jamCopy(j + i, q + i);
		}

		p = j;
	}
	else
	{
		if (nts)
		{
			if (t == C8)
				p = _strClone(v, n);
#ifdef jam_WCHAR
			else
				p = _wstrClone(v, n);
#endif
		}
		else
			p = _memClone(v, s);
	}

	return p;
}

void
jamSet(
	jam_t *j, jamkind_e k, jamfield_t t, jamfield_t s, void *v, jamfield_t n, uint8_t f
)
{
	jam_ASSERT(j);
	jamFree(j); // Assumes object was previously initialized and valid

	j->kind = k;
	j->type = t;
	j->size = jamIsImplicit(k) ? jamWidth((uint8_t)t) : s;
	j->value = jamValue(k, t, s, v, n, f);
	j->items = n;

	if (f & Xfr) // Convert a transfer to bound resource
	{
		f &= ~Xfr;
		f |= Bnd;
	}
	j->flags = f;
}

const char *
jamKindName(uint8_t k)
{
	switch (k)
	{
	case SngI: return "SngI";
	case SngE: return "SngE";
	case Lst: return "Lst";
	case Map: return "Map";
	case Spc: return "Spc";
	case Usr: return "Usr";
	case ArrI: return "ArrI";
	case ArrE: return "ArrE";
	case Und: return "Und";
	default: return "<?>";
	}
}

const char*
jamTypeName(uint8_t t)
{
	switch (t)
	{
	case U8: return "U8";
	case U16: return "U16";
	case U32: return "U32";
	case U64: return "U64";
	case I8: return "I8";
	case I16: return "I16";
	case I32: return "I32";
	case I64: return "I64";
	case C8: return "C8";
	case C16: return "C16";
	case F32: return "F32";
	case F64: return "F64";
	case T: return "T";
	case F: return "F";
	case X: return "X";
	case N: return "N";
	default: return "<?>";
	}
}

jaminfo_t
jaminfoHint(jamid_t i, jamsize_t *h)
{
	jaminfo_t f;
	jamsize_t n = 0;

	f.kind = i >> 4;

	if (f.kind > Map) // Not SngX, Lst or Map
	{
		if (jamIsArray(f.kind)) // ArrX
			f.kind &= ArrE;
		else
			f.kind &= 0xe; // Spc or Usr
	}

	f.type = f.items = 0;

	switch (f.kind)
	{
	case SngI:
		f.type = (i>>0) & 0xf; // Implicit type
		f.size = jamWidth((uint8_t)f.type); // Implicit size in bytes
		break;

	case ArrI:
		f.items = 1<<((i>>5) & 0x3); // Number of items span
		f.type = (i>>0) & 0xf; // Implicit type
		f.size = jamIsEmbedded((uint8_t)f.type) ? 1 : jamWidth((uint8_t)f.type); // Implicit size in bytes
		n = f.items;
		break;

	case ArrE:
		f.items = 1<<((i>>5) & 0x3); // Number of items span
		n = f.items;
	case SngE:
		f.type = 1<<((i>>0) & 0x3); // Type span
		f.size = 1<<((i>>2) & 0x3); // Item size span
		n += f.type + f.size;
		break;

	case Lst:
	case Map:
		f.items = 1<<((i>>0) & 0x3); // Number of items span
		f.size = 1<<((i>>2) & 0x3); // Number of bytes span
		n = f.size + f.items;
		break;

	case Spc:
		f.type = (i>>0) & 0x7; // Special id
		f.size = 1<<((i>>3) & 0x3); // Number of bytes span
		n = f.size;
		break;

	case Usr:
		f.type = (i>>0) & 0x1f; // User-defined id
		f.size = 1<<((i>>3) & 0x3); // Number of bytes span
		n = f.size;
		break;

	case Und:
		break;
	}

	if (h)
		*h = n;

	return f;
}

jamid_t
jamId(const jam_t *j)
{
	jam_ASSERT(j);

	switch (j->kind)
	{
	case SngI: return _jamId(j->kind, (uint8_t)j->type, 0, 0);
	case SngE: return _jamId(j->kind, jamSpan(j->type), jamSpan(j->size), 0);
	case Lst:
	case Map: return _jamId(j->kind, jamSpan(j->items), jamSpan(j->size), 0);
	case Spc:
	case Usr: return _jamId(j->kind, jamSpan(j->size), 0, 0);
	case ArrI: return _jamId(j->kind, jamSpan(j->items), (uint8_t)j->type, 0);
	case ArrE: return _jamId(j->kind, jamSpan(j->items), jamSpan(j->type), jamSpan(j->size));
	}
	return X; // Unknown
}

jamsize_t
jamBytes(const jam_t *j, uint8_t f)
{
	jam_ASSERT(j);

	jamsize_t r;
	bool upd = f & Upd; // Cache updated flag
	f &= ~Upd;
	f = f ? f : Hdr|Val; // Default field flags
	r = (f & Dsc) ? 1 : 0;

	switch (j->kind)
	{
	case SngI:
		if (jamIsEmbedded((uint8_t)j->type))
			r = 1; // Force embedded value
		else if (f & Val)
			r += jamWidth((uint8_t)j->type);
		break;

	case SngE:
		if (f & Inf)
			r += jamSpanBytes(j->size) + jamSpanBytes(j->type);
		if (f & Val)
			r += j->size;
		break;

	case Map:
	case Lst:
		if (!j->size || !upd) // Calculate size field if zero or not updated
			jamUpdate((jam_t *)j, f);
		if (f & Inf)
			r += jamSpanBytes(j->size) + jamSpanBytes(j->items);
		if (f & Val)
			r += j->size;
		break;

	case Spc:
	case Usr:
		if (f & Inf)
			r += jamSpanBytes(j->size);
		if (f & Val)
			r += j->size;
		break;

	case ArrI:
		if (f & Inf)
			r += jamSpanBytes(j->items);
		if (f & Val)
			r += jamIsEmbedded((uint8_t)j->type) ? j->items : j->items*jamWidth((uint8_t)j->type);
		break;

	case ArrE:
		if (f & Inf)
			r += jamSpanBytes(j->items) + jamSpanBytes(j->size) + jamSpanBytes(j->type);
		if (f & Val)
			r += j->items*j->size;
		break;

	default: // Gets encoded as embedded unknown value
		r = 1;
		break;
	}
	return r;
}

void
jamUpdate(jam_t *j, uint8_t f)
{
	jam_ASSERT(j);

	bool nrc = f & Nrc; // Cache non-recurse flag
	f &= ~Nrc;
	f = f ? f : Hdr|Val;
	jamsize_t l = j->items;

	switch (j->kind)
	{
	case SngI:
		j->size = jamWidth((uint8_t)j->type);
		break;

	case ArrI:
		j->size = jamIsEmbedded((uint8_t)j->type) ? 1 : jamWidth((uint8_t)j->type);
		break;

	// case SngE:

	case Map:
		l *= 2;
	case Lst:
		j->size = 0;
		for (jamsize_t i = 0; i < l; ++i)
			j->size += (nrc) ? ((jam_t *)j->value)[i].size : jamBytes(((jam_t *)j->value) + i, f);
		break;

	// case Spc:
	// case Usr:
	// case ArrE:

	case Und:
		j->size = 0;
		break;

	default:
		break;
	}
}

void
jamCopy(jam_t *j, const jam_t *q)
{
	jam_ASSERT(j && q);

	if (j == q)
		return;

	jamFree(j);

	j->kind = q->kind;
	j->type = q->type;
	j->size = q->size;
	j->value = jamValue(q->kind, q->type, q->size, jamPtr(q), q->items, q->flags);
	j->items = q->items;
	j->flags = q->flags;
}

void
jamCopyStr(jam_t *j, const char *s)
{
	jam_ASSERT(j);
	jamsize_t sl = s ? strlen(s) : 0;
	jamC8Arr(j, NULL, sl); // @note Since size is known, this is not null-terminated
	strncpy(jamPtr(j), s, sl);
}

void
jamFree(jam_t *j)
{
	if (!jamIsOk(j))
		return;

	if (jamIsDynamic(j))
	{
		if (j->kind == Lst || j->kind == Map) // Container
			jamDealloc((jam_t *)j->value, (j->kind == Lst) ? j->items : j->items*2);
		else
		{
			_memDealloc(j->value);
			j->value = NULL;
		}
	}

	j->kind = Und; // Invalidate object
	// j->value = NULL;
}

bool
jamCompare(const jam_t *p, const jam_t *q)
{
	jam_ASSERT(p && q);

	if (p->kind != q->kind || p->type != q->type)
		return false;

	jamsize_t pl = jamBytes(p, Val), ql = jamBytes(q, Val);
	if (pl != ql)
		return false;

	void *pv = jamPtr(p), *qv = jamPtr(q);
	if (pv == qv) // Point to same memory or are NULL
		return true;

	if (!pv || !qv)
		return false;

	return !memcmp(pv, qv, pl);
}

/** Our own implementation of strnlen. */
static
int _strnlen(const char *pv, size_t l)
{
	int r = 0;
	if (pv)
	{
		while (*pv && r < l)
			++pv, ++r;
	}
	return r;
}

/** Our own implementation of wcsnlen. */
static
int _wcsnlen(const wchar_t *pv, size_t l)
{
	int r = 0;
	if (pv)
	{
		while (*pv && r < l)
			++pv, ++r;
	}
	return r;
}

bool
jamCompareStr(const jam_t *p, const char *s)
{
	jam_ASSERT(p && s);

	if (p->kind != ArrI || p->type != C8)
		return false;

	jamsize_t pl = p->items, sl = strlen(s);
	char *pv = jamPtr(p);
	if (!pv)
		return false;

	pl = _strnlen(pv, pl); // Discard null bytes

	return (pl != sl) ? false : !strncmp(pv, s, pl);
}

#ifdef jam_WCHAR
bool
jamCompareWstr(const jam_t *p, const wchar_t *s)
{
	jam_ASSERT(p && s);

	if (p->kind != ArrI || p->type != C16)
		return false;

	jamsize_t pl = p->items, sl = wcslen(s);
	wchar_t *pv = jamPtr(p);
	if (!pv)
		return false;

	pl = _wcsnlen(pv, pl); // Discard null bytes

	return (pl != sl) ? false : !wcsncmp(pv, s, pl);
}
#endif

void *
jamItem(const jam_t *j, jamsize_t n)
{
	jam_ASSERT(j);
	jamsize_t l = j->items;

	switch (j->kind)
	{
	case Map:
		l *= 2;
	case Lst:
		if (n < l)
			return (jam_t *)(j->value) + n;
		break;

	case ArrI:
		if (n < l)
		{
			if (!jamIsEmbedded((uint8_t)j->type))
				n *= jamWidth((uint8_t)j->type);
			return (uint8_t *)jamPtr(j) + n;
		}
		break;

	case ArrE:
		if (n < l)
		{
			n *= j->size;
			return (uint8_t *)jamPtr(j) + n;
		}
		break;

	default:
		return jamPtr(j);
	}

	return NULL;
}

jam_t *
jamElement(const jam_t *j, jamsize_t n)
{
	jam_ASSERT(j);
	jamsize_t l = j->items;

	if (n < l)
		switch (j->kind)
		{
		case Map:
			n = 2*n+1;
		case Lst:
			return (jam_t *)(j->value) + n;
		}

	return NULL;
}

jam_t *
jamKey(const jam_t *m, jamsize_t n)
{
	jam_ASSERT(m);
	if (m->kind != Map || n >= m->items) // Only works for maps
		return NULL;

	n *= 2;
	return (jam_t *)(m->value) + n;
}

jam_t *
jamEntry(const jam_t *m, const jam_t *k)
{
	jam_ASSERT(m && k);

	if (m->kind != Map) // Only works for maps
		return NULL;

	jamsize_t l = m->items;
	jam_t *v = (jam_t *)(m->value);

	while (l--)
	{
		if (jamCompare(v++, k))
			return v;
		else
			++v;
	}

	return NULL;
}

jam_t *
jamEntryStr(const jam_t *m, const char *k)
{
	jam_ASSERT(m && k);

	if (m->kind != Map) // Only works for maps
		return NULL;

	jamsize_t l = m->items;
	jam_t *v = (jam_t *)(m->value);

	while (l--)
	{
		if (jamCompareStr(v++, k))
			return v;
		else
			++v;
	}

	return NULL;
}

int
jamFormat(const jam_t *j, uint8_t *b, jamsize_t bl, uint8_t f)
{
	jam_ASSERT(j);
	jamsize_t r = 0; // Track number of bytes written
	bool skp = f & Skp;
	f &= ~Skp;
	f = f ? f : Hdr|Val; // Default flags

	if (!skp)
	{
		if (bl < jamBytes(j, f))
			return EOverflow; // No space
	}

	jamid_t d = jamId(j); // Calculate object descriptor byte
	jaminfo_t g = jaminfoHint(d, NULL);

	if (f & Dsc) // Add descriptor as prefix
		r += jamWrite(b, r, &d, sizeof(jamid_t));

	jamsize_t l = j->items;

	switch (j->kind)
	{
	case SngI:
		if (jamIsEmbedded((uint8_t)j->type))
		{
			if (!(f & Dsc)) // Write translated value instead of embedded id
			{
				uint8_t v = (j->type == True) ? 1 : 0;
				r += jamWrite(b, r, &v, 1);
			}
		}
		else if (f & Val)
			r += jamWrite(b, r, jamPtr(j), g.size);
		break;

	case SngE:
		if (f & Inf)
		{
			r += jamWrite(b, r, &j->size, g.size);
			r += jamWrite(b, r, &j->type, g.type);
		}
		if (f & Val)
			r += jamWrite(b, r, jamPtr(j), j->size);
		break;

	case Map:
		l *= 2;
	case Lst:
		if (f & Inf)
		{
			r += jamWrite(b, r, &j->size, g.size);
			r += jamWrite(b, r, &j->items, g.items);
		}
		if (f & Val)
			for (jamsize_t i = 0; i < l; ++i)
				r += jamFormat((const jam_t *)j->value + i, b + r, bl - r, f|Skp);
		break;

	case Spc:
	case Usr:
		if (f & Inf)
			r += jamWrite(b, r, &j->size, g.size);
		if (f & Val)
			r += jamWrite(b, r, jamPtr(j) /*j->value*/, j->size);
		break;

	case ArrI:
		if (f & Inf)
			r += jamWrite(b, r, &j->items, g.items);
		if (f & Val)
			r += jamWrite(b, r, jamPtr(j) /*j->value*/, l*g.size);
		break;

	case ArrE:
		if (f & Inf)
		{
			r += jamWrite(b, r, &j->items, g.items);
			r += jamWrite(b, r, &j->size, g.size);
			r += jamWrite(b, r, &j->type, g.type);
		}
		if (f & Val)
			r += jamWrite(b, r, jamPtr(j) /*j->value*/, l*j->size);
		break;
	}

	return (int)r;
}

int
jamFormatArray(const jam_t *a, jamsize_t l, uint8_t *b, jamsize_t bl, uint8_t f)
{
	jam_ASSERT(a);
	jamsize_t r = 0;
	f = f ? f : Dsc|Val;

	// Check buffer length
	if (!(f & Skp))
	{
		for (jamsize_t i = 0; i < l; ++i)
			r += jamBytes(a + i, f);

		if (bl < r)
			return EOverflow; // No space

		r = 0;
	}

	for (jamsize_t i = 0; i < l; ++i)
		r += jamFormat(a + i, b + r, bl - r, f|Skp);

	return (int)r;
}

int
jamParse(jam_t *j, const uint8_t *b, jamsize_t bl, uint8_t f)
{
	jam_ASSERT(j);
	jamsize_t r = 0; // Track number of bytes read
	jamid_t d = 0;
	bool nts = f & Nts; // Cache Nts flag
	f &= ~Nts;
	f = f ? f : Hdr|Val; // Default flags

	if (f & Dsc) // Read descriptor from buffer
	{
		if (bl < 1)
			return EFormat;
		r += jamRead(b, r, &d, sizeof(jamid_t));
	}
	else // Use object existing id
	{
		if (!jamIsOk(j))
			return EArgument;
		d = jamId(j);
	}

	jamsize_t h = 0;
	jaminfo_t g = jaminfoHint(d, &h);

	if (bl < r + h)
		return EFormat; // Not enough bytes

	void *tv = NULL;

	switch (g.kind)
	{
	case SngI:
		if (f & Val)
		{
			tv = jamIsEnclosed((uint8_t)g.type) ? &tv : _memAlloc(g.size, -1);
			if (!tv)
				return EMemory;
			r += jamRead(b, r, tv, g.size);
		}
		break;

	case SngE:
		if (f & Inf)
		{
			r += jamRead(b, r, &g.size, g.size);
			r += jamRead(b, r, &g.type, g.type);
		}
		else
		{
			if (!jamIsOk(j))
				return EArgument;
			g.size = j->size, g.type = j->type;
		}

		if ((f & Val) && g.size)
		{
			tv = _memAlloc(g.size, -1);
			if (!tv)
				return EMemory;
			r += jamRead(b, r, tv, g.size);
		}
		break;

	case Map:
	case Lst:
		if (f & Inf)
		{
			r += jamRead(b, r, &g.size, g.size);
			r += jamRead(b, r, &g.items, g.type);
		}
		else // Unusual
		{
			if (!jamIsOk(j))
				return EArgument;
			g.size = j->size, g.items = j->items;
		}

		if ((f & Val) && g.items)
		{
			jamsize_t l = (g.kind == Map) ? 2 * g.items : g.items;
			jam_t *ja = jamAlloc(l);
			if (!ja)
				return EMemory;

			for (jamsize_t i = 0; i < l; ++i)
			{
				int q = jamParse(ja + i, b + r, bl - r, f);
				if (q < 0)
				{
					jamDealloc(ja, i+1);
					return q;
				}
				else
					r += q;
			}
			tv = ja;
		}
		break;

	case Spc:
	case Usr:
		if (f & Inf)
			r += jamRead(b, r, &g.size, g.size);
		else
		{
			if (!jamIsOk(j))
				return EArgument;
			g.size = j->size;
		}

		if ((f & Val) && g.size)
		{
			tv = _memAlloc(g.size, -1);
			if (!tv)
				return EMemory;
			r += jamRead(b, r, tv, g.size);
		}
		break;

	case ArrI:
		if (f & Inf)
			r += jamRead(b, r, &g.items, g.items);
		else
		{
			if (!jamIsOk(j))
				return EArgument;
			g.items = j->items;
		}

		if ((f & Val) && g.items && g.size)
		{
			nts = nts && (g.type == C8 || g.type == C16);
			jamsize_t l = g.size * g.items;
			tv = _memAlloc(l + (nts ? g.size : 0), -1);
			if (!tv)
				return EMemory;
			r += jamRead(b, r, tv, l);
			if (nts)
			{
				if (g.type == C8)
					*((char *)tv + l) = '\0';
#ifdef jam_WCHAR
				else
					*((wchar_t *)tv + l) = 0;
#endif
			}
		}
		break;

	case ArrE:
		if (f & Inf)
		{
			r += jamRead(b, r, &g.items, g.items);
			r += jamRead(b, r, &g.size, g.size);
			r += jamRead(b, r, &g.type, g.type);
		}
		else
		{
			if (!jamIsOk(j))
				return EArgument;
			g.size = j->size, g.type = j->type, g.items = j->items;
		}

		if ((f & Val) && g.items && g.size)
		{
			jamsize_t l = g.size * g.items;
			tv = _memAlloc(l, -1);
			if (!tv)
				return EMemory;
			r += jamRead(b, r, tv, l);
		}
		break;

	case Und:
		break;
	}

	jamSet(j, g.kind, g.type, g.size, tv, g.items, Xfr);

	return (int)r;
}

/*
jamsize_t
jamLink(jamdef_t *d, jamsize_t l, void *s)
{
	jamsize_t r = 0;

	for (jamsize_t m = 0; m < l; ++m)
	{
		jaminfo_t *i = &d[m].info;
		uint8_t *p = (uint8_t *)s + r;
		if (jamIsArray(i->kind) && !i->items) // Internal array
		{
			i->value = p;
			r += sizeof(void *);
		}
		else
		{
			i->value = p;
			// i->flags = jFix;
			r += jamBytes(i, jVal);
		}
	}

	return r;
}
*/

static int
_jamEncode(uint8_t *b, jamsize_t l, const jamdef_t *d, jamsize_t n, const void *v)
{
	jam_ASSERT(d);
	jamsize_t r = 0; // Return bytes
	jamsize_t p, q;

	for (jamsize_t i = 0; i < n; ++i)
	{
		const jaminfo_t *g = &d[i].info;

		switch (g->kind)
		{
		case SngI:
			// Type and size is known, so only value is encoded
			q = (jamIsEmbedded((uint8_t)g->type)) ? sizeof(uint8_t) : jamWidth((uint8_t)g->type); // Embedded value assumed uint8_t
			if (b && v)
			{
				if (r + q > l)
					return EOverflow; // Not enough space
				jamWrite(b, r, v, q);
			}
			p = q;
			break;

		case SngE:
		case Map:
		case Lst:
		case Spc:
		case Usr:
			// SngE jam object, needs all fields to be serialized
			// Assume size is correct if not zero, otherwise it will be updated when calculating bytes
			{
				jam_t *j = (jam_t *)v;
				q = jamBytes(j, 0);
				if (b && v)
				{
					if (r + q > l)
						return EOverflow; // Not enough space
					jamFormat(j, b + r, q, Skp);
				}
			}
			p = sizeof(jam_t);
			break;

		case ArrI:
			// Type and item size known
			if (g->items) // Fixed size array
			{
				q = (jamIsEmbedded((uint8_t)g->type)) ? sizeof(uint8_t) : jamWidth((uint8_t)g->type);
				q *= g->items;
				if (b && v)
				{
					if (r + q > l)
						return EOverflow; // Not enough space
					jamWrite(b, r, v, q);
				}
				p = q;
			}
			else // Dynamic size, null-terminated string or jam object
			{
				if (g->type == C8 || g->type == C16)
				{
					jam_t j = jamINIT;
					if (g->type == C8)
						jamStrC(&j, *(char **)v);
					else
#ifdef jam_WCHAR
						jamWstrC(&j, *(wchar_t **)v);
#else
						return EFail; // Unsupported
#endif
					q = jamBytes(&j, 0);
					if (b && v)
					{
						if (r + q > l)
							return EOverflow; // Not enough space
						jamFormat(&j, b + r, q, Skp);
					}
					p = sizeof(void *);
				}
				else
				{
					jam_t *j = (jam_t *)v;
					q = jamBytes(j, 0);
					if (b && v)
					{
						if (r + q > l)
							return EOverflow; // Not enough space
						jamFormat(j, b + r, q, Skp);
					}
					p = sizeof(jam_t);
				}
			}
			break;

		case ArrE:
			// Explicit array, all items need to have same size
			if (g->items && g->size) // Fixed size array
			{
				q = g->items * g->size;
				if (b && v)
				{
					if (r + q > l)
						return EOverflow; // Not enough space
					jamWrite(b, r, v, q);
				}
				p = q;
			}
			else // Dynamic size jam object
			{
				jam_t *j = (jam_t *)v;
				q = jamBytes(j, 0);
				if (b && v)
				{
					if (r + q > l)
						return EOverflow; // Not enough space
					jamFormat(j, b + r, q, Skp);
				}
				p = sizeof(jam_t);
			}
			break;

		default:
			return EFail;
		}

		if (v)
			v = (uint8_t *)v + p;

		r += q;
	}

	return (int)r;
}

int
jamEncode(jam_t *j, jamfield_t t, const jamdef_t *d, jamsize_t l, const void *v)
{
	jam_ASSERT(j && d);
	// Calculate necessary bytes
	int r = _jamEncode(NULL, 0, d, l, v);

	if (r < 0) // Error
		return r;

	uint8_t *b = _memAlloc(r, -1);
	jamSet(j, SngE, t, r, b, 0, Xfr);
	return _jamEncode(b, r, d, l, v);
}

static int
_jamDecode(const uint8_t *b, jamsize_t l, const jamdef_t *d, jamsize_t n, void *v)
{
	jam_ASSERT(d);
	jamsize_t r = 0; // Processed bytes
	jamsize_t p;
	int q;

	for (jamsize_t i = 0; i < n; ++i)
	{
		const jaminfo_t *g = &d[i].info;

		switch (g->kind)
		{
		case SngI:
			// Type and size is known, so only value is encoded
			q = (jamIsEmbedded((uint8_t)g->type)) ? sizeof(uint8_t) : jamWidth((uint8_t)g->type);
			if (b && v)
			{
				if (r + q > l)
					return EFormat; // Not enough bytes
				jamRead(b, r, v, q);
			}
			p = q;
			break;

		case SngE:
		case Map:
		case Lst:
		case Spc:
		case Usr:
			// All fields are serialized
			{
				jam_t j = jamINIT;
				q = jamParse(&j, b + r, l - r, 0 /*Nts*/);
				if (q < 0)
					return q; // Error

				if (j.kind != g->kind)
				{
					jamFree(&j);
					return EData; // Wrong kind
				}

				jamFree((jam_t *)v);
				*(jam_t *)v = j;
			}
			p = sizeof(jam_t);
			break;

		case ArrI:
			// Type and item size known
			if (g->items) // Fixed size array
			{
				q = (jamIsEmbedded((uint8_t)g->type)) ? sizeof(uint8_t) : jamWidth((uint8_t)g->type);
				q *= g->items;
				if (b && v)
				{
					if (r + q > l)
						return EFormat; // Not enough bytes
					jamRead(b, r, v, q);
				}
				p = q;
			}
			else // Dynamic size, null-terminated string or jam object
			{
				jam_t j = jamINIT;
				q = jamParse(&j, b + r, l - r, Nts); // Will create bounded null-terminated strings
				if (q < 0)
					return q; // Error

				if (j.kind != g->kind || j.type != g->type)
				{
					jamFree(&j);
					return EData; // Wrong kind/type
				}

				if (g->type == C8)
				{
					*(char **)v = j.value; // Acquire string
					p = sizeof(char *);
				}
				else if (g->type == C16)
#ifdef jam_WCHAR
				{
					*(wchar_t **)v = j.value; // Acquire string
					p = sizeof(wchar_t *);
				}
#else
				{
					jamFree(&j);
					return EFail; // Unsupported
				}
#endif
				else
				{
					jamFree((jam_t *)v);
					*(jam_t *)v = j;
					p = sizeof(jam_t);
				}
			}
			break;

		case ArrE:
			// Explicit array, all items need to have same size
			if (g->items && g->size) // Fixed size array
			{
				q = g->items * g->size;
				if (b && v)
				{
					if (r + q > l)
						return EFormat; // Not enough bytes
					jamRead(b, r, v, q);
				}
				p = q;
			}
			else // Dynamic size jam object
			{
				jam_t j = jamINIT;
				q = jamParse(&j, b + r, l - r, 0);
				if (q < 0)
					return q; // Error

				if (j.kind != g->kind || j.type != g->type)
				{
					jamFree(&j);
					return EData; // Wrong kind/type
				}

				jamFree((jam_t *)v);
				*(jam_t *)v = j;
				p = sizeof(jam_t);
			}
			break;

		default: // Gets encoded as embedded unknown value
			return EArgument; // Unknown
		}

		if (v) // Advance v pointer
			v = (uint8_t *)v + p;

		r += q;
	}

	return (int)r;
}


int
jamDecode(const jam_t *j, const jamdef_t *d, jamsize_t n, void *v)
{
	jam_ASSERT(j && d);

	if (j->kind != SngE)
		return EArgument;
	else
		return _jamDecode(j->value, j->size, d, n, v);
}


int
jamPurge(const jamdef_t *d, jamsize_t l, void *v)
{
	jam_ASSERT(d);

	if (!v)
		return EArgument;

	jamsize_t p;

	for (jamsize_t i = 0; i < l; ++i)
	{
		const jaminfo_t *g = &d[i].info;

		switch (g->kind)
		{
		case SngI:
			p = (jamIsEmbedded((uint8_t)g->type)) ? sizeof(uint8_t) : jamWidth((uint8_t)g->type);
			break;

		case SngE:
		case Map:
		case Lst:
		case Spc:
		case Usr:
			{
				jam_t *j = (jam_t *)v;
				if (j->kind != g->kind || j->type != g->type)
					return EData;
				jamFree(j);
			}
			p = sizeof(jam_t);
			break;

		case ArrI:
			if (g->items) // Fixed
			{
				p = (jamIsEmbedded((uint8_t)g->type)) ? sizeof(uint8_t) : jamWidth((uint8_t)g->type);
				p *= g->items;
			}
			else
			{
				if (g->type == C8)
				{
					_memDealloc(*(char **)v);
					p = sizeof(char *);
				}
				else if (g->type == C16)
#ifdef jam_WCHAR
				{
					_memDealloc(*(wchar_t **)v);
					p = sizeof(wchar_t *);
				}
#else
					return EFail; // Unsupported
#endif
				else
				{
					jam_t *j = (jam_t *)v;

					if (j->kind != g->kind || j->type != g->type)
						return EData;

					jamFree(j);
					p = sizeof(jam_t);
				}
			}
			break;

		case ArrE:
			// Explicit array, all items need to have same size
			if (g->items && g->size) // Fixed size array
				p = g->items * g->size;
			else // Dynamic size jam object
			{
				jam_t *j = (jam_t *)v;

				if (j->kind != g->kind || j->type != g->type)
					return EData; // Wrong kind/type

				jamFree(j);
				p = sizeof(jam_t);
			}
			break;

		default: // Gets encoded as embedded unknown value
			return EArgument; // Unknown
		}

		v = (uint8_t *)v + p;
	}

	return Ok;
}

bool
jamCheck(const jam_t *j, const jaminfo_t *i)
{
	jam_ASSERT(j);

	if (!i)
		return true; // Unspecified

	switch (i->kind)
	{
	case SngE:
	// @note Two SngE objects of same type can have different sizes to allow flexible size structs
		if (i->size && j->size != i->size) // Size is specified
			return false;
	case SngI:
		return (j->kind == i->kind && j->type == i->type);

	case Map:
	case Lst:
		return (j->kind == i->kind);

	case Spc:
	case Usr:
		return (j->kind == i->kind);

	case ArrE:
		if (j->size != i->size)
			return false;
	case ArrI:
		if (j->kind != i->kind || j->type != i->type)
			return false;
		else
			return (i->items) ? (j->items == i->items) : true; // Check for fixed size

	default:
		return false;
	}
}

bool
jamCheckItems(const jam_t *j, const jaminfo_t *i, jamsize_t l)
{
	jam_ASSERT(j);
	jamsize_t n = (j->kind == Map) ? 2*j->items : j->items;

	if (j->kind != Lst || j->kind != Map || n != l)
		return false;

	if (i)
	{
		for (jamsize_t m = 0; m < l; ++m)
			if (!jamCheck((jam_t *)jamItem(j, m), i + m))
				return false;
	}

	return true;
}

bool
jamCheckElements(const jam_t *j, const jaminfo_t *i, jamsize_t l)
{
	jam_ASSERT(j);
	if (j->kind != Lst || j->kind != Map || j->items != l)
		return false;

	if (i)
	{
		for (jamsize_t m = 0; m < l; ++m)
			if (!jamCheck(jamElement(j, m), i + m))
				return false;
	}

	return true;
}


