/** @file
	Jam data serialization implemented in C.
*/

#ifndef JAM_H_
#define JAM_H_

#include "jamdef.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
// #if (defined(jam_MEMHEADER) && (jam_MEMHEADER != 0))
// #include jam_MEMHEADER
// #endif

#define j___(p,x) p##x
#define j__(p,x) j___(p,x)
#define j_(x) j__(jam_PREFIX,x)

#ifdef __cplusplus
extern "C" {
#endif

/** Result codes */
typedef enum
{
	j_(Ok) = 0, ///< Success
	j_(Err) = -1, ///< Generic error
	j_(Error) = j_(Err), ///< Generic error
	j_(ESystem) = -16384, ///< System error
	j_(ERange), ///< Out of range
	j_(EArgument), ///< Invalid argument
	j_(EOverflow), ///< Overflow occurred/could occurr
	j_(ETimeout), ///< Timed out
	j_(EFormat), ///< Bad format
	j_(EBusy), ///< Resource busy
	j_(ECancel), ///< Cancelled
	j_(EFail), ///< Request failed
	j_(EAbort), ///< Aborted
	j_(ERefuse), ///< Refused
	j_(EReset), ///< Reset
	j_(EExist), ///< Resource already exists
	j_(EIo), ///< Input/output error
	j_(EData), ///< Data error/not available
	j_(EMemory), ///< Memory error/not available
	j_(EPermission), ///< Permission error
	j_(EUnknown), ///< Unknown error
} jamcode_t;

/** Object kind enumeration */
typedef enum
{
	// Object kind
	j_(SngI) = 0x0, ///< Singleton implicit
	j_(SngE), ///< Singleton explicit
	j_(Lst), ///< List
	j_(Map), ///< Map
	j_(Spc), ///< Special
	j_(Usr) = 0x6, ///< User reserved
	j_(ArrI) = 0x8, ///< Array implicit
	j_(ArrE), ///< Array explicit
	j_(Und) = 0xff, ///< Undefined (not really a kind, but used for uninitialized objects)

	// Aliases
	j_(SingleImplicit) = j_(SngI),
	j_(SingleExplicit) = j_(SngE),
	j_(List) = j_(Lst),
	j_(Special) = j_(Spc),
	j_(User) = j_(Usr),
	j_(ArrayImplicit) = j_(ArrI),
	j_(ArrayExplicit) = j_(ArrE),
	j_(Undefined) = j_(Und),
} jamkind_e;

/** Object type enumeration */
typedef enum
{
	// Implicit object size / Explicit object items/type field / Cardinality items (2 bits)
	j_(b8) = 0x0, ///< 1-byte object, array with 1-byte size
	j_(b16) = 0x1, ///< 2-byte object, array with 2-byte size
	j_(b32) = 0x2, ///< 4-byte object, array with 4-byte size
	j_(b64) = 0x3, ///< 8-byte object, array with 8-byte size or embedded size

	// Implicit object type (2 bits)
	j_(Uns) = 0x0, ///< Unsigned integer
	j_(Sig) = 0x1, ///< Signed integer
	j_(Flt) = 0x2, ///< Floating point
	j_(Chr) = j_(Flt), ///< 8-bit or 16-bit char (string if array)
	// j_(Bit) = 0x3, ///< Bitfield object
	j_(Emb) = 0x3, ///< Embedded value

	// Aliases
	j_(Unsigned) = j_(Uns),
	j_(Signed) = j_(Sig),
	j_(Float) = j_(Flt),
	j_(Char) = j_(Chr),
	j_(Embedded) = j_(Emb),

	// Special object ids
	j_(SpcT) = 0x7, ///< Type definition/footprint

	// Type objects definitions and transmission (upper 2 bits)
	j_(SpcTF) = 0x0, ///< Type form footprint (serialization id array, no names)
	j_(SpcTN), ///< Type definition (name-type map)
	j_(SpcTNV), ///< Full type definition plus default values (name-type-value map)
	j_(SpcTV), ///< Encoded value transmission

	// Aliases
	j_(SpecialTypeForm) = j_(SpcTF),
	j_(SpecialTypeName) = j_(SpcTN),
	j_(SpecialTypeNameValue) = j_(SpcTNV),

	// Implicit type field aliases
	j_(U8) = (j_(Uns)<<2)|(j_(b8)), ///< uint8_t
	j_(U16) = (j_(Uns)<<2)|(j_(b16)), ///< uint16_t
	j_(U32) = (j_(Uns)<<2)|(j_(b32)), ///< uint32_t
	j_(U64) = (j_(Uns)<<2)|(j_(b64)), ///< uint64_t
	j_(I8) = (j_(Sig)<<2)|(j_(b8)), ///< int8_t
	j_(I16) = (j_(Sig)<<2)|(j_(b16)), ///< int16_t
	j_(I32) = (j_(Sig)<<2)|(j_(b32)), ///< int32_t
	j_(I64) = (j_(Sig)<<2)|(j_(b64)), ///< int64_t
	j_(C8) = (j_(Chr)<<2)|(j_(b8)), ///< ASCII (UTF-8)
	j_(C16) = (j_(Chr)<<2)|(j_(b16)), ///< Wide char (UTF-16)
	j_(F32) = (j_(Flt)<<2)|(j_(b32)), ///< Float
	j_(F64) = (j_(Flt)<<2)|(j_(b64)), ///< Double
	j_(T) = (j_(Emb)<<2)|0x0, ///< True
	j_(F) = (j_(Emb)<<2)|0x1, ///< False
	j_(X) = (j_(Emb)<<2)|0x2, ///< Unknown
	j_(N) = (j_(Emb)<<2)|0x3, ///< Null value

	// Aliases
	j_(Uint8) = j_(U8),
	j_(Uint16) = j_(U16),
	j_(Uint32) = j_(U32),
	j_(Uint64) = j_(U64),
	j_(Int8) = j_(I8),
	j_(Int16) = j_(I16),
	j_(Int32) = j_(I32),
	j_(Int64) = j_(I64),

	j_(True) = j_(T),
	j_(False) = j_(F),
	j_(Unknown) = j_(X),
	j_(Null) = j_(N),

	// B1 = (Bit<<2)|0x0, ///< 1-bit (boolean)
	// B2 = (Bit<<2)|0x1, ///< 2-bit
	// B4 = (Bit<<2)|0x2, ///< 4-bit (nibble)
	// B7 = (Bit<<2)|0x3, ///< 7-bit
	// B8 = T, ///< 8-bit boolean
} jamtype_e;

/** Flags */
typedef enum
{
	// Object control flags
	j_(Bnd) = 1<<0, ///< Bound object (object is owner of dynamical resources)
	j_(Fix) = 1<<1, ///< Object is of fixed size
	j_(Cst) = 1<<2, ///< Object is constant (read-only)
	j_(Nts) = 1<<6, ///< Use null-terminated strings
	j_(Xfr) = 1<<7, ///< Transfer external dynamic value to object (becomes bounded)

	// Aliases
	j_(Bound) = j_(Bnd),
	j_(Fixed) = j_(Fix),
	j_(Constant) = j_(Cst),
	j_(Nullterm) = j_(Nts),
	j_(Transfer) = j_(Xfr),

	// Formatting flags
	j_(Dsc) = 1<<0, ///< Descriptor id byte
	j_(Inf) = 1<<1, ///< Include object serialized information (type, size, items)
	j_(Val) = 1<<2, ///< Include value
	j_(Nrc) = 1<<3, ///< Non-recurse list/map traversal (default is recursive)
	j_(Upd) = 1<<4, ///< Inform list/map size is up to date
	j_(Tag) = 1<<5, ///< Include name tag (in type)
	j_(Skp) = 1<<6, ///< Skip checks (eg for recursive calls)
	j_(Hdr) = j_(Dsc)|j_(Inf), ///< Object header (descriptor byte + information fields) alias

	// Aliases
	j_(Descriptor) = j_(Dsc),
	j_(Information) = j_(Inf),
	j_(Value) = j_(Val),
	j_(Nonrecurse) = j_(Nrc),
	j_(Updated) = j_(Upd),
	j_(Skip) = j_(Skp),
	j_(Header) = j_(Hdr),
} jamflag_e;

typedef uint8_t jamid_t; ///< Serialization id byte
typedef jam_FIELDTYPE jamfield_t; ///< Size to store type/size/items field values
typedef jam_SIZETYPE jamsize_t; ///< Size field

/** Structure to store serialization id information */
typedef struct jaminfo_t jaminfo_t;
struct jaminfo_t
{
	jamkind_e kind; ///< Object kind
	jamfield_t type; ///< Implicit type, explicit type/span, special id, user id
	jamfield_t size; ///< Explicit item size/span, list/map byte size/span
	jamfield_t items; ///< Array number of items/span
};

/** Default undefined jaminfo object */
#define jaminfoINIT { 0xff /*j_(Und)*/, 0, 0, 0 }
#define jaminfoVar(j) jaminfo_t j = jaminfoINIT

/** Serialized element information */
typedef struct jam_t jam_t;
struct jam_t
{
	uint8_t kind; ///< Object kind
	jamfield_t type; ///< Implicit type / Explicit item type field value
	jamfield_t size; ///< Item size field value
	void *value; ///< Item value pointer (externally provided or dynamically allocated)
	jamfield_t items; ///< Array number of items field value
	uint8_t flags; ///< Object control flags

	// jamsize_t space; ///< Allocated space for value in bytes
};

/** Default undefined jam object */
#define jamINIT { 0xff /*j_(Und)*/, 0, 0, NULL, 0, 0 }
#define jamVar(j) jam_t j = jamINIT

/** Initialize jaminfo */
static inline void
jaminfoSetup(jaminfo_t *i, jamkind_e k, jamfield_t t, jamfield_t s, jamfield_t n)
{
	jam_ASSERT(i);
	i->kind = k, i->type = t, i->size = s, i->items = n;
}

/** Initialize jam objects */
static inline void jamSetup(jam_t *j, jamsize_t n)
{
	jam_ASSERT(j);
	while (n--)
		j++->kind = j_(Und);
}

/** Build object value
	For a value that is not bound, value pointer is passed through.
	Otherwise, a new value is created.
	@param k Object kind
	@param t Object type
	@param s Object item size
	@param v Object external value pointer
	@param n Object number of items
	@param f Object flags
	@return Value pointer
*/
void *jamValue(
	jamkind_e k, jamfield_t t, jamfield_t s, const void *v, jamfield_t n, uint8_t f
);

/** Calculate span field for value (b8-b64) */
static inline jamid_t jamSpan(jamfield_t v)
{
	return (v <= UINT8_MAX) ? j_(b8)
	: (v <= UINT16_MAX) ? j_(b16)
	: (v <= UINT32_MAX) ? j_(b32)
	: j_(b64);
}

/** Get number of storage bytes for value */
static inline uint8_t jamSpanBytes(jamfield_t v)
{
	return (uint8_t)(1<<jamSpan(v));
}

/** Get implicit type size in bytes */
static inline uint8_t jamWidth(uint8_t t)
{
	if (t < (j_(Emb)<<2))
		return 1<<(t & 0x3);
	else
		return 0; // Embedded (value part of id in SngI, but present in ArrI)
}

/** Check if object type is Embedded */
static inline bool jamIsEmbedded(uint8_t t)
{
	return (t >= (j_(Emb)<<2));
}

/** Check if object kind is valid */
static inline bool jamIsKind(uint8_t k)
{
	return (k <= j_(ArrE) && k != 0x5 && k != 0x7);
}

/** Check if object kind is implicit */
static inline bool jamIsImplicit(uint8_t k)
{
	return (k == j_(SngI)) || (k == j_(ArrI));
}

/** Check if object kind is explicit */
static inline bool jamIsExplicit(uint8_t k)
{
	return (k == j_(SngE)) || (k == j_(ArrE));
}

/** Check if object kind is singleton */
static inline bool jamIsSingle(uint8_t k)
{
	return k <= j_(SngE); // SngI or SngE
}

/** Check if object kind is array */
static inline bool jamIsArray(uint8_t k)
{
	return (k & 0x8) ? true : false; // Check bit 7 is set
}

/** Check if implicit type can be enclosed inside void pointer */
static inline bool jamIsEnclosed(uint8_t t)
{
	uint8_t w = jamWidth(t);
	return (w > 0) && (w <= sizeof(void *));
}

/** Create a serialization id
	@note Arguments have different meanings depending on the object kind.
*/
static inline jamid_t _jamId(uint8_t k, uint8_t a, uint8_t b, uint8_t c)
{
	switch (k)
	{
	case j_(SngI): // a:implicit type
	case j_(SngE): // a:type span, b:size span
	case j_(Lst): // a:items span, b:bytes span
	case j_(Map): // a:items span, b:bytes span
		return (k<<4) | (b<<2) | (a<<0);
	case j_(Spc): // a:special id, b:bytes span
	case j_(Usr): // a:user id, b:bytes span (opt)
		return (k<<4) | (b<<3) | (a<<0);
	case j_(ArrI): // a:items span, b:implicit type
	case j_(ArrE): // a:items span, b:type span, c:size span
		return (k<<4) | (a<<5) | (c<<2) | (b<<0);
	default:
		return (j_(SngI)<<4)|j_(X); // Unknown
	}
}

/** Get kind name */
const char *jamKindName(uint8_t k);

/** Get implicit type name */
const char* jamTypeName(uint8_t t);

/** Explode object information from id */
jaminfo_t jaminfoHint(jamid_t i, jamsize_t *h);

/** Read from buffer
	@param b Buffer pointer. If null, will return number of bytes that would be read.
	@param o Offset
	@param v Data pointer
	@param l Number of bytes to read
	@return Number of bytes actually read
*/
static inline jamsize_t jamRead(const uint8_t *b, jamsize_t o, void *v, jamsize_t l)
{
	if (b && v) // Only read if buffer is not null
		jam_MEMCPY(v, b + o, l);
	return l;
}

/** Write to buffer.
	@param b Buffer pointer (if null, will return number of bytes that would be written)
	@param o Offset
	@param v Data pointer (if null, write zeros)
	@param l Number of bytes to write
	@return Number of written bytes
*/
static inline jamsize_t jamWrite(uint8_t *b, jamsize_t o, const void *v, jamsize_t l)
{
	if (b) // Write only if buffer is provided
	{
		if (v)
			jam_MEMCPY(b + o, v, l);
		else
			jam_MEMSET(b + o, 0, l);
	}

	return l;
}

/** Reinitialize jam object and value
	@note Object must be valid (initialized), any dynamic resources used by the object will be released
*/
void jamSet(jam_t *j, jamkind_e k, jamfield_t t, jamfield_t s, void *v, jamfield_t n, uint8_t f);

/** Allocate a jam object or array */
jam_t *jamAlloc(jamsize_t n);

/** Deallocate a jam object or array
	Objects are freed before deallocating container
*/
void jamDealloc(jam_t *j, jamsize_t n);

/** Delete a single jam object */
static inline void jamDelete(jam_t *j)
{
	jamDealloc(j, 1);
}

/** Generic object constructor
	With default flags (0), if value pointer provided is null, it will construct a bound object.
	Otherwise, the external value is used.
*/
static inline jam_t *jamNew(jamkind_e k, jamfield_t t, jamfield_t s, void *v, jamfield_t n, uint8_t f)
{
	jam_t *j = jamAlloc(1);
	jamSet(j, k, t, s, v, n, f);
	return j;
}

/** Undefined (empty) object constructor */
static inline void jamUnd(jam_t *j)
{
	jamSet(j, j_(Und), 0, 0, NULL, 0, 0);
}

/** Constructor for bound implicit singleton, eg jamSngI(U8) */
static inline void jamSngI(jam_t *j, jamtype_e t)
{
	jamSet(j, j_(SngI), t, 0, NULL, 0, j_(Bnd));
}

/** Constructor for a generic singleton, eg jamSngE(0xcafe, 8) or jamSngE(0xcafe, 8, &extval).
	If value pointer provided is null, it will construct a bound object. Otherwise, the external value is used.
*/
static inline void jamSngE(jam_t *j, jamfield_t t, jamfield_t s, void *v)
{
	jamSet(j, j_(SngE), t, s, v, 0, v ? 0 : j_(Bnd));
}

/** Constructor for an implicit array, eg jamArrI(j, U16, &extarr, 5, 0)
	Eg:
	- jamArrI(j, U16, NULL, 5, 0): Creates an array object with a bounded value of 5 uint16_t
	- jamArrI(j, U16, &arr, 5, 0): Creates an array object with value pointing to an external array
	- jamArrI(j, U16, &arr, 5, Cst): Creates an array object with value pointing to an external constant array
	- jamArrI(j, U16, &arr, 5, Bnd): Creates an array object with a bounded value of 5 uint16_t copied from an external array
*/
static inline void jamArrI(jam_t *j, jamtype_e t, void *v, jamfield_t n, uint8_t f)
{
	f |= v ? 0 : j_(Bnd);
	jamSet(j, j_(ArrI), t, 0, v, n, f);
}

/** Constructor for an explicit bound array, eg jamArrE(0xcafe, 8, &arr, 32) */
static inline void jamArrE(jam_t *j, jamfield_t t, jamfield_t s, void *v, jamfield_t n, uint8_t f)
{
	f |= v ? 0 : j_(Bnd);
	jamSet(j, j_(ArrE), t, s, v, n, f);
}


/** Constructor for a list object, eg jamLst(16) */
static inline void jamLst(jam_t *j, void *v, jamfield_t n, uint8_t f)
{
	f |= v ? 0 : j_(Bnd);
	jamSet(j, j_(Lst), 0, 0, v, n, f);
}

/** Constructor for a map object, eg jamMap(16) */
static inline void jamMap(jam_t *j, void *v, jamfield_t n, uint8_t f)
{
	f |= v ? 0 : j_(Bnd);
	jamSet(j, j_(Map), 0, 0, v, n, f);
}

/** Constructor for a pure type definition.
	@note A jam pure type definition has an unbound (external) null pointer value
	since resource allocation is deferred.
*/
static inline void jamType(jam_t *j, jamkind_e k, jamfield_t t, jamfield_t s, jamfield_t n)
{
	jamSet(j, k, t, s, NULL, n, 0);
}

/** Check if object is valid */
static inline bool jamIsOk(const jam_t *j)
{
	return j && jamIsKind(j->kind) && ((j->kind == j_(SngI)) ? j->type <= 0xf : true);
}

/** Check if object value is dynamic */
static inline bool jamIsDynamic(const jam_t *j)
{
	jam_ASSERT(j);
	return j->value && (j->flags & j_(Bnd))
		&& ((j->kind != j_(SngI)) || ((j->kind == j_(SngI)) && !jamIsEnclosed((uint8_t)j->type)));
}

/** Check if object is type definition (null value pointer and not bound) */
static inline bool jamIsType(const jam_t *j)
{
	jam_ASSERT(j);
	return (j->value == NULL) && !(j->flags & j_(Bnd));
}

/** Check if object is constant */
static inline bool jamIsConstant(const jam_t *j)
{
	jam_ASSERT(j);
	return (j->flags & j_(Cst)) != 0;
}

/** Get pointer to value */
static inline void *jamPtr(const jam_t *j)
{
	jam_ASSERT(j);
	if (!jamIsOk(j))
		return NULL;
	else
		return (j->kind == j_(SngI) && (j->flags & j_(Bnd)) && jamIsEnclosed((uint8_t)j->type))
			? (void *)&(j->value) : j->value;
}

/** Get pointer to external value in object */
#define jamRef(p,t,f) ((uint8_t*)(p) + offsetof(t, f))

void jamCopy(jam_t *j, const jam_t *q);

void jamCopyStr(jam_t *j, const char *s);

/** Compare objects */
bool jamCompare(const jam_t *p, const jam_t *q);

bool jamCompareStr(const jam_t *p, const char *s);
#ifdef jam_WCHAR
bool jamCompareWstr(const jam_t *p, const wchar_t *s);
#endif

static inline jam_t *jamClone(const jam_t *j)
{
	jam_ASSERT(j);
	return jamNew((jamkind_e)j->kind, j->type, j->size, jamPtr(j), j->items, j->flags);
}

/** Free dynamic resources and clear value */
void jamFree(jam_t *j);

/** Build id for object */
jamid_t jamId(const jam_t *j);

/** Calculate serialized bytes */
jamsize_t jamBytes(const jam_t *j, uint8_t f);

/** Synchronize fields */
void jamUpdate(jam_t *j, uint8_t f);

/** Get pointer to item in container */
void *jamItem(const jam_t *j, jamsize_t n);

/** Get pointer to item in list or map */
jam_t *jamElement(const jam_t *j, jamsize_t n);

/** Get pointer to a map key (even item) */
jam_t *jamKey(const jam_t *j, jamsize_t n);

/** Find entry value using key */
jam_t *jamEntry(const jam_t *j, const jam_t *k);

/** Find entry value using string key */
jam_t *jamEntryStr(const jam_t *j, const char *k);

/** Format (serialize) an object into byte array
	@param b Pointer to serialization buffer
	@param bl Buffer length
	@param f Serialization control flags
	@return Error code if negative, number of serialized bytes otherwise
*/
int jamFormat(const jam_t *j, uint8_t *b, jamsize_t bl, uint8_t f);

/** Format (serialize) a compound object
	@param b Pointer to serialization buffer
	@param bl Buffer length
	@param a Pointer to jam objects array
	@param l Items count
	@param f Serialization control flags
	@return Error code if negative, number of serialized bytes otherwise
*/
int jamFormatArray(const jam_t *a, jamsize_t l, uint8_t *b, jamsize_t bl, uint8_t f);

/** Parse (unserialize) an object from byte array
	@param b Pointer to serialization buffer
	@param bl Buffer items
	@param f Parse flags
	@return Error code if negative, number of deserialized bytes otherwise
*/
int jamParse(jam_t *j, const uint8_t *b, jamsize_t bl, uint8_t f);

// Helper constructors
// SngX
#define j_DEF(N,T) \
static inline void jam##N(jam_t *j, T v) { jamSet(j, j_(SngI), j_(N), 0, &v, 0, j_(Bnd)); } \
static inline void jam##N##Ptr(jam_t *j, T *v) { jamSet(j, j_(SngI), j_(N), 0, v, 0, v ? 0 : j_(Bnd)); } \
static inline void jam##N##PtrC(jam_t *j, const T *v) { jamSet(j, j_(SngI), j_(N), 0, (void *)v, 0, j_(Cst) | (v ? 0 : j_(Bnd))); }

// Numbers
j_DEF(U8, uint8_t)
j_DEF(U16, uint16_t)
j_DEF(U32, uint32_t)
j_DEF(U64, uint64_t)
j_DEF(I8, int8_t)
j_DEF(I16, int16_t)
j_DEF(I32, int32_t)
j_DEF(I64, int64_t)
j_DEF(F32, float)
j_DEF(F64, double)

// Chars
j_DEF(C8, char)
#ifdef jam_WCHAR
j_DEF(C16, wchar_t)
#endif

#undef j_DEF

// Helpers for strings and literals
static inline void jamStr(jam_t *j, char *v)
{
	jamSet(j, j_(ArrI), j_(C8), 0, v, v ? strlen(v) : 0, v ? 0 : j_(Bnd));
}

static inline void jamStrC(jam_t *j, const char *v)
{
	jamSet(j, j_(ArrI), j_(C8), 0, (void *)v, v ? strlen(v) : 0, j_(Cst) | (v ? 0 : j_(Bnd)));
}

#ifdef jam_WCHAR
static inline void jamWstr(jam_t *j, wchar_t *v)
{
	jamSet(j, j_(ArrI), j_(C16), 0, v, v ? wcslen(v) : 0, v ? 0 : j_(Bnd));
}

static inline void jamWstrC(jam_t *j, const wchar_t *v)
{
	jamSet(j, j_(ArrI), j_(C16), 0, (void *)v, v ? wcslen(v) : 0, j_(Cst) | (v ? 0 : j_(Bnd)));
}
#endif

// Boolean
static inline void jamBool(jam_t *j, bool v)
{
	jamSet(j, j_(SngI), v ? j_(T) : j_(F), 0, &v, 0, j_(Bnd));
}

static inline void jamBoolPtr(jam_t *j, bool *v)
{
	jamSet(j, j_(SngI), v ? (*v ? j_(T) : j_(F)) : j_(F), 0, v, 0, v ? 0 : j_(Bnd));
}

static inline void jamBoolPtrC(jam_t *j, const bool *v)
{
	jamSet(j, j_(SngI), v ? (*v ? j_(T) : j_(F)) : j_(F), 0, (void *)v, 0, j_(Cst) | (v ? 0 : j_(Bnd)));
}


// ArrX
#define j_DEF(N,T) \
static inline void jam##N##Arr(jam_t *j, T *v, jamsize_t n) { jamSet(j, j_(ArrI), j_(N), 0, v, n, v ? 0 : j_(Bnd)); } \
static inline void jam##N##ArrC(jam_t *j, const T *v, jamsize_t n) { jamSet(j, j_(ArrI), j_(N), 0, (void *)v, n, j_(Cst) | (v ? 0 : j_(Bnd))); }

// Numbers
j_DEF(U8, uint8_t)
j_DEF(U16, uint16_t)
j_DEF(U32, uint32_t)
j_DEF(U64, uint64_t)
j_DEF(I8, int8_t)
j_DEF(I16, int16_t)
j_DEF(I32, int32_t)
j_DEF(I64, int64_t)
j_DEF(F32, float)
j_DEF(F64, double)

// Chars
j_DEF(C8, char)
#ifdef jam_WCHAR
j_DEF(C16, wchar_t)
#endif

#undef j_DEF

static inline void jamBoolArr(jam_t *j, bool *v, jamsize_t n)
{
	jamSet(j, j_(ArrI), j_(T), 0, v, n, v ? 0 : j_(Bnd));
}

static inline void jamBoolArrC(jam_t *j, const bool *v, jamsize_t n)
{
	jamSet(j, j_(ArrI), j_(T), 0, (void *)v, n, j_(Cst) | (v ? 0 : j_(Bnd)));
}


// Type object definition maps
/** Object member definition */
typedef struct jamdef_t jamdef_t;
struct jamdef_t
{
	const char *name; ///< Member name
	jaminfo_t info; ///< Member type/value
};

/** Registered jam type */
typedef struct jamtype_t jamtype_t;
struct jamtype_t
{
	const char *name; ///< Type name
	jaminfo_t info; ///< Type info
	bool (*proc)(jam_t *obj, void *user, void *ctx); ///< Callback
	void *user; ///< Callback user argument
};

/** Encode (serialize) items in a list inside a SngE object using type template */
int jamEncode(jam_t *j, jamfield_t t, const jamdef_t *d, jamsize_t l, const void *v);

/** Decode (unserialize) an object value from byte array using type template
	@param j Pointer to jam object
	@param d Type definition array
	@param l Type array length
	@param v Pointer to value storage
	@return Error code if negative, otherwise number of deserialized bytes
*/
int jamDecode(const jam_t *j, const jamdef_t *d, jamsize_t l, void *v);

/** Free dynamic resources of value using object definition template */
int jamPurge(const jamdef_t *d, jamsize_t l, void *v);

/** Validate object using type information */
bool jamCheck(const jam_t *j, const jaminfo_t *i);

/** Validate list or map items using type information */
bool jamCheckItems(const jam_t *j, const jaminfo_t *i, jamsize_t l);

/** Validate list or map elements using type information */
bool jamCheckElements(const jam_t *j, const jaminfo_t *i, jamsize_t l);

// jamsize_t jamLink(jamdef_t *d, jamsize_t l, void *s); // Associate storage with type definition member value field




/* Jam stream API */




///** Jam input */
//class ijam_t
//{
//public:
//	using types_t = std::unordered_map<jam_FIELDTYPE, jam_t>; ///< Container for type definitions
//
//public:
//	std::istream *stream;
//	types_t types; ///< Type storage
//
//public:
//	ijam_t(): stream(nullptr) { }
//	ijam_t(std::istream &s): stream(&s) { }
//	~ijam_t() { }
//
//	explicit operator bool() { return stream ? bool(*stream) : false; }
//};

#ifdef __cplusplus
} // extern "C"

#include <unordered_map>
#include <vector>

namespace jam_NAMESPACE {

enum
{
	Ok  = j_(Ok),
	Err  = j_(Err),
	Error  = j_(Error),
	ESystem  = j_(ESystem),
	ERange  = j_(ERange),
	EArgument  = j_(EArgument),
	EOverflow  = j_(EOverflow),
	ETimeout  = j_(ETimeout),
	EFormat  = j_(EFormat),
	EBusy  = j_(EBusy),
	ECancel  = j_(ECancel),
	EFail  = j_(EFail),
	EAbort  = j_(EAbort),
	ERefuse  = j_(ERefuse),
	EReset  = j_(EReset),
	EExist  = j_(EExist),
	EIo  = j_(EIo),
	EData  = j_(EData),
	EMemory  = j_(EMemory),
	EPermission  = j_(EPermission),
	EUnknown  = j_(EUnknown),

	SngI  = j_(SngI),
	SngE  = j_(SngE),
	Lst  = j_(Lst),
	Map  = j_(Map),
	Spc  = j_(Spc),
	Usr  = j_(Usr),
	ArrI  = j_(ArrI),
	ArrE  = j_(ArrE),
	Und  = j_(Und),

	U8  = j_(U8),
	U16  = j_(U16),
	U32  = j_(U32),
	U64  = j_(U64),
	I8  = j_(I8),
	I16  = j_(I16),
	I32  = j_(I32),
	I64  = j_(I64),
	C8  = j_(C8),
	C16  = j_(C16),
	F32  = j_(F32),
	F64  = j_(F64),
	T  = j_(T),
	F  = j_(F),
	X  = j_(X),
	N  = j_(N),
	True  = j_(True),
	False  = j_(False),
	Unknown  = j_(Unknown),
	Null  = j_(Null),

	Bnd	 = j_(Bnd),
	Fix	 = j_(Fix),
	Cst	 = j_(Cst),
	Nts	 = j_(Nts),
	Xfr	 = j_(Xfr),

	Dsc	 = j_(Dsc),
	Inf	 = j_(Inf),
	Val	 = j_(Val),
	Nrc	 = j_(Nrc),
	Upd	 = j_(Upd),
	Tag	 = j_(Tag),
	Skp	 = j_(Skp),
	Hdr	 = j_(Hdr)
};

using	code_t = jamcode_t;
using kind_e = jamkind_e;
using type_e = jamtype_e;
using flag_e = jamflag_e;

using info_t = jaminfo_t;
using def_t = jamdef_t;
using type_t = jamtype_t;

/** jam_t wrapper class */
class jam_c
{
public:
	using id_t = jamid_t;
	using field_t = jamfield_t;
	using size_t = jamsize_t;

protected:
	jam_t *_jam; ///< Jam object pointer to internal jam object or external object
	jam_t _obj; ///< Internal jam object

public:
	/** Calculate span field for value (b8-b64) */
	static inline id_t span(field_t v) { return jamSpan(v); }

	/** Get number of storage bytes for value */
	static inline uint8_t spanBytes(field_t v) { return jamSpanBytes(v); }

	/** Get implicit type size in bytes */
	static inline uint8_t width(uint8_t t) { return jamWidth(t); }

	/** Check if object type is Embedded */
	static inline bool isEmbedded(uint8_t t) { return jamIsEmbedded(t); }

	/** Check if object kind is valid */
	static inline bool isKind(uint8_t k) { return jamIsKind(k); }

	/** Check if object kind is implicit */
	static inline bool isImplicit(uint8_t k) { return jamIsImplicit(k); }

	/** Check if object kind is explicit */
	static inline bool isExplicit(uint8_t k) { return jamIsExplicit(k); }

	/** Check if object kind is singleton */
	static inline bool isSingle(uint8_t k) { return jamIsSingle(k); }

	/** Check if object kind is array */
	static inline bool isArray(uint8_t k) { return jamIsArray(k); }

	/** Check if implicit type can be enclosed inside void pointer */
	static inline bool isEnclosed(uint8_t t) { return jamIsEnclosed(t); }

	/** Create a serialization id.
		@note Arguments have different meanings depending on the object kind.
	*/
	static inline id_t id(uint8_t k, uint8_t a, uint8_t b = 0, uint8_t c = 0) { return _jamId(k, a, b, c); }

	/** Get kind name */
	static inline const char* kindName(uint8_t k) { return jamKindName(k); }

	/** Get implicit type name */
	static inline const char* typeName(uint8_t t) { return jamTypeName(t); }

	/** Explode object information from id */
	static inline info_t infoHint(id_t i, size_t *h = nullptr) { return jaminfoHint(i, h); }

	/** Read from buffer
		@param b Buffer pointer; if null, will return number of bytes that would be read
		@param o Offset
		@param v Data pointer
		@param l Number of bytes to read
		@return Number of bytes actually read
	*/
	static inline size_t read(const uint8_t *b, size_t o, void *v, size_t l) { return jamRead(b, o, v, l); }

	/** Write to buffer
		@param b Buffer pointer (if null, will return number of bytes that would be written)
		@param o Offset
		@param v Data pointer (if null, write zeros)
		@param l Number of bytes to write
		@return Number of written bytes
	*/
	static inline size_t write(uint8_t *b, size_t o, const void *v, size_t l) { return jamWrite(b, o, v, l); }

public:
	/** Default constructor */
	jam_c(): _jam(&_obj), _obj(jamINIT) { }

	/** Reference constructor */
	jam_c(jam_t *j): _jam(j), _obj(jamINIT) { }

	/** Constructor for bound implicit singleton, eg jam_c(U8) */
	jam_c(jamtype_e t)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamSngI(_jam, t);
	}

	/** Constructor for a generic singleton, eg jam_c(0xcafe, 8) or jam_c(0xcafe, 8, &extval).
		If value pointer provided is null, it will construct a bound object.
		Otherwise, the external value is used.
	*/
	jam_c(field_t t, field_t s, void *v = nullptr)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamSngE(_jam, t, s, v);
	}

	/** Constructor for an implicit bound array, eg jam_c(U16, 5) */
	jam_c(type_e t, field_t n, uint8_t f = 0)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamArrI(_jam, t, nullptr, n, f);
	}

	/** Constructor for an implicit array, eg jam_c(U16, &extval, 5) */
	jam_c(type_e t, void *v, field_t n, uint8_t f = 0)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamArrI(_jam, t, v, n, f);
	}

	/** Constructor for an implicit array (const) */
	jam_c(type_e t, const void *v, field_t n, uint8_t f = 0)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamArrI(_jam, t, (void *)v, n, f | Cst);
	}

	/** Constructor for an explicit bound array, eg jam_c(0xcafe, 8, 32) */
	jam_c(field_t t, field_t s, field_t n, uint8_t f = 0)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamArrE(_jam, t, s, nullptr, n, f);
	}

	/** Constructor for an explicit array, eg jam_c(0xcafe, 8, &extval, 32) */
	jam_c(field_t t, field_t s, void *v, field_t n, uint8_t f = 0)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamArrE(_jam, t, s, v, n, f);
	}

	/** Constructor for an explicit array (const) */
	jam_c(field_t t, field_t s, const void *v, field_t n, uint8_t f = 0)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamArrE(_jam, t, s, (void *)v, n, f | Cst);
	}

	/** Constructor for a list or map bound object, eg jam_c(Lst, 16) */
	jam_c(kind_e k, field_t n, uint8_t f = 0)
	: _jam(&_obj), _obj(jamINIT)
	{
		switch (k)
		{
		case Lst: jamLst(_jam, nullptr, n, f);
		case Map: jamMap(_jam, nullptr, n, f);
		default: break;
		}
	}

	/** Constructor for a list or map object, eg jam_c(Lst, &extval, 16) */
	jam_c(kind_e k, void *v, field_t n, uint8_t f = 0xff)
	: _jam(&_obj), _obj(jamINIT)
	{
		switch (k)
		{
		case Lst: jamLst(_jam, v, n, f);
		case Map: jamMap(_jam, v, n, f);
		default: break;
		}
	}

	/** Constructor for a list or map object (const) */
	jam_c(kind_e k, const void *v, field_t n, uint8_t f = 0xff)
	: _jam(&_obj), _obj(jamINIT)
	{
		switch (k)
		{
		case Lst: jamLst(_jam, (void *)v, n, f | Cst);
		case Map: jamMap(_jam, (void *)v, n, f | Cst);
		default: break;
		}
	}

	/** Generic object constructor.
		With default flags, if value pointer provided is null, it will construct a bound object. Otherwise, the external value is used.
	*/
	jam_c(kind_e k, field_t t, field_t s, void *v, field_t n, uint8_t f = 0xff)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamSet(_jam, k, t, s, v, n, (f != 0xff) ? f : (v ? 0 : jBnd));
	}

	/** Generic object constructor (const).
		If value pointer provided is null, it will construct a bound object. Otherwise, the external value is used.
	*/
	jam_c(kind_e k, field_t t, field_t s, const void *v, field_t n, uint8_t f = 0xff)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamSet(_jam, k, t, s, (void *)v, n, (f != 0xff) ? f : Cst | (v ? 0 : jBnd));
	}

	/** Constructor for a pure type definition */
	jam_c(kind_e k, field_t t, field_t s, field_t n)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamType(_jam, k, t, s, n);
	}

	/** jam_c type definition factory.
		@note A jam_c pure type definition has an unbound (external) null pointer value.
	*/
	static inline jam_c def(kind_e k, field_t t, field_t s = 0, field_t n = 0)
	{
		return jam_c(k, t, s, (void *)nullptr, n, 0 /* !Bnd */);
	}

	jam_c(const jam_c &j)
	: _jam(&_obj), _obj(jamINIT)
	{
		jamCopy(_jam, j._jam);
	}

#if __cplusplus >= 201103L
	/** Move constructor */
	jam_c(jam_c &&j)
	: _jam(&_obj), _obj(jamINIT)
	{
		if (j._jam != &j._obj) // Reference
			_jam = j._jam;
		else
		{
			_obj = std::move(j._obj);
			j._obj.kind = Und; // Invalidate
		}
	}
#endif

	/** Free dynamic resources and clear value */
	inline void free()
	{
		jamFree(&_obj);
	}

	~jam_c()
	{
		free();
	}

	inline jam_c& operator=(const jam_c &j)
	{
		if (this != &j)
		{
			free();

			if (j._jam != &j._obj) // Reference
				_jam = j._jam;
			else
			{
				_jam = &_obj; // In case it was reference
				jamCopy(_jam, j._jam);
			}
		}
		return *this;
	}

#if __cplusplus >= 201103L
	inline jam_c& operator=(jam_c &&j)
	{
		free();
		if (j._jam != &j._obj) // Reference
			_jam = j._jam;
		else
		{
			_jam = &_obj; // In case it was reference
			_obj = std::move(j._obj);
			j._obj.kind = Und; // Invalidate
		}
		return *this;
	}
#endif

	/** Check if object is valid */
	inline bool isOk() const
	{
		return jamIsOk(_jam);
	}

	inline operator bool() const { return isOk(); }

	/** Check if object is dynamic */
	inline bool isDynamic() const { return jamIsDynamic(_jam); }

	/** Check if object is type definition (null value pointer and not bound) */
	inline bool isType() const { return jamIsType(_jam); }

	/** Check if object is constant */
	inline bool isConstant() const { return jamIsConstant(_jam); }

	/** Build id for object */
	inline id_t id() const { return jamId(_jam); }

	/** Calculate serialized bytes */
	inline size_t bytes(uint8_t f = Hdr|Val) const { return jamBytes(_jam, f); }

	/** Synchronize fields */
	inline void update(uint8_t f = Hdr|Val) const { jamUpdate(_jam, f); }

	/** Get pointer to item in container.
		@todo Use jam_c pointer wrapper
	*/
	const jam_c item(size_t n) const { return jam_c(jamItem(_jam, n)); }
	jam_c item(size_t n)
	{
		return static_cast<const jam_c &>(*this).item(n);
	}

	/** Get pointer to item in list or map */
	const jam_c element(size_t n) const { return jam_c(jamElement(_jam, n)); }
	jam_c element(size_t n)
	{
		return static_cast<const jam_c &>(*this).element(n);
	}

	/** Get pointer to a map key (even item) */
	const jam_c key(size_t n) const { return jam_c(jamKey(_jam, n)); }
	jam_c key(size_t n)
	{
		return static_cast<const jam_c &>(*this).key(n);
	}

	/** Get pointer to a map entry (odd item) */
	const jam_c entry(const jam_c &j) const { return jam_c(jamEntry(_jam, j._jam)); }
	jam_c entry(size_t n)
	{
		return static_cast<const jam_c &>(*this).entry(n);
	}

	/** Get pointer to value */
	inline void *ptr()
	{
		return isConstant() ? nullptr : jamPtr(_jam);
	}

	/** Get pointer to const value */
	inline const void *ptr() const { return jamPtr(_jam); }

	/** Convert jam_c to underlying jam_t */
	operator jam_t *() { return _jam; }
	operator const jam_t *() const { return _jam; }

	jam_t *jam() { return _jam; }
	const jam_t *jam() const { return _jam; }

	// Field getters
	kind_e kind() const { return (kind_e)_jam->kind; }
	field_t type() const { return _jam->type; }
	field_t size() const { return _jam->size; }
	void *value() const { return _jam->value; }
	field_t items() const { return _jam->items; }
	uint8_t flags() const { return _jam->flags; }

	/** Format (serialize) an object into byte array.
		@param b Pointer to serialization buffer.
		@param bl Buffer length.
		@param f Serialization control flags.

		@return Error code if negative, number of serialized bytes otherwise.
	*/
	inline int format(uint8_t *b, jamsize_t l, uint8_t f = Hdr|Val) const
	{
		return jamFormat(_jam, b, l, f);
	}

	inline std::vector<uint8_t> format(uint8_t f = Hdr|Val) const
	{
		std::vector<uint8_t> v(bytes(f));
		format(v.data(), v.size(), f|Skp);
		return v;
	}

	/** Format (serialize) a compound object.
		@param bl Buffer length.
		@param b Pointer to serialization buffer.
		@param l Items count.
		@param f Serialization control flags.
		@param a Pointer to jam_c objects array.

		@return Error code if negative, number of serialized bytes otherwise.
	*/
//	static int format(
//		const jam_c *a, jamsize_t n, uint8_t *b, size_t l, uint8_t f = Dsc|Val
//	)
//	{
//		jam_c j[n];
//		for (size_t i = 0; i < n; ++i)
//			j[i] = a[i]._jam;
//		return jamFormatArray(j, n, b, l, f);
//	}

	/** Parse (unserialize) an object from byte array
		@param bl Buffer items
		@param b Pointer to serialization buffer
		@param f Parse flags
		@return Error code if negative, number of deserialized bytes otherwise
	*/
	inline int parse(const uint8_t *b, size_t l, uint8_t f = Hdr|Val)
	{
		return jamParse(_jam, b, l, f);
	}

	inline int parse(const std::vector<uint8_t> &v, uint8_t f = Hdr|Val)
	{
		return parse(v.data(), v.size(), f);
	}

	/** Parse (unserialize) an object from byte array using type template.
	@param b Pointer to serialization buffer.
	@param bl Buffer items.
	@param[in,out] kl Type array items.
	@param k Type array.
	@param f Parse flags.
	@return Error code if negative, otherwise number of deserialized bytes.
	*/
	int parse(uint8_t *b, size_t bl, size_t &kl, const jam_c *k, uint8_t f = 0);

	// Helper constructors
	// SngX

#define j_DEF(N,T) \
jam_c(T v): _jam(&_obj), _obj(jamINIT) { jamSet(_jam, (kind_e)SngI, N, 0, &v, 0, Bnd); } \
jam_c(T *v): _jam(&_obj), _obj(jamINIT) { jamSet(_jam, (kind_e)SngI, N, 0, v, 0, v ? 0 : Bnd); } \
jam_c(const T *v): _jam(&_obj), _obj(jamINIT) { jamSet(_jam, (kind_e)SngI, N, 0, (void *)v, 0, Cst | (v ? 0 : Bnd)); }

j_DEF(U8, uint8_t)
j_DEF(U16, uint16_t)
j_DEF(U32, uint32_t)
j_DEF(U64, uint64_t)
j_DEF(I8, int8_t)
j_DEF(I16, int16_t)
j_DEF(I32, int32_t)
j_DEF(I64, int64_t)
j_DEF(F32, float)
j_DEF(F64, double)
#undef j_DEF

	/** For convenience using C strings, this will create a char array instead of a pointer to a char.
		To create a pointer to a single char, use the full argument constructor instead.
	*/
	jam_c(char v): _jam(&_obj), _obj(jamINIT) { jamSet(_jam, (kind_e)SngI, C8, 0, &v, 0, Bnd); }
	jam_c(char *v): _jam(&_obj), _obj(jamINIT) { jamStr(_jam, v); }
	jam_c(const char *v): _jam(&_obj), _obj(jamINIT) { jamStrC(_jam, v); }
	/** std::string wrapper */
	jam_c(const std::string &s): _jam(&_obj), _obj(jamINIT) { jamStrC(_jam, s.c_str()); }

#ifdef jam_WCHAR
	jam_c(wchar_t v): _jam(&_obj), _obj(jamINIT) { jamSet(_jam, (kind_e)SngI, C16, 0, &v, 0, Bnd); }
	jam_c(wchar_t *v): _jam(&_obj), _obj(jamINIT) { jamWstr(_jam, v); }
	jam_c(const wchar_t *v): _jam(&_obj), _obj(jamINIT) { jamWstrC(_jam, v); }
	/* std::string wrapper */
	// jam_c(const std::wstring &s) { jamWstrC(_jam, s.c_str()); }
#endif

	jam_c(bool v): _jam(&_obj), _obj(jamINIT) { jamBool(_jam, v); }
	jam_c(bool *v): _jam(&_obj), _obj(jamINIT) { jamBoolPtr(_jam, v); }
	jam_c(const bool *v): _jam(&_obj), _obj(jamINIT) { jamBoolPtrC(_jam, v); }

	// ArrX
#define j_DEF(N,T) \
jam_c(T *v, size_t n): _jam(&_obj), _obj(jamINIT) { jamSet(_jam, (kind_e)ArrI, N, 0, v, n, v ? 0 : Bnd); } \
jam_c(const T *v, size_t n): _jam(&_obj), _obj(jamINIT) { jamSet(_jam, (kind_e)ArrI, N, 0, (void *)v, n, Cst | (v ? 0 : Bnd)); }

// Numbers
j_DEF(U8, uint8_t)
j_DEF(U16, uint16_t)
j_DEF(U32, uint32_t)
j_DEF(U64, uint64_t)
j_DEF(I8, int8_t)
j_DEF(I16, int16_t)
j_DEF(I32, int32_t)
j_DEF(I64, int64_t)
j_DEF(F32, float)
j_DEF(F64, double)

// Chars
j_DEF(C8, char)
#ifdef jam_WCHAR
j_DEF(C16, wchar_t)
#endif

#undef j_DEF

	jam_c(bool *v, size_t n): _jam(&_obj), _obj(jamINIT) { jamBoolArr(_jam, v, n); }
	jam_c(const bool *v, size_t n): _jam(&_obj), _obj(jamINIT) { jamBoolArrC(_jam, v, n); }

	/** Jam string factory */
	static inline jam_c str(char *s) { return jam_c(s, strlen(s)); }

	/** Jam string factory (const) */
	static inline jam_c str(const char *s) { return jam_c(s, strlen(s)); }

#ifdef jam_WCHAR
	/** Jam wide string factory */
	static inline jam_c wstr(wchar_t *s) { return jam_c(s, wcslen(s)); }

	/** Jam wide string factory (const) */
	static inline jam_c wstr(const wchar_t *s) { return jam_c(s, wcslen(s)); }
#endif

	inline int encode(field_t t, const def_t *d, size_t l, const void *v = nullptr) { return jamEncode(_jam, t, d, l, v); }
	inline int decode(const def_t *d, size_t l, void *v = nullptr) const { return jamDecode(_jam, d, l, v); 	}

	jam_c(field_t t, const def_t *d, size_t l, const void *v = nullptr): _jam(&_obj), _obj(jamINIT) { jamEncode(_jam, t, d, l, v); }

	static inline int purge(const def_t *d, size_t l, void *v) { return jamPurge(d, l, v); }

	inline bool check(const info_t &i) const { return jamCheck(_jam, &i); }
	inline bool checkItems(const info_t *i, size_t l) const { return jamCheckItems(_jam, i, l); }
	inline bool checkElements(const info_t *i, size_t l) const { return jamCheckElements(_jam, i, l); }

private:
		/** Build a jam list value. Assumes T is an iterable type */
	template <typename T>
	static inline void *buildList(const T &l)
	{
		size_t n = l.size();
		jam_t *j = jamAlloc(n);

		if (j)
		{
			jam_t *p = j;

			for (auto &i: l)
				jam_c(p++) = i;
		}

		return static_cast<void *>(j);
	}

	/** Build a map value. Assumes T is a map type */
	template <typename T>
	static inline void *buildMap(const T &m)
	{
		size_t n = m.size() * 2;
		jam_t *j = jamAlloc(n);

		if (j)
		{
			jam_t *p = j;

			for (auto &i: m)
			{
				jam_c(p++) = i.first;
				jam_c(p++) = i.second;
			}
		}

		return static_cast<void *>(j);
	}

public:
//	template <typename T>
//	jam_c(const T &t)
//	: kind(Lst), type(0), size(0), items(t.size()), flags(Bnd),
//		value(buildList(t))
//	{
//	}
//
//	template <typename K, typename T, typename H, typename P, typename A>
//	jam_c(const std::unordered_map<K, T, H, P, A> &m)
//	: kind(Map), type(0), size(0), items(m.size()), flags(Bnd),
//		value(buildMap(m))
//	{
//	}
//
//	template <typename K, typename T, typename C, typename A>
//	jam_c(const std::map<K, T, C, A> &m)
//	: kind(Map), type(0), size(0), items(m.size()), flags(Bnd),
//		value(buildMap(m))
//	{
//	}

};

} // namespace jam_NAMESPACE

#endif // __cplusplus

#undef j_
#undef j__
#undef j___

#endif // JAM_H_
