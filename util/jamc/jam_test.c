#include "jam.h"
#include "jamreg.h"

#include <stdio.h>

#ifdef jam_DEBUG
extern int _objects, _allocs;
#endif

static void
_printStats()
{
#ifdef jam_DEBUG
	printf("Objects = %d\n", _objects);
	printf("Allocs = %d\n", _allocs);
#endif
	fflush(stdout);
}

static void
_printHex(const uint8_t *b, unsigned l)
{
	for (unsigned i = 0; i < l; ++i)
	{
		printf("%02x", b[i]);
		if ((i+1) % 16 == 0)
			printf("\n");
		else
			printf(" ");
	}
	printf("\n");
	fflush(stdout);
}

static void
_printHexStr(const char *s)
{
	_printHex((const uint8_t *)s, strlen(s));
}

static void
_printValue(const jam_t *k)
{
	jamsize_t sz = (jamIsImplicit(k->kind)) ? jamWidth((uint8_t)k->type)
		: k->size;

	const void *o = jamPtr(k);

	if (!o)
	{
		printf("<nul>");
		fflush(stdout);
		return;
	}

	unsigned i = 0;
	jamsize_t ln = (jamIsSingle(k->kind)) ? 1 : k->items;

	while (true)
	{
		switch (k->type)
		{
		case jU8: printf("%u", *(const uint8_t *)o); break;
		case jU16: printf("%u", *(const uint16_t *)o); break;
		case jU32: printf("%u", *(const uint32_t *)o); break;
		// case U64: printf("%" PRIu64, *(const uint64_t *)o); break;

		case jI8: printf("%d", *(const int8_t *)o); break;
		case jI16: printf("%d", *(const int16_t *)o); break;
		case jI32: printf("%d", *(const int32_t *)o); break;
		// case I64: printf("%lld", *(const int64_t *)o); break;

		case jC8: printf("%c", *(const char *)o); break;
		// case C16: printf("%s", *(const wchar_t*)o); break;

		case jF32: printf("%g", *(const float *)o); break;
		case jF64: printf("%g", *(const double *)o); break;

		case jT: printf("True"); break;
		case jF: printf("False"); break;
		case jX: printf("Unknown"); break;
		case jN: printf("Null"); break;

		default: printf("<?>");
		}

		++i;
		if (i == ln)
			break;

		if (k->type != jC8)
			printf(" ");

		o = (uint8_t *)o + sz;
	}

	fflush(stdout);
}

static void
_printJaminfo(const jaminfo_t *i, bool lf)
{
	printf("%s: type=%x, size=%d, items=%d", jamTypeName(i->kind), (unsigned)i->type, (unsigned)i->size, (unsigned)i->items);

	if (lf)
		printf("\n");
	fflush(stdout);
}

static void
_printJam(const jam_t *k, bool lf)
{
	jamid_t i = jamId(k);
	printf("id: 0x%x (%s", i, jamKindName(k->kind));

	jaminfo_t f = jaminfoHint(i, NULL);
	int n = k->items;

	switch(k->kind)
	{
	case jSngI:
		printf(",%s,%dB), value: ", jamTypeName((uint8_t)k->type), (unsigned)f.size);
		_printValue(k);
		break;

	case jSngE:
		printf(",%dB,%dB)", (unsigned)f.type, (unsigned)f.size);
		printf(", type: 0x%x, size: %d", (unsigned)k->type, (unsigned)k->size);
		break;

	case jMap:
		n *= 2;
	case jLst:
		printf(",items:%d(%dB)), value:\n", n, (unsigned)f.items);
		for (jamsize_t i = 0; i < n; ++i)
		{
			printf("  ");
			_printJam(jamItem(k, i), true);
		}
		lf = false;
		break;

	case jSpc:
	case jUsr:

	case jArrI:
	case jArrE:
		printf(",%s,items:%d(%dB)), value: ", jamTypeName(k->type), n, (unsigned)f.items); // Fix for explicit
		_printValue(k);
		break;

	default:
		printf(")");
		break;
	}

	if (lf)
		printf("\n");
	fflush(stdout);
}

static void
test_bound()
{
	jam_t j = jamINIT;
	// jamVar(j);

	jamU8(&j, 0);
	_printJam(&j, true);

	jamU16(&j, 5);
	_printJam(&j, true);

	jamLst(&j, NULL, 6, 0);
	_printJam(&j, true);

	uint8_t buf[256];
	int r = 0;

	r = jamFormat(&j, buf+r, 256-r, 0);
	_printHex(buf, r);

	jamFree(&j);
}

//static void
//test_embedded()
//{
//	jam_t j, t(T), f(F), x(X), n(N);
//	_printJam(j);
//	_printJam(t);
//	_printJam(f);
//	_printJam(x);
//	_printJam(n);
//
//	jam_t a = std::list<jam_t>{uint8_t(0xab), T, F, X, N};
//	_printJam(a);
//
//	uint8_t buf[256];
//	int r = 0;
//
//	r = a.format(buf+r, 256-r);
//	_printHex(buf, r);
//
//	r = 0;
//	r = a.format(buf+r, 256-r, Val);
//	_printHex(buf, r);
//}

static void
test_string()
{
	jam_t j = jamINIT;
	jamStr(&j, "This is string 1");
	_printJam(&j, true);

	jamStr(&j, "This is string 2");
	_printJam(&j, true);

	jamFree(&j);
}

static void
test_basic()
{
	uint8_t u8 = 12;
	uint16_t u16 = 3456;
	uint32_t u32 = 0xdeadb00b;

	char msg[] = "This is the message";
	uint16_t arr[] = { 0x1234, 0x5678, 0x1001, 0x1616 };

	printf("Formatting\n");

	uint8_t buf[256];
	int r = 0;
	jam_t k = jamINIT;


	jamU8Ptr(&k, &u8);
	_printJam(&k, true);
	r += jamFormat(&k, buf+r, 256-r, 0);
	_printHex(buf, r);

	jamU16Ptr(&k, &u16);
	_printJam(&k, true);
	r += jamFormat(&k, buf+r, 256-r, 0);
	_printHex(buf, r);

	jamU32Ptr(&k, &u32);
	_printJam(&k, true);
	r += jamFormat(&k, buf+r, 256-r, 0);
	_printHex(buf, r);

	jamStr(&k, msg);
	_printJam(&k, true);
	r += jamFormat(&k, buf+r, 256-r, 0);
	_printHex(buf, r);

	jamU16Arr(&k, arr, 4);
	_printJam(&k, true);
	r += jamFormat(&k, buf+r, 256-r, 0);
	_printHex(buf, r);

	r = 0;
	jam_t lt[2] = {{0}};
	jamSetup(lt, 2);
	jamType(lt+0, jArrI, jU16, 0, 4);
	jamType(lt+1, jSngI, jU8, 0, 0);

	jamLst(&k, &lt, 2, 0);
	_printJam(&k, true);
	r += jamFormat(&k, buf+r, 256-r, 0);
	_printHex(buf, r);

	jamFree(&k);
}

#include <packed.h>

packed(
struct TestStruct1
{
	uint8_t byt;
	char str[16];
	float flt[4];
});

static void
_printTS1(const jam_t *j, void *a, void *ctx)
{
	struct TestStruct1 *s = (struct TestStruct1 *)a;
	if (sizeof(*s) != j->size)
	{
		printf("Size error\n");
		goto end;
	}

	*s = *(struct TestStruct1 *)jamPtr(j);

	printf("byt=%x, str=%s, flt[]=", s->byt, s->str);
	for (int i = 0; i < 4; ++i)
		printf("%g ", s->flt[i]);
	printf("\n");

end:
	fflush(stdout);
}

// Test plain struct serialization
static void
test_struct1()
{
	uint8_t buf[256];
	int r = 0;

	struct TestStruct1 ts1 = {
		0x55,
		"The string 1",
		{0, 2.718, 3.14159f, -1}
	};

	jam_t j = jamINIT;

	jamSngE(&j, 0x666 /*type*/, sizeof(ts1), &ts1);
	_printJam(&j, true);
	r += jamFormat(&j, buf+r, sizeof(buf)-r, 0);
	_printHex(buf, r);
	jamFree(&j);

	jamr_t *jr = jamrNew();

	jaminfo_t ji = {jSngE, 0x666, sizeof(struct TestStruct1)};
	jamtype_t *tp = jamrInsert(jr, "TestStruct1", &ji);
	tp->proc = _printTS1; // Callback
	tp->user = &ts1; // User argument for callback

	printf("%s: ", tp->name);
	_printJaminfo(&tp->info, true);

	r = jamParse(&j, buf, r, 0);
	if (r < 0)
		printf("Error parsing object.\n");
	else
		_printJam(&j, true);

	jamrProcess(jr, &j, NULL);

	jamFree(&j);
	jamrDelete(jr);
}

packed(
struct TestStruct2
{
	uint8_t byt;
	char *str;
	jam_t flt; // float *flt;
});

static jamdef_t TS2[] =
{
	{"byt", {jSngI, jU8 }},
	{"str", {jArrI, jC8 }}, // Non-fixed size, so will point to null-terminated string
	{"flt", {jArrI, jF32, /*.items = 0*/ }} // A jam object because we need to know array length
};

static void
_printTS2(const jam_t *j, void *a, void *ctx)
{
	struct TestStruct2 *ts2 = (struct TestStruct2 *)a;
	struct TestStruct2 t = {.flt = jamINIT };

	int r = jamDecode(j, TS2, sizeof(TS2)/sizeof(jamdef_t), &t);
	if (r < 0)
	{
		printf("Error decoding TS2 data.\n");
		return;
	}

	printf("%s = %x, %s = %s, %s =", TS2[0].name, t.byt, TS2[1].name, t.str, TS2[2].name);

	for (jamsize_t i = 0; i < t.flt.items; ++i)
		printf(" %g", *(float *)jamItem(&t.flt, i));

	printf("\n");

	jamPurge(TS2, sizeof(TS2)/sizeof(jamdef_t), &t);
	// jamFree(&t.flt);
}

// Test struct with objects
static void
test_struct2()
{
	uint8_t buf[256];
	int r = 0;
	float fa[] = {0, 2.718, 3.14159f, -1};

	struct TestStruct2 ts2 = {
		0x55,
		"The string 2",
		jamINIT
	};

	// Point ts2.flt to fa array
	jamF32Arr(&ts2.flt, fa, 4);
	// jamArrI(&ts2.flt, jF32, fa, 4, jBnd);

	jam_t j = jamINIT;

// Type definition for TestStruct2
//	jamdef_t tsd[] =
//	{
//		{"byt", {jSngI, jU8, .value = &ts2.byt }}, // External storage
//		{"str", {jArrI, jC8, .items = 16, .value = ts2.str }}, // External storage fixed-size array
//		{"flt", {jArrI, jF32, .items = 0, .value = &ts2.flt }} // Internal storage dynamic-size array (ts.flt will point to internal data)
//	};


	// jamLink(tsd, 3, &ts2); // Associate ts2 structure with definitions

	// Encode structure inside the value bytes of a SngE object
	jamEncode(&j, 0x667, TS2, sizeof(TS2)/sizeof(jamdef_t), &ts2);
	_printJam(&j, true);
	r += jamFormat(&j, buf+r, sizeof(buf)-r, 0);
	_printHex(buf, r);
	jamFree(&j);

	jamr_t *jr = jamrNew();
	jaminfo_t ji = {jSngE, 0x667}; // Size=0, so variable size
	jamtype_t *tp = jamrInsert(jr, "TestStruct2", &ji);
	tp->proc = _printTS2; // Callback
	tp->user = &ts2;

	printf("%s\n", tp->name);
	_printJaminfo(&tp->info, true);

	r = jamParse(&j, buf, r, 0);
	if (r < 0)
		printf("Error parsing object.\n");
	else
		_printJam(&j, true);

	jamrProcess(jr, &j, NULL);

	jamFree(&j);
	jamrDelete(jr);
}

static void
test_compound()
{
	uint8_t buf[256];
	int r = 0;

	jam_t j = jamINIT;
	jamLst(&j, NULL, 5, 0);
	_printJam(&j, true);

	r = jamFormat(&j, buf, 256, 0);
	_printHex(buf, r);

	jamFree(&j);
}

static void
test_compare()
{
	jam_t a = jamINIT, b = jamINIT, c = jamINIT, j = jamINIT;

	jamStr(&a, "One");
	jamCopyStr(&b, "One");
	jamStr(&c, "OneNot");

	printf("a==j: %s\n", jamCompare(&a, &j) ? "True" : "False");
	jamU8(&j, 5);
	printf("a==j: %s\n", jamCompare(&a, &j) ? "True" : "False");
	printf("a==b: %s\n", jamCompare(&a, &b) ? "True" : "False");
	printf("a==c: %s\n", jamCompare(&a, &c) ? "True" : "False");
	printf("b==c: %s\n", jamCompare(&b, &c) ? "True" : "False");

	printf("a=='One': %s\n", jamCompareStr(&a, "One") ? "True" : "False");
	printf("a=='Two': %s\n", jamCompareStr(&a, "Two") ? "True" : "False");
	printf("b=='One': %s\n", jamCompareStr(&b, "One") ? "True" : "False");
	printf("b=='Two': %s\n", jamCompareStr(&b, "Two") ? "True" : "False");
	printf("c=='One': %s\n", jamCompareStr(&c, "One") ? "True" : "False");
	printf("c=='Two': %s\n", jamCompareStr(&c, "Two") ? "True" : "False");
	printf("c=='OneNot': %s\n", jamCompareStr(&c, "OneNot") ? "True" : "False");

	jamFree(&a);
	jamFree(&b);
	jamFree(&c);
	jamFree(&j);
}

static void
test_map()
{
	jam_t m = jamINIT;

	jam_t *v = jamAlloc(2*3);

	jamCopyStr(v+0, "One");
	jamU8(v+1, 1);
	jamCopyStr(v+2, "Two");
	jamF32(v+3, 3.14159);
	jamCopyStr(v+4, "Three");
	jamCopyStr(v+5, "This is a string");

	jamMap(&m, v, 3, jXfr);

	_printJam(&m, true);

	jamFree(&m);
}


//static void
//test_jams(uint8_t f = Hdr|Val)
//{
//	// char buf[256];
//	std::stringstream ss;
//	ojam_t oj(ss, f);
//
//	uint8_t u8 = 12;
//	uint16_t u16 = 3456;
//	uint32_t u32 = 0xdeadb00b;
//
//	oj << u8;
//	oj << u16;
//	oj << u32;
//	oj << "A message";
//	oj << 12345;
//
//	std::list<uint8_t> l = { 0, 1, 2, 3, 4, 5 };
//	oj << l;
//
//	_printHex(ss.str());
//}

#define TEST(x) printf("\n--\n" #x "\n"); test_##x(); _printStats()

int
main(int argc, char **argv)
{
	_printStats();

	TEST(basic);
	TEST(bound);
	TEST(string);
	TEST(map);
	TEST(compare);
	TEST(struct1);
	TEST(struct2);
	// test_embedded();
	TEST(compound);
	// test_ojam();
	// test_ojam(Val);



/*
	jam_t::def_info vi[] =
	{
		{ "byt", &vt[0] },
		{ "str", &vt[1] },
		{ "flt", &vt[2] },
	};
*/
/*
	printf("Parsing\n");
	m = 0, l = 0;
	k.value = mem;

	while (m < r)
	{
		l = k.parse(256-m, buf+m, Pfx);
		if (l)
		{
			_printJam(&k, true);
			m += l;
		}
	}
*/

	return 0;
}


