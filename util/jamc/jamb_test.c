#include "jambus.h"

#include <stdio.h>

#ifdef jam_DEBUG
extern int _objects, _allocs;
#endif

static void
_printStats()
{
#ifdef jam_DEBUG
	printf("Objects = %d\n", _objects);
	printf("Allocs = %d\n", _allocs);
#endif
	fflush(stdout);
}

static void
_printHex(const uint8_t *b, unsigned l)
{
	for (unsigned i = 0; i < l; ++i)
	{
		printf("%02x", b[i]);
		if ((i+1) % 16 == 0)
			printf("\n");
		else
			printf(" ");
	}
	printf("\n");
	fflush(stdout);
}

static void
_printHexStr(const char *s)
{
	_printHex((const uint8_t *)s, strlen(s));
}


static void
_printJamb(const jamb_t *j, bool lf)
{
	printf("%02x->%02x: [%d]", j->source, j->target, (unsigned)j->size);
	for (int i = 0; i < j->size; ++i)
		printf(" %02x", jambData(j)[i]);
	printf(" (%08x)", j->crc);
	printf(" s=%d i=%d k=%02x c=%02x b=%c", j->space, j->index, j->block, j->count, j->bound ? 'T' : 'F');

	if (lf)
		printf("\n");
	fflush(stdout);
}

static void
test_basic()
{
	uint8_t d[] = {0xa, 0xb, 0xc, 0xd, 0xe};

	jamb_t j = jambINIT;
	_printJamb(&j, true);

	jambSet(&j, 1, 2, NULL, 5);
	_printJamb(&j, true);

	jambSet(&j, 1, 2, d, 5);
	_printJamb(&j, true);

	jambSet(&j, 1, 2, NULL, 20);
	_printJamb(&j, true);

	jambSet(&j, 1, 2, NULL, 30);
	_printJamb(&j, true);

	jambSet(&j, 1, 2, NULL, 15);
	_printJamb(&j, true);

	jambSet(&j, 1, 2, d, 5);
	_printJamb(&j, true);

	jambUpdate(&j, d, 1);
	_printJamb(&j, true);

	jambUpdate(&j, d, 5);
	_printJamb(&j, true);

	jambFree(&j);

	jambSet(&j, 1, 2, d, 5);
	_printJamb(&j, true);

	jambFree(&j);
}

static void
test_adjust()
{
	jamb_t j = jambINIT;

	jambAdjust(&j, 0, false);
	_printJamb(&j, true);

	jambAdjust(&j, 1, false);
	_printJamb(&j, true);

	jambAdjust(&j, jamb_BUFFERSPACE, false);
	_printJamb(&j, true);

	for (int i = 0; i < j.space; ++i)
		jambData(&j)[i] = i;
	// j.size = j.space;
	jambUpdate(&j, NULL, j.space);

	jambAdjust(&j, jamb_BUFFERSPACE * 2, true);
	_printJamb(&j, true);

	jambFree(&j);
}

static void
test_update()
{
	jamb_t j = jambINIT;
	jambUpdate(&j, NULL, 0);
	_printJamb(&j, true);

	jambUpdate(&j, NULL, 5);
	_printJamb(&j, true);

	uint8_t d[] = {0xa, 0xb, 0xc, 0xd, 0xe};

	jambUpdate(&j, d, 0);
	_printJamb(&j, true);

	jambUpdate(&j, d, 5);
	_printJamb(&j, true);

	jambFree(&j);
}

static void
test_copy()
{
	jamb_t p = jambINIT, q = jambINIT;
	uint8_t d[] = {0xa, 0xb, 0xc, 0xd, 0xe};

	jambSet(&p, 1, 2, d, 5);
	_printJamb(&p, true);

	bool r = jambCopy(&q, &p);
	_printJamb(&q, true);

	jambFree(&q);
	jambSet(&q, 3, 4, NULL, 10);
	jambUpdate(&q, NULL, 10);
	_printJamb(&q, true);

	r = jambCopy(&p, &q);
	_printJamb(&p, true);

	uint8_t e[16];
	jambSet(&p, 0, 0, e, 16);

	r = jambCopy(&p, &q);
	_printJamb(&p, true);

	jambFree(&p);
	jambFree(&q);
}

static void
test_jam()
{
	jamb_t j = jambINIT;
	jam_t k = jamINIT;

	jambSetJam(&j, 5, 6, &k);
	_printJamb(&j, true);

	jamU8(&k, 0xca);
	jambSetJam(&j, 5, 6, &k);
	_printJamb(&j, true);

	jamStr(&k, "This is the message. This is the message. This is the message.");
	jambSetJam(&j, 5, 6, &k);
	_printJamb(&j, true);

	jamFree(&k);
	jambFree(&j);
}

#define TEST(x) printf("\n--\n" #x "\n"); test_##x(); _printStats()

int
main(int argc, char **argv)
{
	_printStats();

	TEST(basic);
	TEST(adjust);
	TEST(update);
	TEST(copy);
	TEST(jam);

	return 0;
}


