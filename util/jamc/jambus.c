#include "jambus.h"
#include "jammem.h"
#include "jamprv.h"

#ifdef jam_DEBUG
extern int _jamObjects; ///< Track number of objects
extern int _jamAllocs; ///< Track memory allocations
#endif

/* CRC32C table */
static const uint32_t _crc32c_tbl[] =
{
	0x00000000L, 0xF26B8303L, 0xE13B70F7L, 0x1350F3F4L,
	0xC79A971FL, 0x35F1141CL, 0x26A1E7E8L, 0xD4CA64EBL,
	0x8AD958CFL, 0x78B2DBCCL, 0x6BE22838L, 0x9989AB3BL,
	0x4D43CFD0L, 0xBF284CD3L, 0xAC78BF27L, 0x5E133C24L,
	0x105EC76FL, 0xE235446CL, 0xF165B798L, 0x030E349BL,
	0xD7C45070L, 0x25AFD373L, 0x36FF2087L, 0xC494A384L,
	0x9A879FA0L, 0x68EC1CA3L, 0x7BBCEF57L, 0x89D76C54L,
	0x5D1D08BFL, 0xAF768BBCL, 0xBC267848L, 0x4E4DFB4BL,
	0x20BD8EDEL, 0xD2D60DDDL, 0xC186FE29L, 0x33ED7D2AL,
	0xE72719C1L, 0x154C9AC2L, 0x061C6936L, 0xF477EA35L,
	0xAA64D611L, 0x580F5512L, 0x4B5FA6E6L, 0xB93425E5L,
	0x6DFE410EL, 0x9F95C20DL, 0x8CC531F9L, 0x7EAEB2FAL,
	0x30E349B1L, 0xC288CAB2L, 0xD1D83946L, 0x23B3BA45L,
	0xF779DEAEL, 0x05125DADL, 0x1642AE59L, 0xE4292D5AL,
	0xBA3A117EL, 0x4851927DL, 0x5B016189L, 0xA96AE28AL,
	0x7DA08661L, 0x8FCB0562L, 0x9C9BF696L, 0x6EF07595L,
	0x417B1DBCL, 0xB3109EBFL, 0xA0406D4BL, 0x522BEE48L,
	0x86E18AA3L, 0x748A09A0L, 0x67DAFA54L, 0x95B17957L,
	0xCBA24573L, 0x39C9C670L, 0x2A993584L, 0xD8F2B687L,
	0x0C38D26CL, 0xFE53516FL, 0xED03A29BL, 0x1F682198L,
	0x5125DAD3L, 0xA34E59D0L, 0xB01EAA24L, 0x42752927L,
	0x96BF4DCCL, 0x64D4CECFL, 0x77843D3BL, 0x85EFBE38L,
	0xDBFC821CL, 0x2997011FL, 0x3AC7F2EBL, 0xC8AC71E8L,
	0x1C661503L, 0xEE0D9600L, 0xFD5D65F4L, 0x0F36E6F7L,
	0x61C69362L, 0x93AD1061L, 0x80FDE395L, 0x72966096L,
	0xA65C047DL, 0x5437877EL, 0x4767748AL, 0xB50CF789L,
	0xEB1FCBADL, 0x197448AEL, 0x0A24BB5AL, 0xF84F3859L,
	0x2C855CB2L, 0xDEEEDFB1L, 0xCDBE2C45L, 0x3FD5AF46L,
	0x7198540DL, 0x83F3D70EL, 0x90A324FAL, 0x62C8A7F9L,
	0xB602C312L, 0x44694011L, 0x5739B3E5L, 0xA55230E6L,
	0xFB410CC2L, 0x092A8FC1L, 0x1A7A7C35L, 0xE811FF36L,
	0x3CDB9BDDL, 0xCEB018DEL, 0xDDE0EB2AL, 0x2F8B6829L,
	0x82F63B78L, 0x709DB87BL, 0x63CD4B8FL, 0x91A6C88CL,
	0x456CAC67L, 0xB7072F64L, 0xA457DC90L, 0x563C5F93L,
	0x082F63B7L, 0xFA44E0B4L, 0xE9141340L, 0x1B7F9043L,
	0xCFB5F4A8L, 0x3DDE77ABL, 0x2E8E845FL, 0xDCE5075CL,
	0x92A8FC17L, 0x60C37F14L, 0x73938CE0L, 0x81F80FE3L,
	0x55326B08L, 0xA759E80BL, 0xB4091BFFL, 0x466298FCL,
	0x1871A4D8L, 0xEA1A27DBL, 0xF94AD42FL, 0x0B21572CL,
	0xDFEB33C7L, 0x2D80B0C4L, 0x3ED04330L, 0xCCBBC033L,
	0xA24BB5A6L, 0x502036A5L, 0x4370C551L, 0xB11B4652L,
	0x65D122B9L, 0x97BAA1BAL, 0x84EA524EL, 0x7681D14DL,
	0x2892ED69L, 0xDAF96E6AL, 0xC9A99D9EL, 0x3BC21E9DL,
	0xEF087A76L, 0x1D63F975L, 0x0E330A81L, 0xFC588982L,
	0xB21572C9L, 0x407EF1CAL, 0x532E023EL, 0xA145813DL,
	0x758FE5D6L, 0x87E466D5L, 0x94B49521L, 0x66DF1622L,
	0x38CC2A06L, 0xCAA7A905L, 0xD9F75AF1L, 0x2B9CD9F2L,
	0xFF56BD19L, 0x0D3D3E1AL, 0x1E6DCDEEL, 0xEC064EEDL,
	0xC38D26C4L, 0x31E6A5C7L, 0x22B65633L, 0xD0DDD530L,
	0x0417B1DBL, 0xF67C32D8L, 0xE52CC12CL, 0x1747422FL,
	0x49547E0BL, 0xBB3FFD08L, 0xA86F0EFCL, 0x5A048DFFL,
	0x8ECEE914L, 0x7CA56A17L, 0x6FF599E3L, 0x9D9E1AE0L,
	0xD3D3E1ABL, 0x21B862A8L, 0x32E8915CL, 0xC083125FL,
	0x144976B4L, 0xE622F5B7L, 0xF5720643L, 0x07198540L,
	0x590AB964L, 0xAB613A67L, 0xB831C993L, 0x4A5A4A90L,
	0x9E902E7BL, 0x6CFBAD78L, 0x7FAB5E8CL, 0x8DC0DD8FL,
	0xE330A81AL, 0x115B2B19L, 0x020BD8EDL, 0xF0605BEEL,
	0x24AA3F05L, 0xD6C1BC06L, 0xC5914FF2L, 0x37FACCF1L,
	0x69E9F0D5L, 0x9B8273D6L, 0x88D28022L, 0x7AB90321L,
	0xAE7367CAL, 0x5C18E4C9L, 0x4F48173DL, 0xBD23943EL,
	0xF36E6F75L, 0x0105EC76L, 0x12551F82L, 0xE03E9C81L,
	0x34F4F86AL, 0xC69F7B69L, 0xD5CF889DL, 0x27A40B9EL,
	0x79B737BAL, 0x8BDCB4B9L, 0x988C474DL, 0x6AE7C44EL,
	0xBE2DA0A5L, 0x4C4623A6L, 0x5F16D052L, 0xAD7D5351L
};

/*
static uint32_t
_crc32c(uint32_t c, const uint8_t *b, jamsize_t l)
{
	while (l--) {
		c ^= *b++;
		for (int i = 0; i < 8; ++i)
			c = (c >> 1) ^ ((c & 1) ? 0x82F63B78 : 0);
	}
	return c;
}
*/

static uint32_t
_crc32c(uint32_t c, const uint8_t *b, jamsize_t l)
{
	if (l)
		jam_ASSERT(b);

	while (l--)
		c = _crc32c_tbl[(c ^ *b++) & 0xff] ^ (c >> 8);

	return c;
}

/*
	jambSet(&j, s, t, NULL, l != 0) -> Internal storage
	jambSet(&j, s, t, d != NULL , l) -> External storage
*/
bool
jambSet(jamb_t *j, uint8_t s, uint8_t t, uint8_t *d, jamsize_t l)
{
	jam_ASSERT(j);

	j->source = s;
	j->target = t;

	jamsize_t n = (l < jamb_BUFFERSPACE) ? jamb_BUFFERSPACE : l;

	if (d)
	{
		if (d != jambData(j)) // External data
		{
			if (j->data && j->bound)
				_memDealloc(j->data);

			j->data = d;
			j->bound = false;
			j->size = j->space = l;
		}
		else // Reshape
			j->size = (l < j->space) ? l : j->space;
	}
	else // NULL d, create or resize
	{
		if (!j->bound)
			j->data = NULL;
		else if (j->data && j->space != n)
		{
			_memDealloc(j->data);
			j->data = NULL;
		}

		if (!j->data && n > jamb_BUFFERSPACE) // Dynamic memory needed
		{
			j->data = _memAlloc(n, 0);
			if (!j->data)
				return false;
		}

		j->bound = true;
		j->size = 0;
		j->space = n;
	}

	j->crc = _crc32c(0xffffffff, &j->source, 1);
	j->crc = _crc32c(j->crc, &j->target, 1);
	if (j->size)
		j->crc = _crc32c(j->crc, jambData(j), j->size);

	j->index = j->block = j->count = 0;

	return true;
}

void
jambFree(jamb_t *j)
{
	jam_ASSERT(j);

	if (j->data && j->bound)
		_memDealloc(j->data);
	j->data = NULL;
	j->size = 0;
	j->space = jamb_BUFFERSPACE;
	j->bound = true;
}

bool
jambUpdate(jamb_t *j, const uint8_t *d, jamsize_t l)
{
	jam_ASSERT(j);

	if (l > j->space) // Not enough space
	{
		bool t = jambAdjust(j, l, d ? false : true);
		if (!t)
			return false;
	}

	if (d)
		_memCopy((void *)jambData(j), d, l);
	j->size = l;

	j->crc = _crc32c(0xffffffff, &j->source, 1);
	j->crc = _crc32c(j->crc, &j->target, 1);
	if (j->size)
		j->crc = _crc32c(j->crc, jambData(j), j->size);

	return true;
}

bool
jambAdjust(jamb_t *j, jamsize_t n, bool p)
{
	jam_ASSERT(j);

	n = n < jamb_BUFFERSPACE ? jamb_BUFFERSPACE : n;
	if (n == j->space)
		return true; // Nothing to do

	if (j->data && !j->bound)
		return false; // Cannot adjust with external data

	void *d = (n == jamb_BUFFERSPACE) ? j->buffer : _memAlloc(n, 0);
	if (!d)
		return false;

	jamsize_t l = n < j->size ? n : j->size;

	if (p) // Preserve data
		_memCopy(d, jambData(j), l);
	if (j->data && j->bound) // Release if dynamic
		_memDealloc(j->data);

	j->data = (n > jamb_BUFFERSPACE) ? d : NULL;
	j->size = l;
	j->space = n;
	j->bound = true;

	return true;
}

bool
jambCopy(jamb_t *j, const jamb_t *q)
{
	jam_ASSERT(j && q);

	if (j == q)
		return true;

	j->source = q->source;
	j->target = q->target;

//	if (j->space < q->size && !jambAdjust(j, q->size, false))
//		return false;

	return jambUpdate(j, jambData(q), q->size);
}

jamsize_t
jambBytes(const jamb_t *j)
{
	jam_ASSERT(j);
	jamsize_t l = jambLength(j);
	jamsize_t t = (l-1)/254 + 1;
	return l + t + 0 /*SEP*/;
}

bool
jambSetJam(jamb_t *j, uint8_t s, uint8_t t, const jam_t *k)
{
	jamsize_t b = jamBytes(k, 0);
	jambSet(j, s, t, NULL, b);
	j->size = jamFormat(k, (uint8_t *)jambData(j), j->space, jSkp);
	return jambUpdate(j, NULL, j->size);
}


/** Next zero count */
static int
_blockCount(const jamb_t *j, jamsize_t o)
{
	jam_ASSERT(j);
	int c = 0;

	while (c < 0xfe)
	{
		switch (o)
		{
		case 0:
			if (j->source == 0)
				return c;
			else
				++c, ++o;
			break;

		case 1:
			if (j->target == 0)
				return c;
			else
				++c, ++o;
			break;

		default:
			{
				jamsize_t q = o - 2;

				if (q < j->size)
				{
					while ((c < 0xfe) && (q < j->size))
						if (jambData(j)[q] == 0)
							return c;
						else
							++c, ++q;
					o = j->size + 2;
				}
				else
				{
					q -= j->size;

					if (q >= 4)
						return -1; // No more bytes

					while ((c < 0xfe) && (q < 4))
						if (((uint8_t *)&j->crc)[q] == 0)
							return c;
						else
							++c, ++q;

					return c; // End reached without zeros
				}
			}
			break;
		}
	}

	return c;
}

static inline const uint8_t *
_jambByte(const jamb_t *j, jamsize_t o)
{
	jam_ASSERT(j);

	switch (o)
	{
	case 0: return &j->source;
	case 1: return &j->target;
	default:
		{
			o -= 2;
			if (o < j->size)
				return jambData(j) + o;
			o -= j->size;
			if (o < 4)
				return (uint8_t *)&j->crc + o;
			return NULL;
		}
	}
}

static inline jamsize_t
_jambWrite(const jamb_t *j, jamsize_t o, uint8_t *b, jamsize_t l)
{
	jam_ASSERT(j);
	jamsize_t i = 0;

	while (i < l)
	{
		switch (o + i)
		{
		case 0:
			jamWrite(b, i++, &j->source, 1);
			break;

		case 1:
			jamWrite(b, i++, &j->target, 1);
			break;

		default:
			{
				l -= i;
				jamsize_t q = (o + i) - 2;

				if (q < j->size)
				{
					jamsize_t r = j->size - q;
					r = (r < l) ? r : l;
					r = jamWrite(b, i, jambData(j) + q, r);
					i += r;
					l -= r;
					if (q + r != j->size)
						return i;
					else
						q = 0;
				}
				else
					q -= j->size;

				if (q < 4)
				{
					jamsize_t r = 4 - q;
					r = (r < l) ? r : l;
					r = jamWrite(b, i, ((uint8_t *)&j->crc) + q, r);
					i += r;
				}

				return i;
			}
		}
	}

	return i;
}

int
jambWrite(const jamb_t *j, uint8_t *b, jamsize_t l)
{
	jam_ASSERT(j);
	jamsize_t n = jambLength(j), i = 0;
	jamb_t *k = (jamb_t *)j;

	while (k->index < n && i < l)
	{
		if (!k->count)
		{
			int c = _blockCount(k, k->index);

			if (c < 0) // No more bytes
				return i;

			if (k->block == 1 && c) // Single zero
				--i;

			uint8_t d = c + 1;
			jamWrite(b, i++, &d, 1);

			if (k->block && !c)
				++k->index; // Skip zero

			k->block = d;
			k->count = c;
		}
		else // Non-zero block
		{
			jamsize_t q = l - i;
			q = (q < k->count) ? q : k->count;
			q = _jambWrite(k, k->index, b + i, q);
			k->count -= q;
			k->index += q;
			if (k->block == 0xff)
				k->block = 0;
			i += q;
		}
	}

	return i;
}

bool
jambCheck(jamb_t *j)
{
	jam_ASSERT(j);

	if (j->index < 6 || j->crc != 0)
		return false;

	jamsize_t s = j->index - 6;

/*
	uint32_t c = *(uint32_t *)(jambData(j) + s);
	if (c != j->crc)
		return false; // Bad crc
*/
	
	j->size = s; // Fix size
	return true;
}

/*
	When reading, 'space' indicates the total space of the data array
	and 'index' the number of read bytes. The read CRC is stored in
	the data array.
*/
static inline jamsize_t
_jambRead(jamb_t *j, jamsize_t o, const uint8_t *b, jamsize_t l)
{
	jam_ASSERT(j);

	jamsize_t i = 0;

	while (i < l)
	{
		switch (o + i)
		{
		case 0:
			jamRead(b, i++, &j->source, 1);
			j->crc = _crc32c(0xffffffff, &j->source, 1);
			break;

		case 1:
			jamRead(b, i++, &j->target, 1);
			j->crc = _crc32c(j->crc, &j->target, 1);
			break;

		default: // Data and CRC
			{
				l -= i;

				if (j->space > l + o)
				{
					jamsize_t q = o + i - 2;

					jamsize_t r = j->space - q;
					r = (r < l) ? r : l;
					r = jamRead(b, i, (uint8_t *)jambData(j) + q, r);
					j->crc = _crc32c(j->crc, b + i, r);
					i += r;
					// l -= r;
				}
				return i;
			}
		}
	}

	return i;
}

int
jambRead(jamb_t *j, const uint8_t *b, jamsize_t l)
{
	jam_ASSERT(j);

	jamsize_t i = 0;

	while (i < l)
	{
		if (!j->count)
		{
			uint8_t d = 0;
			jamRead(b, i, &d, 1);

			if (!d)
			{
				if (i == 0)
					return EFormat;
				else
					return i;
			}
			++i;

			j->count = d-1;

			if (j->block && (d < 0xff)) // Read zero
			{
				if (j->space + 2 <= j->index)
					return EOverflow;

				uint8_t e = 0;
				_jambRead(j, j->index++, &e, 1);
			}
			else if (j->space + 2 < j->index + j->count)
				return EOverflow;

			j->block = d;
		}
		else
		{
			jamsize_t q = (l - i);
			q = (q < j->count) ? q : j->count;

			q = _jambRead(j, j->index, b + i, q);
			j->index += q;
			j->count -= q;
			i += q;

			if (!j->count && j->block == 0xff) // Restart
				j->block = 0;
		}
	}

	return i;
}
