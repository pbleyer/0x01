/** @file
	jam Bus
*/

#ifndef JAMBUS_H_
#define JAMBUS_H_

#include "jam.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Jam bus API */
/* Packet format (COBS encoded):
	[SRC:1_B] [DST:1_B] [PLD:N_B] [CRC:4_B]
*/

/** jam bus object */
typedef struct jamb_t jamb_t;
struct jamb_t
{
	uint8_t source; ///< Source address
	uint8_t target; ///< Target address
	uint8_t *data; ///< External/dynamic data pointer, when NULL internal buffer is used
	jamsize_t size; ///< Payload byte length
	uint32_t crc; ///< Packet CRC

	jamsize_t space; ///< Size when data is buffer
	bool bound; ///< Dynamic data when true

	jamsize_t index; ///< Processing data index
	uint8_t block; ///< Block bytes
	uint8_t count; ///< Remaining block bytes

	uint8_t buffer[jamb_BUFFERSPACE]; ///< Internal buffer
};

#define jambINIT { 0, 0, NULL, 0, 0, 0, true, 0, 0, 0 }
#define jambVar(j) jamb_t j = jambINIT

/** Setup a jamb_t packet object */
bool jambSet(jamb_t *j, uint8_t s, uint8_t t, uint8_t *d, jamsize_t l);

/** Embed a jam object in a jamb packet */
bool jambSetJam(jamb_t *j, uint8_t s, uint8_t t, const jam_t *k);

/** Free a jamb_t object with dynamic data */
void jambFree(jamb_t *j);

/** Copy a jamb_t object */
bool jambCopy(jamb_t *j, const jamb_t *q);

/** Update object data and CRC */
bool jambUpdate(jamb_t *j, const uint8_t *d, jamsize_t l);

/** Update object data and CRC */
static inline bool jambUpdateJam(jamb_t *j, const jam_t *k)
{
	return jambSetJam(j, j->source, j->target, k);
}

/** Adjust storage space */
bool jambAdjust(jamb_t *j, jamsize_t l, bool p);

/** Reset the buffer stream */
static inline void jambReset(jamb_t *j)
{
	jam_ASSERT(j);
	j->index = j->block = j->count = 0;
}

/** Calculate packet total length */
static inline jamsize_t jambLength(const jamb_t *j)
{
	jam_ASSERT(j);
	return 6 + j->size;
}

/** Calculate serialized buffer size for packet object */
jamsize_t jambBytes(const jamb_t *j);

/** Serialize packet to buffer */
int jambWrite(const jamb_t *j, uint8_t *b, jamsize_t l);

/** Deserialize packet from buffer */
int jambRead(jamb_t *j, const uint8_t *b, jamsize_t l);

/** Verify read packet and adjust fields */
bool jambCheck(jamb_t *j);

static inline const uint8_t *jambData(const jamb_t *j)
{
	jam_ASSERT(j);
	return (j->data) ? j->data : j->buffer;
}

#ifdef __cplusplus
} // extern "C"

namespace jam_NAMESPACE {

/** jamb_t wrapper class */
class jamb_c
{
protected:
	jamb_t _jamb;

public:
	jamb_c(): _jamb(jambINIT) { jambSet(&_jamb, 0, 0, nullptr, 0); }
	jamb_c(jamsize_t l): _jamb(jambINIT) { jambSet(&_jamb, 0, 0, nullptr, l); }
	jamb_c(uint8_t s, uint8_t t, jamsize_t l): _jamb(jambINIT) { jambSet(&_jamb, s, t, NULL, l); }
	jamb_c(uint8_t s, uint8_t t, uint8_t *d, jamsize_t l, bool b = false)
		: _jamb(jambINIT)
	{
		if (!b) // Not bound
			jambSet(&_jamb, s, t, d, l);
		else
		{
			jambSet(&_jamb, s, t, nullptr, l);
			jambUpdate(&_jamb, d, l);
		}
	}
	jamb_c(uint8_t s, uint8_t t, const std::vector<uint8_t> &v)
		: _jamb(jambINIT)
	{
		jambSet(&_jamb, s, t, nullptr, v.size());
		jambUpdate(&_jamb, v.data(), v.size());
	}
	jamb_c(uint8_t s, uint8_t t, const jam_c &j)
		: _jamb(jambINIT)
	{
		jamsize_t b = j.bytes();
		jambSet(&_jamb, s, t, nullptr, b);
		_jamb.size = j.format((uint8_t *)jambData(&_jamb), b, Skp);
		jambUpdate(&_jamb, nullptr, b);
	}

	jamb_c(const jamb_c &j): _jamb(jambINIT) { jambCopy(&_jamb, &j._jamb); }
	~jamb_c() { jambFree(&_jamb); }

#if __cplusplus >= 201103L
	/** Move constructor. */
	jamb_c(jamb_c &&j)
	: _jamb(std::move(j._jamb))
	{
		j._jamb.bound = false;
	}
#endif

	inline jamb_c& operator=(const jamb_c &j)
	{
		jambCopy(&_jamb, &j._jamb);
		return *this;
	}

#if __cplusplus >= 201103L
	inline jamb_c& operator=(jamb_c &&j)
	{
		_jamb = std::move(j._jamb);
		j._jamb.bound = false;
		return *this;
	}
#endif

	void setup(uint8_t s, uint8_t t, uint8_t *d = nullptr, jamsize_t l = 0)
	{
		jambSet(&_jamb, s, t, d, l);
	}

	/** Update data and recalculate CRC */
	bool update(const uint8_t *d, jamsize_t l) { return jambUpdate(&_jamb, d, l); }

	/** Recalculate CRC */
	uint32_t update() { jambUpdate(&_jamb, jambData(&_jamb), _jamb.size); return _jamb.crc; }

	void reset() { jambReset(&_jamb); }
	jamsize_t length() const { return jambLength(&_jamb); }
	jamsize_t bytes() const { return jambBytes(&_jamb); }
	jamsize_t space() const { return _jamb.space; }
	bool bound() const { return _jamb.bound; }

	uint8_t source() const { return _jamb.source; }
	void setSource(uint8_t s) { _jamb.source = s; }

	uint8_t target() const { return _jamb.target; }
	void setTarget(uint8_t t) { _jamb.target = t; }

	const uint8_t *data() const { return jambData(&_jamb); }
	void setData(uint8_t *d, bool b = false) { _jamb.data = d; _jamb.bound = b; }

	jamsize_t size() const { return _jamb.size; }
	void setSize(jamsize_t l) { _jamb.size = l < _jamb.space ? l : _jamb.space; }

	uint32_t crc() const { return _jamb.crc; }
	void setCrc(uint32_t c) { _jamb.crc = c; }

	jamsize_t index() const { return _jamb.index; }
	void setIndex(jamsize_t i) { _jamb.index = i; }

	uint8_t block() const { return _jamb.block; }
	void setBlock(uint8_t b) { _jamb.block = b; }

	uint8_t count() const { return _jamb.count; }
	void setCount(uint8_t c) { _jamb.count = c; }

	/** Convert jamb_c to underlying jamb_t */
	operator jamb_t *() { return &_jamb; }
	operator const jamb_t *() const { return &_jamb; }
	jamb_t *jamb() { return &_jamb; }
	const jamb_t *jamb() const { return &_jamb; }

	uint8_t operator[](jamsize_t i) const
	{
		return (i < _jamb.size) ? jambData(&_jamb)[i] : 0;
	}

/*
	uint8_t &operator[](jamsize_t i)
	{
		return (_jamb.data && i < _jamb.size) ? _jamb.data[i] : 0;
	}
*/

	int write(uint8_t *b, jamsize_t l) const { return jambWrite(&_jamb, b, l); }
	int read(const uint8_t *b, jamsize_t l) { return jambRead(&_jamb, b, l); }

	bool check() { return jambCheck(&_jamb); }
	operator bool() { return check(); }
};

} // namespace jam_NAMESPACE

#endif // __cplusplus

#endif // JAMBUS_H_
