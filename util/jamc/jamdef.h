/** @file
	Jam configuration.
*/

#ifndef JAMDEF_H_
#define JAMDEF_H_

#include "jamcfg.h"

/** Object type used to store type/size/items field values */
#ifndef jam_FIELDTYPE
#define jam_FIELDTYPE uintptr_t
#endif

/** Object type used to store size field values */
#ifndef jam_SIZETYPE
#define jam_SIZETYPE uintptr_t
#endif

/** Dynamic memory header file */
// #ifndef jam_MEMHEADER
// #define jam_MEMHEADER <malloc.h>
// #endif

/** Has wide char */
#if 0
// #ifdef WCHAR_MAX
#define jam_WCHAR
#endif

/** Memory allocation function (same prototype as malloc) */
#ifndef jam_ALLOC
#define jam_ALLOC malloc
#endif

/** Allocated memory free function (same prototype as free) */
#ifndef jam_FREE
#define jam_FREE free
#endif

/** Memory byte copy function (same prototype as memcpy) */
#ifndef jam_MEMCPY
#define jam_MEMCPY memcpy
#endif

/** Memory set function (same prototype as memset) */
#ifndef jam_MEMSET
#define jam_MEMSET memset
#endif

/** Prefix for global module names */
#ifndef jam_PREFIX
#define jam_PREFIX j
#endif

/** Block size in bytes for stream operations */
#ifndef jams_BLOCK
#define jams_BLOCK 512
#endif

/** Code assertions macro */
#ifndef jam_ASSERT
#define jam_ASSERT(x) (void)0
#endif

/** C++ namespace */
#ifndef jam_NAMESPACE
#define jam_NAMESPACE jam
#endif

/** jamb_t internal storage */
#ifndef jamb_BUFFERSPACE
#define jamb_BUFFERSPACE 16
#endif

#endif // JAMDEF_H_
