#include "jamios.h"
#include "jammem.h"
#include "jamprv.h"

#ifdef jam_DEBUG
extern int _objects; ///< Track number of objects
extern int _allocs; ///< Track memory allocations
#endif

void
jamsSetup(jams_t *s, jams_e m, void *bof, jamsize_t l, uint8_t f)
{
	jam_ASSERT(s);

	s->mode = m;
	if (m == j_(Buf))
		s->buffer = (uint8_t *)bof;
	else
		s->file = (FILE *)bof;
	s->index = 0;
	s->size = l; // Size unspecified if == 0
	s->flags = f;
	s->block = jams_BLOCK;
}

int
jamsWrite(jams_t *s, const uint8_t *b, jamsize_t l)
{
	jam_ASSERT(s);

	if (s->size && l > (s->size - s->index))
		return EOverflow; // No space left

	if (s->mode == j_(Buf))
		l = jamWrite(s->buffer, s->index, b, l);
	else
		l = fwrite(b, 1, l, s->file);

	s->index += l;

	return l;
}

int
jamsRead(jams_t *s, uint8_t *b, jamsize_t l)
{
	jam_ASSERT(s);

	if (s->size && l > (s->size - s->index))
		return EOverflow; // No space left

	if (s->mode == j_(Buf))
		l = jamWrite(s->buffer, s->index, b, l);
	else
		l = fwrite(b, 1, l, s->file);

	s->index += l;

	return (int)l;
}

int jamsFormat(jams_t *s, const jam_t *j)
{
	jam_ASSERT(s);

	jamsize_t l = jamBytes(j, s->flags);
	uint8_t *b = _memAlloc(l, -1);
	if (b)
	{
		jamFormat(j, b, l, Skp | s->flags);
		l = jamsWrite(s, b, l);
		_memDealloc(b);
	}

	return (int)l;
}

int jamsParse(jams_t *s, jam_t *j)
{
	jam_ASSERT(s);

	int l;
	jam_t k = jamINIT;

	while (true)
	{
		uint8_t *b = NULL;
		l = jamsRead(s, b, s->block);

		if (l < 0)
			return l;

		l = jamParse(&k, b, l, s->flags);
		break;
	}

	return l;
}
