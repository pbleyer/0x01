/** @file
	jam IO streams
*/

#ifndef JAMIOS_H_
#define JAMIOS_H_

#include "jam.h"

#include <stdio.h>

#define j___(p,x) p##x
#define j__(p,x) j___(p,x)
#define j_(x) j__(jam_PREFIX,x)

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	j_(Buf) = 0,
	j_(Fil)
} jams_e;

/** Jam stream I/O */
typedef struct jams_t jams_t;
struct jams_t
{
	jams_e mode; ///< Operating mode
	union
	{
		uint8_t *buffer;
		FILE *file;
	};
	jamsize_t size; ///< Total size
	jamsize_t index; ///< Current index
	uint8_t flags; ///< Formatting flags
	jamsize_t block; ///< Block size for file IO
};

/** Initialize jam stream */
void jamsSetup(jams_t *s, jams_e m, void *bof, jamsize_t l, uint8_t f);

int jamsWrite(jams_t *s, const uint8_t *b, jamsize_t l);
int jamsRead(jams_t *s, uint8_t *b, jamsize_t l);

int jamsFormat(jams_t *s, const jam_t *j);
int jamsParse(jams_t *s, jam_t *j);

#ifdef __cplusplus
} // extern "C"

namespace jam_NAMESPACE {

/** Jam output. */
class jamo_c
{
public:
	std::ostream *stream;
	uint8_t flags; ///< Formatting flags.

public:
	jamo_c(): stream(nullptr), flags(0) { }
	jamo_c(std::ostream &s, uint8_t f = Hdr|Val): stream(&s), flags(f) { }
	~jamo_c() { }

	jamo_c &operator<<(const jam_c &j)
	{
		if (stream)
		{
			size_t bl = j.bytes(flags);
			uint8_t *b = new uint8_t[bl];
			if (b)
			{
				j.format(b, bl, Skp|flags);
				stream->write((const char *)b, bl);
				delete[] b;
			}
		}

		return *this;
	}

	explicit operator bool() { return stream ? bool(*stream) : false; }
};

/** Jam input. */
class jami_c
{
public:
	using types_t = std::unordered_map<jam_FIELDTYPE, jam_c>; ///< Container for type definitions.

public:
	std::istream *stream;
	types_t types; ///< Type storage.

public:
	jami_c(): stream(nullptr) { }
	jami_c(std::istream &s): stream(&s) { }
	~jami_c() { }

	explicit operator bool() { return stream ? bool(*stream) : false; }
};

/*
std::ostream &operator<<(std::ostream &o, const jam_c &j)
{
	return o;
}
*/


} // jam_NAMESPACE

#endif // __cplusplus

#undef j_
#undef j__
#undef j___

#endif // JAMIOS_H_
