/** @file
	jam internal memory management functions
*/

#ifndef JAMMEM_H_
#define JAMMEM_H_

#include "jamdef.h"

#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef jam_DEBUG
extern int _jamObjects; ///< Track number of objects
extern int _jamAllocs; ///< Track memory allocations
#endif

/** Allocate memory.
	@param s Allocation size
	@param c If zero or positive, set to value
	@return Pointer to allocated memory
*/
static inline void *_memAlloc(jamsize_t s, int c)
{
	void *p = s ? jam_ALLOC(s) : NULL;

	if (s)
		jam_ASSERT(p);

	if (p)
	{
#ifdef jam_DEBUG
		++_jamAllocs;
#endif

		if (c >= 0) // Set memory
			jam_MEMSET(p, c & 0xff, s);
	}

	return p;
}

/** Deallocate memory.
	@param Pointer to allocated memory
*/
static inline void
_memDealloc(void *p)
{
#ifdef jam_DEBUG
	if (p)
		--_jamAllocs;
#endif

	jam_FREE(p);
}

/** Clone memory.
	@param v Pointer to memory to be copied, if NULL then new memory is cleared
	@param s Memory size
	@return New memory
*/
static inline void *
_memClone(const void *v, jamsize_t s)
{
	void *p = _memAlloc(s, v ? -1 : 0);

	if (v)
		jam_MEMCPY(p, v, s);

	return p;
}

/** Set memory */
static inline void *
_memSet(void *p, uint8_t c, jamsize_t s)
{
	jam_ASSERT(p);
	jam_MEMSET(p, c, s);
	return p;
}

/** Copy memory */
static inline void *
_memCopy(void *p, const void *v, jamsize_t s)
{
	jam_ASSERT(p);

	if (s)
		jam_ASSERT(v);

	if (p != v)
		jam_MEMCPY(p, v, s);
	return p;
}

/** Clone string
	@param s Pointer to source string
	@param l String length (if zero then length is found assuming null-terminated string)
	@return Cloned string pointer (null-terminated)
*/
static inline char *
_strClone(const char *s, jamsize_t l)
{
	if (!s && !l)
		return NULL;

	if (l == 0) // Calculate string length
		l = strlen(s);

	char *p = _memAlloc(l + 1, s ? -1 : 0);

	if (p)
	{
		if (s)
			_memCopy(p, s, l);
		p[l] = '\0';
	}

	return p;
}

#ifdef jam_WCHAR
/** Clone wstring
	@param s Pointer to source string
	@param l String length (if zero then length is found assuming null-terminated string)
	@return Cloned string pointer (null-terminated)
*/
static inline wchar_t *
_wstrClone(const wchar_t *s, jamsize_t l)
{
	if (!s && !l)
		return NULL;

	if (l == 0) // Calculate string length
		l = wcslen(s);

	wchar_t *p = _memAlloc(l + sizeof(wchar_t), s ? -1 : 0);

	if (p)
	{
		if (s)
			_memCopy(p, s, l*sizeof(wchar_t));
		p[l] = '\0';
	}

	return p;
}
#endif

#ifdef __cplusplus
} // extern "C"
#endif

#endif // JAMMEM_H_
