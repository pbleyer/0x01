/** @file
	jam private definitions
*/

#ifndef JAMPRV_H_
#define JAMPRV_H_

#include "jamdef.h"

#define j___(p,x) p##x
#define j__(p,x) j___(p,x)
#define j_(x) j__(jam_PREFIX,x)

// Internal aliases
#define Ok j_(Ok)
#define Err j_(Err)
#define Error j_(Error)
#define ESystem j_(ESystem)
#define ERange j_(ERange)
#define EArgument j_(EArgument)
#define EOverflow j_(EOverflow)
#define ETimeout j_(ETimeout)
#define EFormat j_(EFormat)
#define EBusy j_(EBusy)
#define ECancel j_(ECancel)
#define EFail j_(EFail)
#define EAbort j_(EAbort)
#define ERefuse j_(ERefuse)
#define EReset j_(EReset)
#define EExist j_(EExist)
#define EIo j_(EIo)
#define EData j_(EData)
#define EMemory j_(EMemory)
#define EPermission j_(EPermission)
#define EUnknown j_(EUnknown)

#define SngI j_(SngI)
#define SngE j_(SngE)
#define Lst j_(Lst)
#define Map j_(Map)
#define Spc j_(Spc)
#define Usr j_(Usr)
#define ArrI j_(ArrI)
#define ArrE j_(ArrE)
#define Und j_(Und)

#define U8 j_(U8)
#define U16 j_(U16)
#define U32 j_(U32)
#define U64 j_(U64)
#define I8 j_(I8)
#define I16 j_(I16)
#define I32 j_(I32)
#define I64 j_(I64)
#define C8 j_(C8)
#define C16 j_(C16)
#define F32 j_(F32)
#define F64 j_(F64)
#define T j_(T)
#define F j_(F)
#define X j_(X)
#define N j_(N)
#define True j_(True)
#define False j_(False)
#define Unknown j_(Unknown)
#define Null j_(Null)

#define Bnd	j_(Bnd)
#define Fix	j_(Fix)
#define Cst	j_(Cst)
#define Nts	j_(Nts)
#define Xfr	j_(Xfr)

#define Dsc	j_(Dsc)
#define Inf	j_(Inf)
#define Val	j_(Val)
#define Nrc	j_(Nrc)
#define Upd	j_(Upd)
#define Tag	j_(Tag)
#define Skp	j_(Skp)
#define Hdr	j_(Hdr)

// #undef j_
// #undef j__
// #undef j___

#endif // JAMPRV_H_
