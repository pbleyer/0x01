#include "jamreg.h"
#include "jammem.h"
#include "jamprv.h"

#include <util_dlst.h>

/** Registry node. */
typedef struct jamnode_t jamnode_t;
struct jamnode_t
{
	dlst_HEADER; ///< List container
	jamtype_t type; ///< Type object
};

/** Opaque structure */
struct jamr_t
{
	DlstList list;
};

jamr_t *
jamrNew()
{
	jamr_t *d = _memAlloc(sizeof(jamr_t), -1);
	if (d)
		// jam_MEMSET(d, 0, sizeof(jamr_t));
		dlstSetup(&d->list);
	return d;
}

// Free node resources and delete node
static int
_jamrDeleteFnc(void *n, void *a)
{
	jam_ASSERT(n);
	jamtype_t *t = &((jamnode_t *)n)->type;
	_memDealloc((void *)t->name);
	_memDealloc(n);
	return 0;
}

void
jamrDelete(jamr_t *d)
{
	jam_ASSERT(d);
	dlstApply(d->list.head, _jamrDeleteFnc, NULL, NULL, 1);
	_memDealloc(d);
}

jamsize_t
jamrItems(jamr_t *d)
{
	jam_ASSERT(d);
	return (jamsize_t)dlstItems(&d->list);
}

jamnode_t *_jamrQuery(jamr_t *d, jamkind_e k, jamfield_t t);

jamtype_t *
jamrInsert(jamr_t *d, const char *n, const jaminfo_t *i)
{
	jam_ASSERT(d && i);
	jamnode_t *q = _jamrQuery(d, i->kind, i->type);

	if (!q) // New item
	{
		q = _memAlloc(sizeof(jamnode_t), 0);
		if (!q)
			return NULL;
		dlstNodeSetup(q);
		dlstInsertBack(&d->list, q);
		jaminfoSetup(&q->type.info, Und, 0, 0, 0);
	}

	jamtype_t *r = &q->type;

	_memDealloc((void *)r->name);
	r->name = _strClone(n, 0);
	r->info = *i;

	return r;
}

static int
_jamrQueryFnc(void *n, void *a)
{
	jam_ASSERT(n && a);
	jamtype_t *t = &((jamnode_t *)n)->type;
	jaminfo_t *s = (jaminfo_t *)a;

	return t->info.kind == s->kind && t->info.type == s->type;
}

jamnode_t *
_jamrQuery(jamr_t *d, jamkind_e k, jamfield_t t)
{
	jam_ASSERT(d);
	jaminfo_t s = {k, t};
	return (jamnode_t *)dlstSearch(d->list.head, _jamrQueryFnc, &s, 1);
}

jamtype_t *
jamrQuery(jamr_t *d, jamkind_e k, jamfield_t t)
{
	jam_ASSERT(d);
	jamnode_t *n = _jamrQuery(d, k, t);
	if (n) // Found
		return &n->type;
	else
		return NULL;
}

static int
_jamrFindFnc(void *n, void *a)
{
	jam_ASSERT(n);
	jamtype_t *t = &((jamnode_t *)n)->type;

	if (!t->name)
		return 0;

	return strcmp(t->name, (const char *)a) == 0;
}

jamtype_t *
jamrFind(jamr_t *d, const char *a)
{
	jam_ASSERT(d);
	jamnode_t *n = (jamnode_t *)dlstSearch(d->list.head, _jamrFindFnc, (void *)a, 1);
	if (n) // Found
		return &n->type;
	else
		return NULL;
}

static int
_jamrRemoveFnc(void *n, void *a)
{
	jam_ASSERT(n && a);
	jamtype_t *t = &((jamnode_t *)n)->type;
	if (t == a) // Found
	{
		_memDealloc((void *)t->name);
		_memDealloc(n);
		return 1;
	}
	else
		return 0;
}

bool
jamrRemove(jamr_t *d, jamtype_t *t)
{
	jam_ASSERT(d);
	return dlstApply(d->list.head, _jamrRemoveFnc, t, NULL, 1);
}

bool
jamrProcess(jamr_t *d, jam_t *j, void *ctx)
{
	jam_ASSERT(d && j);
	jamtype_t *t = jamrQuery(d, j->kind, j->type);

	if (!t || !t->proc || !jamCheck(j, &t->info))
		return false;

	// Process data using callback
	return t->proc(j, t->user, ctx);
}
