/** @file
	jam Registry
*/

#ifndef JAMREG_H_
#define JAMREG_H_

#include "jam.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Object registry opaque type */
typedef struct jamr_t jamr_t;

/** Create new jam registry */
jamr_t *jamrNew(void);

/** Delete registry */
void jamrDelete(jamr_t *d);

/** Add/replace type definition to registry
	@param d Pointer to registry object
	@param n Entry object name
	@param i Entry type information
	@return Pointer to object jamtype_t in registry
*/
jamtype_t *jamrInsert(jamr_t *d, const char *n, const jaminfo_t *i);

/** Get number of registered items */
jamsize_t jamrItems(jamr_t *d);

/** Find type object in registry by kind and type */
jamtype_t *jamrQuery(jamr_t *d, jamkind_e k, jamfield_t t);

/** Find type in registry by name */
jamtype_t *jamrFind(jamr_t *d, const char *n);

/** Remove type from registry using type object pointer */
bool jamrRemove(jamr_t *d, jamtype_t *t);

/** Process object using registry.
	@return True on success.
*/
bool jamrProcess(jamr_t *d, jam_t *j, void *ctx);

#ifdef __cplusplus
} // extern "C"

namespace jam_NAMESPACE {

/** jamr_t wrapper class */
class jamr_c
{
private:
	jamr_t *_jamr;

public:
	jamr_c(): _jamr(jamrNew()) { }
	~jamr_c() { jamrDelete(_jamr); }

	inline type_t *insert(const char *n, const info_t &i) { return jamrInsert(_jamr, n, &i); }
	inline size_t items() const { return jamrItems(_jamr); }
	inline type_t *query(kind_e k, jam_c::field_t t) const { return jamrQuery(_jamr, k, t); }
	inline type_t *find(const char *n) const { return jamrFind(_jamr, n); }
	inline bool remove(type_t *t) { return jamrRemove(_jamr, t); }
	inline bool process(jam_c &j, void *ctx = nullptr) const { return jamrProcess(_jamr, j.jam(), ctx); }
	inline bool process(jam_t *j, void *ctx = nullptr) const { return jamrProcess(_jamr, j, ctx); }
};

} // namespace jam_NAMESPACE

#endif // __cplusplus

#endif // JAMREG_H_
