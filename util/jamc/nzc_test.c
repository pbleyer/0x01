#include <stdio.h>

#include "jambus.h"

static int
_nonzeroCount(const jamb_t *j, jamsize_t o, bool *z)
{
	jamsize_t c = 0;

	if (z)
		*z = true;

	while (true)
	{
		if (o == 0)
		{
			if (j->source == 0)
				return c;
			else
				++c, ++o;
		}
		else if (o == 1)
		{
			if (j->target == 0)
				return c;
			else
				++c, ++o;
		}
		else
		{
			if (j->data && ((o-2) < j->size))
			{
				jamsize_t q = o - 2;
				while (q < j->size)
					if (*(j->data + q) == 0)
						return c;
					else
						++c, ++q;
				o = j->size + 2;
			}
			else
			{
				o -= (j->data) ? j->size + 2 : 2;
				if (o >= 4)
					return -1; // No more bytes
				while (o < 4)
					if (*((uint8_t*)&j->crc + o) == 0)
						return c;
					else
						++c, ++o;

				if (z)
					*z = false; // End reached without zeros
				return c;
			}
		}
	}
}

static void
_printJamb(const jamb_t *j)
{
	printf("%d->%d:", j->source, j->target);
	if (j->data)
		for (int i = 0; i < j->size; ++i)
			printf(" %02x", j->data[i]);
	printf(" <%08x>\n", j->crc);
	fflush(stdout);
}

static void
_testJamb(const jamb_t *j)
{
	jamsize_t o = 0;
	int p;

	_printJamb(j);

	printf("NZC =");

	bool z;
	do
	{
		p = _nonzeroCount(j, o, &z);
		printf(" %d", p);
		if (p < 0) //
			break;
		o += p + 1;
	} while (z);
	printf("\n");
	fflush(stdout);
}

int
main()
{
	uint8_t
		da[] = {0},
		db[] = {2, 0},
		dc[] = {0, 1, 2, 0},
		dd[] = {0, 1, 2, 0, 5},
		de[] = {1, 2, 3, 4, 5};

	uint32_t
		crc0 = 0x00345678,
		crc1 = 0x12345678;

	jamb_t
		j0 = { 0, 1, NULL, 0 },
		j1 = { 1, 0, NULL, 0 },
		j20 = { 1, 2, da, sizeof(da), .crc = crc0 },
		j21 = { 1, 2, da, sizeof(da), .crc = crc1 },
		j30 = { 1, 2, db, sizeof(db), .crc = crc0 },
		j31 = { 1, 2, db, sizeof(db), .crc = crc1 },
		j40 = { 1, 2, dc, sizeof(dc), .crc = crc0 },
		j41 = { 1, 2, dc, sizeof(dc), .crc = crc1 },
		j50 = { 1, 2, dd, sizeof(dd), .crc = crc0 },
		j51 = { 1, 2, dd, sizeof(dd), .crc = crc1 },
		j6 = { 1, 2, de, sizeof(de), .crc = crc1};

	jamb_t *j[] = { &j0, &j1, &j20, &j21, &j30, &j31, &j40, &j41, &j50, &j51, &j6, NULL };

	int i = 0;
	while (j[i])
		_testJamb(j[i++]);

	return 0;
}
