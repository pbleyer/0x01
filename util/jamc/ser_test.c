#include <stdio.h>

#include "jambus.h"
#include "jamreg.h"

static void
_printJamb(const jamb_t *j)
{
	printf("[%d/%d] %02x->%02x:", jambLength(j), jambBytes(j), j->source, j->target);
	if (j->data)
		for (int i = 0; i < j->size; ++i)
			printf(" %02x", j->data[i]);
	printf(" <%08x>\n", j->crc);
	fflush(stdout);
}

static void
_testJamb(const jamb_t *j)
{
	_printJamb(j);

	uint8_t b[1024];
	int q = jambWrite(j, b, sizeof(b));

	printf("[%d] =", q);

	for (int i = 0; i < q; ++i)
		printf(" %02x", (unsigned)b[i]);
	printf("\n");
	fflush(stdout);

	uint8_t a[1024];
	jamb_t k = {0, 0, a, .space = sizeof(a) };
	jambReset(&k);

	q = jambRead(&k, b, q);
	// jambCheck(&k)
	k.size = k.index - 2;
	_printJamb(&k);
	printf("-\n");
	fflush(stdout);
}

int
main()
{
	uint8_t
		da[] = {0},
		db[] = {0x22, 0},
		dc[] = {0, 0x11, 0x22, 0},
		dd[] = {0, 0x11, 0x22, 0, 0x55},
		de[] = {0x11, 0x22, 0x33, 0x44, 0x55},
		df[254-2];

	for (int i = 0; i < sizeof(df) - 4; ++i)
		df[i] = i + 3;

	for (int i = 0; i < 4; ++i)
		df[254-2-4+i] = 0xfb + i;

	uint32_t
		crc0 = 0x00345678,
		crc1 = 0x12345678;

	jamb_t
		j00 = { 0, 0, da, sizeof(da), .crc = 0 },
		j0 = { 0, 0x99, NULL, 0 },
		j1 = { 0x99, 0, NULL, 0 },
		j20 = { 0x99, 0xaa, da, sizeof(da), .crc = crc0 },
		j21 = { 0x99, 0xaa, da, sizeof(da), .crc = crc1 },
		j30 = { 0x99, 0xaa, db, sizeof(db), .crc = crc0 },
		j31 = { 0x99, 0xaa, db, sizeof(db), .crc = crc1 },
		j40 = { 0x99, 0xaa, dc, sizeof(dc), .crc = crc0 },
		j41 = { 0x99, 0xaa, dc, sizeof(dc), .crc = crc1 },
		j50 = { 0x99, 0xaa, dd, sizeof(dd), .crc = crc0 },
		j51 = { 0x99, 0xaa, dd, sizeof(dd), .crc = crc1 },
		j6 = { 0xbb, 0xcc, de, sizeof(de), .crc = crc1},
		j7 = { 1, 2, df, sizeof(df) - 4, .crc = 0xfefdfcfb},
		j8 = { 1, 2, df, sizeof(df), .crc = 0xfefdfcfb};


	jamb_t *j[] = { &j00, &j0, &j1, &j20, &j21, &j30, &j31, &j40, &j41, &j50, &j51, &j6, &j7, &j8, NULL };

	int i = 0;
	while (j[i])
		_testJamb(j[i++]);

	return 0;
}
