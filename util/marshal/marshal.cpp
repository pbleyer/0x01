#include "marshal.h"

namespace marshal_NAMESPACE {

#include <algorithm>

MarshalJson::json_t
MarshalJson::format(const IMarshalJson *m) const
{
	if (!m)
		return json_t();

	auto i = _map.find(typeid(*m));

	if (i != _map.end())
		return format(i->second._typeName, m); // Handler found
	else
		return m->toJson(this); // Handler not found, call object JSON serialization instead
}

IMarshalJson *
MarshalJson::parse(const json_t &j) const
{
	auto jt = j.find(marshal_TYPEKEY), jv = j.find(marshal_VALUEKEY);

	if (!j.is_object() || jt == j.end() || !jt->is_string() || jv == j.end())
		return nullptr; // Not an object or invalid format

	// Find type index from type name
	auto i = std::find_if(_map.begin(), _map.end(),
		[&](const auto &p) { return (p.second._typeName == jt.value()); }
	);

	if (i == _map.end())
		return nullptr; /** @todo Autodiscover object type instead? */
	else
		return i->second._unpack(jv.value(), this);
}

} // namespace marshal_NAMESPACE
