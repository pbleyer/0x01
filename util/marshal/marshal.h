#ifndef MARSHAL_H_
#define MARSHAL_H_

/** Default namespace. */
#ifndef marshal_NAMESPACE
#define marshal_NAMESPACE marshal
#endif

#ifndef marshal_TYPEKEY
#define marshal_TYPEKEY "type" ///< Default type key string.
#endif

#ifndef marshal_VALUEKEY
#define marshal_VALUEKEY "value" ///< Default value key string.
#endif

#include <json.hpp>
#include <unordered_map>
#include <string>
#include <typeindex>
#include <cstdint>
#include <memory>

namespace marshal_NAMESPACE {

/** Marshal object template. */
template<typename F>
class Marshal
{
public:
	/** Register type name and parser functor for type.
		@param t Object type index.
		@param s Object type name.
		@param f JSON parser for object value.
	*/
	bool add(const std::type_index &t, std::string s, F f)
	{
		if (_map.count(t))
			return false; // Exists
		_map.emplace(t, info_t{std::move(s), f});
		return true;
	}

	/** Unregister functor using type index.
		@param t Object type index.
		@return Registered functor.
	*/
	F remove(const std::type_index &t)
	{
		F f = nullptr;
		auto i = _map.find(t);

		if (i != _map.end())
		{
			f = i->second._unpack;
			_map.erase(i);
		}

		return f;
	}

	/** Unregister functor using type name.
		@param s Object type name.
		@return Registered functor.
	*/
	F remove(const std::string &s)
	{
		F f = nullptr;

		// Find string
		auto i = std::find_if(_map.begin(), _map.end(),
			[&](const auto &p) { return (p.second._typeName == s); }
		);

		if (i != _map.end())
		{
			f = i->second._unpack;
			_map.erase(i);
		}

		return f;
	}

protected:
	struct info_t
	{
		std::string _typeName;
		F _unpack;
	};

	using map_t = std::unordered_map<std::type_index, info_t>;

	map_t _map; ///< Type to parser mapping.
};


class MarshalJson;

/** JSON serializable object interface. */
struct IMarshalJson
{
	using json_t = nlohmann::json;

	/** Format object to JSON, using marshal facilities if provided. */
	virtual json_t toJson(const MarshalJson *m = nullptr) const = 0;

	/** Type of the object value JSON deserializer static handler. */
	using fromJson_t = IMarshalJson *(*)(const json_t &j, const MarshalJson *m);
};

/** JSON serialization manager. */
class MarshalJson: public Marshal<IMarshalJson::fromJson_t>
{
public:
	using json_t = IMarshalJson::json_t;

public:
	/** Object marshaling format.
		@param t Object type string.
		@param p Pointer to object to format.
		@return JSON serialization.
	*/
	json_t format(const std::string &t, const IMarshalJson *p) const
	{
		return json_t{{marshal_TYPEKEY, t}, {marshal_VALUEKEY, (p) ? p->toJson(this) : nullptr}};
	}

	/** Recursive object serialization.
		@param p Pointer to object to format.
		@return JSON serialization.
	*/
	json_t format(const IMarshalJson *p) const;

	/** Read a marshalled object.
		@param j JSON object to parse.
	*/
	IMarshalJson *parse(const json_t &j) const;
};


/* Binary */

class MarshalBinary;

/** Binary serializable object interface. */
struct IMarshalBinary
{
	using byte_t = uint8_t;

	/** Format object to binary, using marshal facilities if provided. */
	virtual std::size_t toBinary(byte_t *b, std::size_t n, const MarshalBinary *m = nullptr) const = 0;

	/** Type of the object value binary deserializer static handler. */
	using fromBinary_t = IMarshalBinary *(*)(const byte_t *b, std::size_t n, const MarshalBinary *m);
};


/** Binary serialization manager. */
class MarshalBinary: public Marshal<IMarshalBinary::fromBinary_t>
{
public:
	using byte_t = IMarshalBinary::byte_t;

public:
	/** Object marshaling format.
		@param t Object type string.
		@param p Pointer to object to format.
		@return Serialized byte count.
	*/
	std::size_t format(const std::string &t, const IMarshalBinary *p) const
	{
		return 0; /** @todo */
	}

	/** Recursive object serialization.
		@param p Pointer to object to format.
		@return Serialized byte count.
	*/
	std::size_t format(byte_t *b, std::size_t n, const IMarshalBinary *p) const;

	/** Read a marshalled object.
		@param j JSON object to parse.
	*/
	IMarshalBinary *parse(const byte_t *b, std::size_t n) const;
};

} // marshal_NAMESPACE

#endif // MARSHAL_H_
