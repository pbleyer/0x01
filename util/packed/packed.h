/** @file
	Packed definition
*/

#ifndef PACKED_H_
#define PACKED_H_

#if defined(__GNUC__)
#define __inline inline
/** Alias to define packed attributes (1-byte struct alignment) */
#define packed(x) x __attribute__((packed)) // __attribute__((aligned(4)))

#elif defined(__CC_ARM)
/** Alias to define packed attributes (1-byte struct alignment) */
#define packed(x) __packed x // __attribute__((packed))

#elif defined(_MSC_VER)
#define packed(x) __pragma(pack(push, 1)) x __pragma(pack(pop))
#endif

#endif // PACKDEF_H_
