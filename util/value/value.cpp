#include "value.h"

namespace value_NAMESPACE {

#include <sstream>

/* IntegerValue */
template<> const char *
IntegerValue::typeLabel() { return "Integer"; }

template<> string
IntegerValue::str(const type_t &v, bool d)
{
	std::stringstream s;
	s << v;
	return s.str();
}

template<> json_t
IntegerValue::toJson(const MarshalJson *m) const
{
	return json_t(str());
}

template<> IMarshalJson *
IntegerValue::fromJson(const json_t &j, const MarshalJson *m)
{
	if (!j.is_string())
		return nullptr;

	return ValueFactory::create(std::stoi(j.get<string>()));
}

/* RealValue */
template<> const char *
RealValue::typeLabel() { return "Real"; }

template<> string
RealValue::str(const type_t &v, bool d)
{
	std::stringstream s;
	s << v;
	return s.str();
}

template<> IMarshalJson *
RealValue::fromJson(const json_t &j, const MarshalJson *m)
{
	if (!j.is_number())
		return nullptr;

	return ValueFactory::create(j.get<double>());
}


/* String */
template<> const char *
StringValue::typeLabel() { return "String"; }

template<> string
StringValue::str(const type_t &v, bool d)
{
	std::stringstream s;
	s << (d ? "\"" : "") << v << (d ? "\"" : "");
	return s.str();
}

template<> IMarshalJson *
StringValue::fromJson(const json_t &j, const MarshalJson *m)
{
	if (!j.is_string())
		return nullptr;

	return ValueFactory::create(j.get<string>());
}

/* List */
template<> const char *
ListValue::typeLabel() { return "List"; }

/*
template<> template<typename... A>
ListValue::Value(A&&... a)
{
	std::list<ValueBuilder> l{a...};
	for (auto &i: l)
		_value.push_back(std::unique_ptr<IValue>(i.value));
}
*/

template<> ListValue::type_t
ListValue::clone(const type_t &t)
{
	type_t r;

	for (const auto &i: t)
		r.push_back(std::unique_ptr<IValue>(i->clone()));

	return r;
}

template<> string
ListValue::str(const type_t &v, bool d)
{
	std::stringstream s;
	auto i = v.begin(), e = v.end();

	if (d)
		s << "[";
	while (i != e)
	{
		IValue *v = i->get();

		s << ((v) ? v->str() : "null");
		++i;
		if (i != e)
			s << ",";
	}
	if (d)
		s << "]";

	return s.str();
}

template<> json_t
ListValue::toJson(const MarshalJson *m) const
{
	json_t v;
	for (const auto &i: _value)
		v.push_back(i ? (m ? m->format(i.get()) : i->toJson()) : nullptr);

	return v;
}

template<> IMarshalJson *
ListValue::fromJson(const json_t &j, const MarshalJson *m)
{
	if (!j.is_array())
		return nullptr;

	ListValue *p = new ListValue();

	if (m)
	{
		for (auto i = j.begin(); i != j.end(); ++i)
		{
			IValue *n = static_cast<IValue *>(m->parse(*i));
			if (n)
				p->value().push_back(std::unique_ptr<IValue>(n));
		}
	}

	return p;
}

/* Map */
template<> const char *
MapValue::typeLabel() { return "Map"; }

template<> MapValue::type_t
MapValue::clone(const type_t &t)
{
	type_t r;

	for (const auto &i: t)
		r[i.first] = std::unique_ptr<IValue>(i.second->clone());

	return r;
}

template<> string
MapValue::str(const type_t &v, bool d)
{
	std::stringstream s;
	auto i = v.begin(), e = v.end();

	if (d)
		s << "{";
	while (i != e)
	{
		s << "\"" << i->first << "\":";
		s << ((i->second) ? i->second->str() : "null");
		++i;
		if (i != e)
			s << ",";
	}
	if (d)
		s << "}";

	return s.str();
}

template<> json_t
MapValue::toJson(const MarshalJson *m) const
{
	json_t v;
	for (const auto &i: _value)
		v[i.first] = i.second ? (m ? m->format(i.second.get()) : i.second->toJson()) : nullptr;

	return v;
}

template<> IMarshalJson *
MapValue::fromJson(const json_t &j, const MarshalJson *m)
{
	if (!j.is_object())
		return nullptr;

	MapValue *p = new MapValue();

	if (m)
	{
		for (auto i = j.begin(); i != j.end(); ++i)
		{
			IValue *n = static_cast<IValue *>(m->parse(i.value()));
			if (n)
				p->value().emplace(i.key(), std::unique_ptr<IValue>(n));
		}
	}

	return p;
}

void ValueFactory::setup(MarshalJson &m)
{
	m.add(typeid(IntegerValue), IntegerValue::typeLabel(), IntegerValue::fromJson);
	m.add(typeid(RealValue), RealValue::typeLabel(), RealValue::fromJson);
	m.add(typeid(StringValue), StringValue::typeLabel(), StringValue::fromJson);
	m.add(typeid(ListValue), ListValue::typeLabel(), ListValue::fromJson);
	m.add(typeid(MapValue), MapValue::typeLabel(), MapValue::fromJson);
}


} // value_NAMESPACE
