/** @file
	Generic value framework.
*/

#ifndef VALUE_H_
#define VALUE_H_

#ifndef value_NAMESPACE
#define value_NAMESPACE value
#endif

#include <marshal.h>
#include <list>
#include <unordered_map>
#include <memory>

namespace value_NAMESPACE {

using namespace marshal;
using json_t = nlohmann::json;

/** Value interface. */
class IValue: public IMarshalJson
{
public:
	using list_t = std::list<std::unique_ptr<IValue>>;
	using map_t = std::unordered_map<string, std::unique_ptr<IValue>>;

public:
	virtual ~IValue() = default;

	/** Clone object. */
	virtual IValue *clone() const = 0;

	/** Copy from object.
		@return True on success, false otherwise (eg if object type is wrong).
	*/
	virtual bool copy(const IValue *v) = 0;

	/** Get object string representation.
		@param delim True to use object delimiters or decorators in representation.
	*/
	virtual string str(bool delim = true) const = 0;

	/** Get string representation of the object type.
		@return C-string with object type name.
	*/
	virtual const char* typeName() const = 0;
};


/** Value class template.
	Value derives from IValue and manages a specific value type.
	@param T The value type.
*/
template<typename T>
class Value: public IValue
{
public:
	using type_t = T; ///< Type alias.

protected:
	/** Convert value to string representation.
		Called by default by the str(bool) function with the object value as a parameter,
		so class implementations should define this.
		@param v Value to convert.
		@param delim If true, Use specific type string representation delimiters.
	*/
	static string str(const type_t &v, bool delim = true);

	/** Value clone, used by the clone() function to clone the managed value.
		@param v Value to clone.
		@return A deep copy of the value.
	*/
	static type_t clone(const type_t &v);

public:
	/* Build value object. */
	// Value() = default;

	/** Copy constructor. */
	Value(const Value &v)
		: _value(std::move(clone(v._value)))
	{ }

	/** Move constructor. */
	Value(Value &&v) = default;
	//	: _value(std::move(v._value)) { }

	/** Copy operator. */
	Value &operator=(const Value &v)
	{
		if (this != &v)
			_value = std::move(clone(v._value));
		return *this;
	}

	/** Move operator. */
	Value &operator=(Value &&v) = default;
	// { _value = std::move(v._value); return *this; }

	/** Create object value from value.
		@param v Value to copy into object value.
	*/
	Value(const type_t &v)
		: _value(std::move(clone(t)))
	{ }

	/** Variadic template constructor. */
	template <typename... A> Value(A&&... a);

	/** Map initializers */
	template<typename U> Value(std::unordered_map<string, U> &&m);
	template<typename U> Value(std::map<string, U> &&m);

	/** Helper constructor for strings. */
	// template <typename = std::enable_if<std::is_same<T, string>::value>>
	// Value(const char *s): _value(s) { }

	/** Value object copy.
		Return true if copy was successful.
	*/
	bool copy(const IValue *v); // { _value = dynamic_cast<const Value *>(v)->_value; }

	/** Value object clone.
		@return Object clone.
	*/
	IValue *clone() const { return new Value(*this); }

	/** Get value. */
	const type_t& value() const { return _value; }
	type_t& value() { return _value; }

	// operator const type_t&() const { return _value; }
	// operator type_t&() { return _value; }

	/** Explicitely set value.
		@param v Value to set object value.
	*/
	void setValue(const type_t &v) { _value = std::move(clone(t)); }

	/** Get string representation of value.
		@param delim True to use representation delimiters.
		@Return String representation, delimited if delim is true.
	*/
	string str(bool delim = true) const { return str(_value, delim); }

	/* IMarshal json serialization. */
	json_t toJson(const MarshalJson *m = nullptr) const;
	static IMarshalJson *fromJson(const json_t &j, const MarshalJson *m = nullptr);

	static const char* typeLabel();
	const char* typeName() const { return typeLabel(); }

protected:
	type_t _value; ///< Object value.
};

/** Default value clone is a pass through. */
template<typename T> inline
T Value<T>::clone(const T &t)
{
	return t;
}

/** Variadic template constructor.
	@todo Revise for list and map values.
*/
template<typename T>
template<typename... A> Value<T>::Value(A&&... a)
	: _value(a...)
{ }

/** Default copy method. */
template<typename T> inline bool
Value<T>::copy(const IValue *v)
{
	const Value *p = dynamic_cast<const Value *>(v);
	if (!p)
		return false;

	_value = std::move(clone(p->_value));
	return true;
}

/** Default json serialization. */
template<typename T> inline json_t
Value<T>::toJson(const MarshalJson *m) const
{
	// return json(str(typeid(*this) != typeid(StringValue)));
	return json_t(_value);
}

/** Default json deserialization. */
template<typename T> inline IMarshalJson *
Value<T>::fromJson(const json_t &j, const MarshalJson *m)
{
	return nullptr;
}

/* Supported value objects */
using IntegerValue = Value<int>;
using RealValue = Value<double>;
using StringValue = Value<string>;
using ListValue = Value<IValue::list_t>;
using MapValue = Value<IValue::map_t>;

/** Value factory and helper.
	Used directly through static member functions or can also hold a new
	value object in a unique_ptr (eg that may be moved).
*/
struct ValueFactory
{
	static IValue *create() { return nullptr; }
	static IValue *create(const std::nullptr_t &p) { return nullptr; }
	static IValue *create(const void *p) { return nullptr; }
	static IValue *create(int v) { return new IntegerValue(v); }
	static IValue *create(unsigned v) { return new IntegerValue(v); }
	static IValue *create(int v) { return new IntegerValue(v); }
	static IValue *create(float v) { return new RealValue(v); }
	static IValue *create(double v) { return new RealValue(v); }
	static IValue *create(const char *s) { return new StringValue(s); }
	static IValue *create(string v) { return new StringValue(std::move(v)); }

	ValueFactory(const ValueFactory &v): pvalue((v.pvalue) ? v.pvalue->clone() : nullptr) { }
	ValueFactory(ValueFactory &&v): pvalue(std::move(v.pvalue)) { }

	template <typename T> ValueFactory(const T &t): pvalue(create(t)) { }
	template <typename T> ValueFactory(T &&t): pvalue(create(std::move(t))) { }
	template <typename... A> ValueFactory(A&&... a): pvalue(new ListValue())
	{
		std::list<ValueFactory> l{a...};
		auto &p = pvalue.get().value();
		for (auto &i: l)
			p.push_back(std::move(i.pvalue));
	}

//	ValueFactory(std::initializer_list<std::pair<string, ValueFactory>> &l): pvalue(new MapValue())
//	{
//		auto &p = pvalue.get().value();
//		for (auto &i: l)
//			p.emplace(i.first, std::unique_ptr<IValue>(std::move(i.second.pvalue)));
//	}

	ValueFactory &operator=(ValueFactory v) { pvalue = std::move(v.pvalue); }
	ValueFactory &operator=(ValueFactory &&v) { pvalue = std::move(v.pvalue); }

	/** Register object serialization with marshal. */
	static void setup(MarshalJson &m);

	std::unique_ptr<IValue> pvalue;
};

/* Template specializations */

template<> template<typename... A> ListValue::Value(A&&... a)
{
	// ValueFactory v(a...);
	std::list<ValueFactory> l{a...};
	for (auto &i: l)
		_value.push_back(std::move(i.pvalue));
}

//template<> template<typename... A> MapValue::Value(A&&... a)
//{
//	std::unordered_map<string, ValueFactory> m{a...};
//	for (auto &i: m)
//		_value.emplace(i.first, std::move(i.second.pvalue));
//}

template<> template<typename U> MapValue::Value(std::unordered_map<string, U> &&m)
{
	for (auto &i: m)
		_value.emplace(std::move(i.first), std::move(i.second.pvalue));
}

template<> template<typename U> MapValue::Value(std::map<string, U> &&m)
{
	for (auto &i: m)
		_value.emplace(std::move(i.first), std::move(i.second.pvalue));
}

template<> json_t IntegerValue::toJson(const MarshalJson *m) const;
template<> json_t ListValue::toJson(const MarshalJson *m) const;
template<> json_t MapValue::toJson(const MarshalJson *m) const;

template<> IMarshalJson *IntegerValue::fromJson(const json_t &j, const MarshalJson *m);
template<> IMarshalJson *RealValue::fromJson(const json_t &j, const MarshalJson *m);
template<> IMarshalJson *StringValue::fromJson(const json_t &j, const MarshalJson *m);
template<> IMarshalJson *ListValue::fromJson(const json_t &j, const MarshalJson *m);
template<> IMarshalJson *MapValue::fromJson(const json_t &j, const MarshalJson *m);

template<> ListValue::type_t ListValue::clone(const type_t &t) const;
template<> MapValue::type_t MapValue::clone(const type_t &t) const;

// template<> string ListValue::str(bool d) const;
// template<> string MapValue::str(bool d) const;


} // value_NAMESPACE

#endif // VALUE_H
